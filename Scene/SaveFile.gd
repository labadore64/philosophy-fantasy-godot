extends Node

onready var player_name := ""

onready var duels_fought = []

onready var decks = []

var events = []

onready var duel_stats = {
	"wins" : 0,
	"losses" : 0,
	"draw" : 0
}

onready var character_appearance := {
	"background":1,
	"back_hair":2,
	"body":1,
	"neck":1,
	"shirt":1,
	"necklace":1,
	"face":1,
	"nose":1,
	"mouth":1,
	"eyes_back":1,
	"eyes":1,
	"eyebrows":1,
	"snout":1,
	"snout2":1,
	"glasses":1,
	"ear":2,
	"earring":1,
	"front_hair":2,
	"hat":1
}

onready var character_colors := {
	"background":Color(1,1,1),
	"back_hair":Color(1,1,1),
	"body":Color(1,1,1),
	"neck":Color(1,1,1),
	"shirt":Color(1,1,1),
	"necklace":Color(1,1,1),
	"face":Color(1,1,1),
	"nose":Color(1,1,1),
	"mouth":Color(1,1,1),
	"eyes_back":Color(1,1,1),
	"eyes":Color(1,1,1),
	"eyebrows":Color(1,1,1),
	"snout":Color(1,1,1),
	"snout2":Color(1,1,1),
	"glasses":Color(1,1,1),
	"ear":Color(1,1,1),
	"earring":Color(1,1,1),
	"front_hair":Color(1,1,1),
	"hat":Color(1,1,1),
}

onready var available_properties := {
	"background" : [],
	"back_hair" : [1,2,3,4,5,6,7,8],
	"body" : [1,2],
	"neck" : [1,2,3,4,5],
	"shirt" : [1,2,3,4],
	"necklace" : [],
	"face" : [1,2,3,4],
	"nose" : [1,2,3,4,5,6,7,8,9,10,11],
	"mouth" : [1,2,3,4,5,6,7,8,9,10,11,12,13,14],
	"eyes" : [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22],
	"eyebrows" : [1,2,3,4,5,6,7,8,9,10],
	"snout" : [],
	"glasses": [1,2,3,4],
	"ear" : [1,2,3,4,5,6,7,8],
	"front_hair" : [1,2,3,4,5,6,7,8],
	"hat" : [],
	"earring" : []
}

func saveFile():
	# create the json dictionary
	var json = {}
	json["available_properties"] = available_properties
	json["character_colors"] = character_colors
	json["character_appearance"] = character_appearance
	json["decks"] = decks
	json["player_name"] = player_name
	json["events"] = events
	json["duel_stats"] = duel_stats
	json["duel_fought"] = duels_fought
 
	var savegame = File.new()
	savegame.open("user://savegame.save", File.WRITE)

	savegame.store_line(JSON.print(json))

	savegame.close()

func loadFile():
	var savegame = File.new()
	savegame.open("user://savegame.save", File.READ)
	
	var line = savegame.get_line()
	var res = JSON.parse(line)
	var json = res.result
	
	savegame.close()
	available_properties = json["available_properties"]
	character_colors = json["character_colors"] 
	character_appearance = json["character_appearance"] 
	decks = json["decks"]
	player_name = json["player_name"]
	events = json["events"]
	duel_stats = json["duel_stats"]
	duels_fought = json["duel_fought"]
	
	# convert colors to actual colors
	for c in character_colors:
		var col = character_colors[c].split_floats(",")
		character_colors[c] = Color(col[0],col[1],col[2],col[3])

func clearFile():
	available_properties = {
		"background" : [],
		"back_hair" : [1,2,3,4,5,6,7,8],
		"body" : [1,2],
		"neck" : [1,2,3,4,5],
		"shirt" : [1,2,3,4],
		"necklace" : [],
		"face" : [1,2,3,4],
		"nose" : [1,2,3,4,5,6,7,8,9,10,11],
		"mouth" : [1,2,3,4,5,6,7,8,9,10,11,12,13,14],
		"eyes" : [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22],
		"eyebrows" : [1,2,3,4,5,6,7,8,9,10],
		"snout" : [],
		"glasses": [1,2,3,4],
		"ear" : [1,2,3,4,5,6,7,8],
		"front_hair" : [1,2,3,4,5,6,7,8],
		"hat" : [],
		"earring" : []
	}
		
	character_colors = {
		"background":Color(1,1,1),
		"back_hair":Color(1,1,1),
		"body":Color(1,1,1),
		"neck":Color(1,1,1),
		"shirt":Color(1,1,1),
		"necklace":Color(1,1,1),
		"face":Color(1,1,1),
		"nose":Color(1,1,1),
		"mouth":Color(1,1,1),
		"eyes_back":Color(1,1,1),
		"eyes":Color(1,1,1),
		"eyebrows":Color(1,1,1),
		"snout":Color(1,1,1),
		"snout2":Color(1,1,1),
		"glasses":Color(1,1,1),
		"ear":Color(1,1,1),
		"earring":Color(1,1,1),
		"front_hair":Color(1,1,1),
		"hat":Color(1,1,1),
	}
	
	character_appearance = {
		"background":1,
		"back_hair":2,
		"body":1,
		"neck":1,
		"shirt":1,
		"necklace":1,
		"face":1,
		"nose":1,
		"mouth":1,
		"eyes_back":1,
		"eyes":1,
		"eyebrows":1,
		"snout":1,
		"snout2":1,
		"glasses":1,
		"ear":2,
		"earring":1,
		"front_hair":2,
		"hat":1
	}

	duel_stats = {
		"wins" : 0,
		"losses" : 0,
		"draw" : 0
	}
	
	events = []
	player_name = ""
	duels_fought = []
	decks = []

func addEvent(event):
	events.append(event)
	
func removeEvent(event):
	events.erase(event)
	
func hasEvent(event):
	return events.has(event)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func addDuel(name):
	if !duels_fought.has(name):
		duels_fought.append(name)

func hasDuel(name):
	return duels_fought.has(name)

func addDeck(deck):
	decks.append(deck)
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
