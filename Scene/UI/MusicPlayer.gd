## MusicPlayer
## Plays music
extends AudioStreamPlayer

onready var last_song = ""		# Last song played
onready var current_song = ""	# Current song playing
onready var queue_song = ""		# Song to be queued next
onready var volume = .5			# Volume level for the music
onready var mute = false		# If the music is muted
onready var fade_time = 0.5		# Fade time for the music
onready var doFadeIn = false	# Is the music fading in?

# Silences the music
func _ready():
	volume_db = -80
	
# If mute, silences the music. If not, tweens in the fade.
func fadein():
	if(mute):
		volume_db = -80
	else:
		$Tween.stop_all()
		$Tween.interpolate_property(self, "volume_db",
		volume_db, 0-40*(1-volume), fade_time,
		Tween.TRANS_SINE, Tween.EASE_IN_OUT)
		$Tween.start()

# If mute, silences the music. If not, tweens out the fade.
func fadeout(fadeInTho=false):
	if(mute):
		volume_db = -80
	else:
		if(!fadeInTho):
			current_song = null
		doFadeIn = fadeInTho
		$Tween.stop_all()
		$Tween.interpolate_property(self, "volume_db",
		volume_db, -80, fade_time,
		Tween.TRANS_SINE, Tween.EASE_IN_OUT)
		$Tween.start()

# Starts playing the current song with a fade in.
func doPlay():
	if(current_song != null):
		if(mute):
			volume_db = -80
		var path = "res://sound/music/"+ current_song +".mp3"
		fadein()
		stop()
		if current_song != "":
			stream = load(path)
			play()

# If the current song isn't the same as the resource, updates
# the song and plays the new song.
func playSong(resource):
	if(current_song != resource):
		last_song = current_song;
		current_song = resource;
		if(last_song == ""):
			doPlay()
		else:
			fadeout(true)

# When all tweens are completed end the doFadeIn state.
func _on_Tween_tween_all_completed():
	if(doFadeIn):
		doPlay()
	doFadeIn = false
