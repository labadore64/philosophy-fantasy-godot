## Resolution
## Resolution  menu
extends Panel

onready var selectedIndex = 0		# Selcted index for the resolution
var translatey = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	rect_global_position = Vector2(
							(1920-rect_size.x) * 0.5, 
							(1080-rect_size.y) * 0.5+translatey
							)
	get_parent().get_node("AnimationPlayer").play("Load")
	get_parent().get_node("Open").play()
	clearInput()
	if(Global.window_width == 1920):
		$ScrollContainer/VBoxContainer/Resolution1.pressed = true
	elif(Global.window_width == 1280):
		$ScrollContainer/VBoxContainer/Resolution2.pressed = true
	elif(Global.window_width == 1024):
		$ScrollContainer/VBoxContainer/Resolution3.pressed = true
	elif(Global.window_width == 960):
		$ScrollContainer/VBoxContainer/Resolution4.pressed = true
	elif(Global.window_width == 768):
		$ScrollContainer/VBoxContainer/Resolution5.pressed = true

# Clears all the inputs so one can be selected.
func clearInput():
	for c in range(1,6):
		var node = $ScrollContainer/VBoxContainer.get_node("Resolution" + String(c))
		node.pressed = false
		
# Confirm the current selection
func confirm(width,height):
	Global.window_width = width
	Global.window_height = height
	Global.setWindowSize()
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().base_parent.get_node("Panel").updateResolutionButton()

# Cancel button pressed
func _on_CancelButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Confirm").play()


# Inputs the selection and closes the menu
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel") || Input.is_action_just_pressed("ui_select"):
		if(!get_parent().disabled):
			Input.action_release("ui_select")
			Input.action_release("ui_cancel")
			_on_CancelButton_pressed()
			
# The following select one of the 5 resolutions:
func _on_Resolution1_pressed():
	clearInput()
	$ScrollContainer/VBoxContainer/Resolution1.pressed = true
	confirm(1920,1080)

func _on_Resolution2_pressed():
	clearInput()
	$ScrollContainer/VBoxContainer/Resolution2.pressed = true
	confirm(1280,720)

func _on_Resolution3_pressed():
	clearInput()
	$ScrollContainer/VBoxContainer/Resolution3.pressed = true
	confirm(1024,576)

func _on_Resolution4_pressed():
	clearInput()
	$ScrollContainer/VBoxContainer/Resolution4.pressed = true
	confirm(960,540)

func _on_Resolution5_pressed():
	clearInput()
	$ScrollContainer/VBoxContainer/Resolution5.pressed = true
	confirm(768,432)
	
# When finished animation destroys the menu
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		var main = get_tree().get_root().get_node("Main")
		main.emit_signal("settingsTrigger",get_parent().name)
		get_parent().queue_free()
