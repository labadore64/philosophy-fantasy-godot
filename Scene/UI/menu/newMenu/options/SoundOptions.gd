## Sound
## Sound options menu
extends Panel

var _v_scroll	# Scrollbar container reference
var _v_scroller	# Vscroll reference
var translatey = 0

# Preloaded Sound Test var
onready var soundTest = preload("res://Scene/UI/menu/newMenu/SoundTest/SoundTest.tscn")

# Configures the references, sets the pressed values ect.
func _ready():
	$Control.translatey = translatey
	rect_global_position = Vector2(
							(1920-rect_size.x) * 0.5, 
							(1080-rect_size.y) * 0.5 + 20+translatey
							)
	_v_scroll = get_node("ScrollContainer")
	_v_scroller = _v_scroll.get_node("_v_scroll")

	get_parent().get_node("AnimationPlayer").play("Load")
	get_parent().get_node("Open").play()
	$ScrollContainer/VBoxContainer.get_node("Volume").get_node("SFXHSlider").value = Global.sfxVolume*10
	$ScrollContainer/VBoxContainer.get_node("Volume").get_node("MusicHSlider").value = Global.musicVolume*10
	$ScrollContainer/VBoxContainer.get_node("Volume").get_node("MasterHSlider").value = Global.masterVolume*10
	$ScrollContainer/VBoxContainer.get_node("Volume").get_node("AmbienceHSlider").value = Global.ambienceVolume*10

# Does OK/Cancel actions with the key presses
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		if(!get_parent().disabled):
			Input.action_release("ui_cancel")
			_on_CancelButton_pressed()
	if Input.is_action_just_pressed("ui_select"):
		if(!get_parent().disabled):
			Input.action_release("ui_select")
			_on_OKButton_pressed()
		
# OK button pressed
func _on_OKButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Confirm").play()

# Cancel button pressed
func _on_CancelButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Cancel").play()

# Destroy the menu after the animation completes
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		var main = get_tree().get_root().get_node("Main")
		main.emit_signal("settingsTrigger",get_parent().name)
		get_parent().queue_free()

# SFX Volume slider
func _on_SFXHSlider_value_changed(value):
	Global.sfxVolume = value * 0.1

# Music Volume slider
func _on_MusicHSlider_value_changed(value):
	Global.musicVolume = value * 0.1

# Master Volume slider
func _on_MasterHSlider_value_changed(value):
	Global.masterVolume = value * 0.1
	
# Ambience Volume slider
func _on_AmbienceHSlider_value_changed(value):
	Global.ambienceVolume = value * 0.1

# Close button action
func doClose():
	_on_CancelButton_pressed()

# Creates submenu for sound test
func _on_Button_pressed():
	var sound = get_parent().create_submenu(soundTest)
