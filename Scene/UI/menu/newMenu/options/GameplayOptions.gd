## Gameplay
## Gameplay options menu
extends Panel

var translatey = 0
var _v_scroll	# Scrollbar container reference
var _v_scroller	# Vscroll reference

# Gets the references and populates the values
func _ready():
	rect_global_position = Vector2(
							(1920-rect_size.x) * 0.5, 
							(1080-rect_size.y) * 0.5 + 20+translatey
							)
	$Control.translatey = translatey	
	_v_scroll = get_node("ScrollContainer")
	_v_scroller = _v_scroll.get_node("_v_scroll")
	get_parent().get_node("AnimationPlayer").play("Load")
	get_parent().get_node("Open").play()
	$ScrollContainer/VBoxContainer.get_node("Volume").get_node("SpeedHSlider").value = round((Global.textSpeed - 1) * 0.5)
	$ScrollContainer/VBoxContainer/autoChkbox.pressed = Global.auto_continue_text
	$ScrollContainer/VBoxContainer/AutoSpeed/AutoHSlider.value = Global.auto_continue_text_speed
	$ScrollContainer/VBoxContainer/AutoSpeed/AutoHSlider.editable = $ScrollContainer/VBoxContainer/autoChkbox.pressed

# Does OK/Cancel actions with the key presses
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		if(!get_parent().disabled):
			Input.action_release("ui_cancel")
			_on_CancelButton_pressed()
	if Input.is_action_just_pressed("ui_select"):
		if(!get_parent().disabled):
			Input.action_release("ui_select")
			_on_OKButton_pressed()
			
# Pressed the OK button
func _on_OKButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Confirm").play()

# Pressed the Cancel button
func _on_CancelButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Cancel").play()

# Animation completes and kills the menu
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		var main = get_tree().get_root().get_node("Main")
		main.emit_signal("settingsTrigger",get_parent().name)
		get_parent().queue_free()

# Text speed slider
func _on_SpeedHSlider_value_changed(value):
	Global.textSpeed = value*2 + 1

# Auto continue checkbox
func _on_autoChkbox_pressed():
	$ScrollContainer/VBoxContainer/AutoSpeed/AutoHSlider.editable = $ScrollContainer/VBoxContainer/autoChkbox.pressed
	Global.auto_continue_text = $ScrollContainer/VBoxContainer/autoChkbox.pressed

# Auto continue speed slider
func _on_AutoHSlider_value_changed(value):
	Global.auto_continue_text_speed = value

# Close button action
func doClose():
	_on_CancelButton_pressed()
