## Input
## Input options menu
extends Panel

onready var main = get_tree().get_root().get_node("Main")
var menu = null
var translatey = 0
onready var keymenu = preload("res://Scene/UI/menu/newMenu/options/InputBind/KeyboardInput.tscn")
onready var controllermenu = preload("res://Scene/UI/menu/newMenu/options//InputBind/GamepadInput.tscn")
onready var mousemenu = preload("res://Scene/UI/menu/newMenu/options/InputBind/MouseInput.tscn")
# Called when the node enters the scene tree for the first time.

var _v_scroll	# Scrollbar container reference
var _v_scroller	# Vscroll reference

# Configures the references, sets the pressed values ect.
func _ready():
	rect_global_position = Vector2(
							(1920-rect_size.x) * 0.5, 
							(1080-rect_size.y) * 0.5+translatey
							)
	$Control.translatey = translatey
	_v_scroll = get_node("ScrollContainer")
	_v_scroller = _v_scroll.get_node("_v_scroll")
	
	get_parent().get_node("AnimationPlayer").play("Load")
	get_parent().get_node("Open").play()
	if Input.get_connected_joypads().size() > 0:
		$ScrollContainer/VBoxContainer/Keyboard.disabled = false
		$ScrollContainer/VBoxContainer/Controller.disabled = false
	else:
		$ScrollContainer/VBoxContainer/Keyboard.disabled = false
		$ScrollContainer/VBoxContainer/Controller.disabled = true

# Does OK/Cancel actions with the key presses
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		if(!get_parent().disabled):
			Input.action_release("ui_cancel")
			_on_CancelButton_pressed()
	if Input.is_action_just_pressed("ui_select"):
		if(!get_parent().disabled):
			Input.action_release("ui_select")
			_on_OKButton_pressed()

# OK button pressed
func _on_OKButton_pressed():
	if(!is_instance_valid(menu)):
		get_parent().get_node("AnimationPlayer").play("Unload")
		get_parent().get_node("Confirm").play()

# Cancel button pressed
func _on_CancelButton_pressed():
	if(!is_instance_valid(menu)):
		get_parent().get_node("AnimationPlayer").play("Unload")
		get_parent().get_node("Cancel").play()

# Destroy the menu after the animation completes
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		get_parent().queue_free()

# Keyboard menu open
func _on_Keyboard_pressed():
	var sub = get_parent().create_submenu(keymenu)
	sub.get_node("Panel").translatey = translatey
	sub.get_node("Panel").rect_position.y += translatey
	sub.get_node("Panel/Control").translatey = translatey

# Controller menu open
func _on_Controller_pressed():
	if Input.get_connected_joypads().size() > 0:
		var sub = get_parent().create_submenu(controllermenu)
		sub.get_node("Panel").translatey = translatey
		sub.get_node("Panel").rect_position.y += translatey
		sub.get_node("Panel/Control").translatey = translatey
	else:
		main.playSoundFx("ui/menu_error")

# Mouse menu open
func _on_Mouse_pressed():
	var sub = get_parent().create_submenu(mousemenu)
	sub.get_node("Panel").translatey = translatey
	sub.get_node("Panel").rect_position.y += translatey
	sub.get_node("Panel/Control").translatey = translatey
	
# Close button action
func doClose():
	_on_CancelButton_pressed()
