## Graphics
## Graphics options menu
extends Panel

onready var menu = null			# Submenu reference
# Preloaded resolution menu
onready var resmenu = preload("res://Scene/UI/menu/newMenu/options/ResolutionMenu.tscn")
var translatey = 0
var _v_scroll	# Scrollbar container reference
var _v_scroller	# Vscroll reference

# Configures the references, sets the pressed values ect.
func _ready():
	rect_global_position = Vector2(
							(1920-rect_size.x) * 0.5, 
							(1080-rect_size.y) * 0.5 + 20+translatey
							)
	$Control.translatey = translatey
	_v_scroll = get_node("ScrollContainer")
	_v_scroller = _v_scroll.get_node("_v_scroll")
	get_parent().get_node("AnimationPlayer").play("Load")
	
	get_parent().get_node("Open").play()
	$ScrollContainer/VBoxContainer/FullscreenChkbox.pressed = OS.window_fullscreen
	$ScrollContainer/VBoxContainer/BlackscreenChkbox.pressed = Global.show_screen
	$ScrollContainer/VBoxContainer/PortraitChkbox.pressed = Global.show_portrait
	$ScrollContainer/VBoxContainer/ColorblindChkbox.pressed = Global.colorblind_mode

	# action button
	updateResolutionButton()

# Updates the value displayed on the resolution button
func updateResolutionButton():
	$ScrollContainer/VBoxContainer/ScreenLabel4/Resolution.text = String(Global.window_width) + " x " + String(Global.window_height)

# Does OK/Cancel actions with the key presses
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		if(!get_parent().disabled):
			Input.action_release("ui_cancel")
			_on_CancelButton_pressed()
	if Input.is_action_just_pressed("ui_select"):
		if(!get_parent().disabled):
			Input.action_release("ui_select")
			_on_OKButton_pressed()
			
# OK button pressed
func _on_OKButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Confirm").play()

# Cancel button pressed
func _on_CancelButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Cancel").play()

# Animation finished, closes menu
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		var main = get_tree().get_root().get_node("Main")
		main.emit_signal("settingsTrigger",get_parent().name)
		get_parent().queue_free()

# Fullscreen button checked
func _on_CheckButton_pressed():
	OS.window_fullscreen = $ScrollContainer/VBoxContainer/FullscreenChkbox.pressed

# Enable graphics checked (doesnt appear to work?)
func _on_BlackscreenChkbox_pressed():
	Global.setScreenVisible($ScrollContainer/VBoxContainer/BlackscreenChkbox.pressed)
	if !Global.show_screen:
		if(!Global.TTS_enabled):
			var main = get_tree().get_root().get_node("Main")
			Global.TTS_enabled = true
			Global.tts_say("Screen disabled. TTS Enabled.")
		Global.camera_follow = false

# Camera_follow checkbox checked
func _on_CameraAdjustChkbox_pressed():
	Global.camera_follow = $ScrollContainer/VBoxContainer/CameraAdjustChkbox.pressed

# Show_portrait checkbox checked
func _on_BlackscreenChkbox2_pressed():
	Global.show_portrait = $ScrollContainer/VBoxContainer/PortraitChkbox.pressed

# Resolution menu opened
func _on_Resolution_pressed():
	var sub = get_parent().create_submenu(resmenu)
	sub.get_node("Panel").translatey = translatey
	sub.get_node("Panel").rect_position.y += translatey
	
# Saturation slider
func _on_SaturationHSlider_value_changed(value):
	get_tree().get_root().get_node("Main/ViewportContainer/Viewport/Map/map/Camera").setSaturation(value)

# Contrast slider
func _on_ContrastHSlider_value_changed(value):
	get_tree().get_root().get_node("Main/ViewportContainer/Viewport/Map/map/Camera").setContrast(value)

# Brightness slider
func _on_BrightnessHSlider_value_changed(value):
	get_tree().get_root().get_node("Main/ViewportContainer/Viewport/Map/map/Camera").setBrightness(value)

# Render distance slider
func _on_RenderDistHSlider_value_changed(value):
	get_tree().get_root().get_node("Main/ViewportContainer/Viewport/Map/map/Camera").setRenderDistance(value)

# Close button action
func doClose():
	_on_CancelButton_pressed()

func _exit_tree():
	var noder = get_tree().get_root().get_node("Main")
	if noder.get_node("Node2D") != null:
		noder.get_node("Node2D").doColorblind()

func _on_PortraitChkbox2_pressed():
	Global.colorblind_mode = $ScrollContainer/VBoxContainer/ColorblindChkbox.pressed
