## KeyboardInput
## Keyboard Input Menu
extends Panel

# Container reference
onready var container = $ScrollContainer/VBoxContainer

var inputz = null	# Input Menu

# Represents different groups of keys that cannot be shared
# Might customize individual commands later more.
const OVERWORLD_KEYS = ["run","interact","run_toggle","move_up","move_down","move_left","move_up"]
const UI_KEYS = ["ui_select","ui_accept","open_menu","ui_cancel","ui_right","ui_left","ui_down","ui_up","ui_focus_prev","ui_focus_next"]
const ACCESS_KEYS = ["tts_stop","tts_repeat","tts_look","tts_read_coordinates","access_tooltip"]
const ZOOM_KEYS = ["access_key1","access_key2","access_key3","access_key4","access_key5","access_key6","access_key7","access_key8","access_key9"]
var translatey = 0
# Preloaded key menu
onready var keymenu = preload("res://Scene/UI/menu/newMenu/options/InputBind/InputKey.tscn")

var _v_scroll	# Scrollbar container reference
var _v_scroller	# Vscroll reference

# Closes the box with the x button
func doClose():
	_on_CancelButton_pressed()

# Configures the references, sets the buttons properly ect
func _ready():
	$Control.translatey = translatey
	rect_global_position = Vector2(
							(1920-rect_size.x) * 0.5, 
							(1080-rect_size.y) * 0.5+translatey
							)
	_v_scroll = get_node("ScrollContainer")
	_v_scroller = _v_scroll.get_node("_v_scroll")
	setButtons()
	# populates navigation
	# this lets you easily select in the box
		
	get_parent().get_node("AnimationPlayer").play("Load")
	get_parent().get_node("Open").play()

# Configures the buttons to say the right text
func setButtons():
	setButtonName(container.get_node("GridContainer/AcceptButton"),"ui_accept")
	setButtonName(container.get_node("GridContainer/SelectButton2"),"ui_select")
	setButtonName(container.get_node("GridContainer/SelectButton"),"open_menu")
	setButtonName(container.get_node("GridContainer/CancelnputButton"),"ui_cancel")
	setButtonName(container.get_node("GridContainer/UpButton"),"ui_up")
	setButtonName(container.get_node("GridContainer/DownButton"),"ui_down")
	setButtonName(container.get_node("GridContainer/LeftButton"),"ui_left")
	setButtonName(container.get_node("GridContainer/RightButton"),"ui_right")
	setButtonName(container.get_node("GridContainer/FocusPreviousButton"),"ui_focus_prev")
	setButtonName(container.get_node("GridContainer/FocusNextButton"),"ui_focus_next")
	setButtonName(container.get_node("GridContainer/HelpButton"),"ui_help")
	setButtonName(container.get_node("GridContainer/RunToggleButton"),"run_toggle")
	
	setButtonName(container.get_node("GridContainer3/TTSStopButton"),"tts_stop")
	setButtonName(container.get_node("GridContainer3/TTSRepeatButton"),"tts_repeat")
	setButtonName(container.get_node("GridContainer3/TTSHintButton"),"access_tooltip")
	setButtonName(container.get_node("GridContainer3/TTSCoordinatesButton"),"tts_read_coordinates")

# Sets a single button's text
func setButtonName(button,action):
	button.text = "[None]"
	var list = InputMap.get_action_list(action)
	for c in list:
		if c is InputEventKey:
			button.text = OS.get_scancode_string(c.scancode)
			break;

# Press the cancel key
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		if(!get_parent().disabled):
			Input.action_release("ui_cancel")
			_on_CancelButton_pressed()
			
# Press the OK button
func _on_OKButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Confirm").play()
	Global.saveKeyboardInput()

# Pressed the cancel button
func _on_CancelButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Cancel").play()

# Destroys itself when the menu close animation is complete
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		get_tree().get_root().get_node("Main").emit_signal("settingsTrigger",get_parent().name)
		get_parent().queue_free()
		
# Update the input of a single key
func updateInput(input,label,exceptions=[],can_clear=true):
	if inputz == null:
		var test = exceptions.duplicate()
		test.erase(input)
		var parentContainer = get_parent()
		parentContainer.disable()
		var menu = keymenu.instance()
		menu.translatey = translatey
		inputz = menu
		menu.can_clear = can_clear
		parentContainer.add_child(menu)
		menu.action = input
		var actions = InputMap.get_action_list(input)
		for a in actions:
			if a is InputEventKey:
				menu.action_obj = a
				break;
		menu.setText(label.text,test)

# Zoom button arrays, for magnifier keys.
func getZoomArray():
	var arr = UI_KEYS
	arr.append_array(ACCESS_KEYS)
	arr.append_array(ZOOM_KEYS)
	return arr
	
# Access key arrays, for access keys
func getAccessKeyArray():
	var arr = UI_KEYS
	arr.append_array(ACCESS_KEYS)
	arr.append_array(OVERWORLD_KEYS)
	arr.append_array(ZOOM_KEYS)
	return arr

# When the top button is selected, scroll all the way to the top
func _on_AcceptButton_focus_entered():
	$ScrollContainer.scroll_vertical = 0

# The following are individual buttons for specific keys

func _on_AcceptButton_pressed():
	updateInput("ui_accept", container.get_node("GridContainer/AcceptLabel"),UI_KEYS,false)

func _on_SelectButton_pressed():
	updateInput("open_menu", container.get_node("GridContainer/SelectLabel"),UI_KEYS,false)

func _on_CancelnputButton_pressed():
	updateInput("ui_cancel", container.get_node("GridContainer/CancelLabel"),UI_KEYS,false)

func _on_UpButton_pressed():
	updateInput("ui_up", container.get_node("GridContainer/UpLabel"),UI_KEYS,false)

func _on_DownButton_pressed():
	updateInput("ui_down", container.get_node("GridContainer/DownLabel"),UI_KEYS,false)

func _on_LeftButton_pressed():
	updateInput("ui_left", container.get_node("GridContainer/LeftLabel"),UI_KEYS,false)

func _on_RightButton_pressed():
	updateInput("ui_right", container.get_node("GridContainer/RightLabel"),UI_KEYS,false)

func _on_MoveUpButton_pressed():
	updateInput("move_up", container.get_node("GridContainer2/UpLabel"),OVERWORLD_KEYS)

func _on_MoveDownButton_pressed():
	updateInput("move_down", container.get_node("GridContainer2/DownLabel"),OVERWORLD_KEYS)

func _on_MoveLeftButton_pressed():
	updateInput("move_left", container.get_node("GridContainer2/LeftLabel"),OVERWORLD_KEYS)

func _on_MoveRightButton_pressed():
	updateInput("move_right", container.get_node("GridContainer2/RightLabel"),OVERWORLD_KEYS)

func _on_RunButton_pressed():
	updateInput("run", container.get_node("GridContainer2/RunLabel"),OVERWORLD_KEYS)

func _on_RunToggleButton_pressed():
	updateInput("run_toggle", container.get_node("GridContainer/RunToggleLabel"),OVERWORLD_KEYS)

func _on_InteractButton_pressed():
	updateInput("interact", container.get_node("GridContainer2/InteractLabel"),OVERWORLD_KEYS,false)

func _on_TTSStopButton_pressed():
	updateInput("tts_stop", container.get_node("GridContainer3/TTSStopLabel"),getAccessKeyArray())

func _on_TTSRepeatButton_pressed():
	updateInput("tts_repeat", container.get_node("GridContainer3/TTSRepeatLabel"),getAccessKeyArray())

func _on_TTSHintButton_pressed():
	updateInput("access_tooltip", container.get_node("GridContainer3/TTSHintLabel"),getAccessKeyArray())

func _on_TTSCoordinatesButton_pressed():
	updateInput("tts_read_coordinates", container.get_node("GridContainer3/TTSCoordinatesLabel"),getAccessKeyArray())

func _on_FocusPreviousButton_pressed():
	updateInput("ui_focus_prev", container.get_node("GridContainer/FocusPreviousLabel"),UI_KEYS)

func _on_FocusNextButton_pressed():
	updateInput("ui_focus_next", container.get_node("GridContainer/FocusNextLabel"),UI_KEYS)

func _on_HelpButton_pressed():
	updateInput("ui_help", container.get_node("GridContainer/HelpLabel"),UI_KEYS)

func _on_SelectButton2_pressed():
	updateInput("ui_select", container.get_node("GridContainer/SelectLabel2"),UI_KEYS,false)
