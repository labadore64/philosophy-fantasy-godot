## KeyboardInputAccess
## Inherits the accessibility class and adds some tab controls
extends "res://access/NewAccessible.gd"

# Adds tab control for the different sections
func onReady():
	addTabControl($Panel/ScrollContainer/VBoxContainer/GridContainer,
				$Panel/OKButton,
				$Panel/ScrollContainer/VBoxContainer/GridContainer3)
	addTabControl($Panel/ScrollContainer/VBoxContainer/GridContainer3,
				$Panel/ScrollContainer/VBoxContainer/GridContainer,
				$Panel/OKButton)
	
# Adds one set of tab controls
func addTabControl(current,prev,next):
	
	var previous
	var nexter
	
	if prev is Button:
		previous = prev.get_path()
	else:
		previous = prev.get_child(1).get_path()
	if next is Button:
		nexter = next.get_path()
	else:
		nexter = next.get_child(1).get_path()
	
	for c in current.get_children():
		if c is Button:
			c.focus_next = nexter
			c.focus_previous = previous
