# Defines the help menu for Keyboard Input

extends "res://Scene/UI/menu/Help/HelpClose.gd"

# Called when the node enters the scene tree for the first time.
func _ready():
	base_parent = get_parent()
	resource_name = "res://help/InputKeyboard.tres"
	._ready()
