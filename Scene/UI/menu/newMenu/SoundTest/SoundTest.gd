## SoundTest
## Sound Test menu
extends Panel

const SONGS = [
				"test",
				"victory"
			] 	# List of songs
const SFX = [	
				"ui/close_dialog",
				"ui/continue_dialog",
				"ui/menu_textfinish",
				"ui/menu_alert",
				"ui/menu_cancel",
				"ui/menu_error",
				"ui/menu_help",
				"ui/menu_left",
				"ui/menu_right",
				"ui/menu_down",
				"ui/menu_up",
				"ui/menu_open_up",
				"ui/menu_open",
				"ui/menu_tabprev",
				"ui/menu_tabnext",
				"ui/open_dialog",
				"overworld/defaultWarp",
				"test/swan",
				"footstep/test",
				"footstep/wall_hit",
				"env/meadow",
				"env/desert_01",
				"env/ambience_wind",
				"emote/emote_thinking",
				"emote/emote_sad",
				"emote/emote_question",
				"emote/emote_exclaim",
				"access/access_textedit",
				"access/disable_magnify",
				"access/enable_magnify",
				"access/npclocator",
				"access/npc_swan",
				"access/place_locator",
				"access/talkToNPC",
				"access/talkToNPCDo",
				"access/talkToNPCOut",
				"access/user_locator",
				"access/view_point_in",
				"access/view_point_out",
				"access/view_point_warp",
				"access/wallSound"
			]	# List of sfx
			
var selected_song = 0	# Current selected song index
var selected_sfx = 0	# Current selected SFX index

# On ready, load animations
func _ready():
	rect_global_position = Vector2(
							(1920-rect_size.x) * 0.5, 
							(1080-rect_size.y) * 0.5
							)
	get_parent().get_node("AnimationPlayer").play("Load")
	get_parent().get_node("Open").play()

# Decrement song
func _on_DecreaseMusic_pressed():
	selected_song-=1
	if selected_song < 0:
		selected_song = SONGS.size()-1
	Global.playSong(SONGS[selected_song])
	$Volume/MusicNumber.text = str(selected_song+1)

# Increment song
func _on_IncreaseMusic_pressed():
	selected_song+=1
	if selected_song >= SONGS.size():
		selected_song = 0
	Global.playSong(SONGS[selected_song])
	$Volume/MusicNumber.text = str(selected_song+1)

func _on_DecreaseSFX_pressed():
	selected_sfx-=1
	if selected_sfx < 0:
		selected_sfx = SFX.size()-1
	Global.playSoundFx(SFX[selected_sfx])
	$Volume/SFXNumber.text = str(selected_sfx+1)

func _on_IncreaseSFX_pressed():
	selected_sfx+=1
	if selected_sfx >= SFX.size():
		selected_sfx = 0
	Global.playSoundFx(SFX[selected_sfx])
	$Volume/SFXNumber.text = str(selected_sfx+1)

# OK button pressed
func _on_OKButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Confirm").play()

# Cancel button pressed
func _on_CancelButton_pressed():
	get_parent().get_node("AnimationPlayer").play("Unload")
	get_parent().get_node("Cancel").play()

# Destroy the menu after the animation completes
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		var main = get_tree().get_root().get_node("Main")
		main.emit_signal("settingsTrigger",get_parent().name)
		get_parent().queue_free()

# Close button action
func doClose():
	_on_CancelButton_pressed()

			
# Does OK/Cancel actions with the key presses
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		if(!get_parent().disabled):
			Input.action_release("ui_cancel")
			_on_CancelButton_pressed()
	if Input.is_action_just_pressed("ui_select"):
		if(!get_parent().disabled):
			Input.action_release("ui_select")
			_on_OKButton_pressed()

# Keyboard/Controller input for music
func _on_MusicNumber_gui_input(event):
	if Input.is_action_just_pressed("ui_left"):
		_on_DecreaseMusic_pressed()
	elif Input.is_action_just_pressed("ui_right"):
		_on_IncreaseMusic_pressed()
	elif Input.is_action_just_pressed("ui_accept"):
		Global.stopSong()
		Global.playSong(SONGS[selected_song])

# Keyboard/Controller input for sfx
func _on_SFXNumber_gui_input(event):
	if Input.is_action_just_pressed("ui_left"):
		_on_DecreaseSFX_pressed()
	elif Input.is_action_just_pressed("ui_right"):
		_on_IncreaseSFX_pressed()
	elif Input.is_action_just_pressed("ui_accept"):
		Global.playSoundFx(SFX[selected_sfx])
