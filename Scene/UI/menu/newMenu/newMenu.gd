## newMenu
## Actually, just the main menu in the overworld. It's a new version of it.
extends "res://access/NewAccessible.gd"

var destroyed = false			# Whether or not the menu has been destroyed
var anim_exec = false			# Whether or not the start animation was started.

# Graphics menu scene
onready var graphicsmenu = preload("res://Scene/UI/menu/newMenu/options/Graphics.tscn")
# Sound menu scene
onready var soundmenu = preload("res://Scene/UI/menu/newMenu/options/Sound.tscn")
# Input menu scene
onready var inputmenu = preload("res://Scene/UI/menu/newMenu/options/Input.tscn")
# Gameplay menu scene
onready var gameplaymenu = preload("res://Scene/UI/menu/newMenu/options/Gameplay.tscn")
# Access menu scene
onready var accessmenu = preload("res://Scene/UI/menu/newMenu/options/Accessibility.tscn")

onready var map_points = [] 	# The map point list
onready var viewing = false		# Whether or not you are viewing a map point.
onready var warping = false		# Whether or not you are warping to a map point.

# Reference to the Main viewport
onready var main = get_tree().get_root().get_node("Main/ViewportContainer/Viewport")
# Reference to the currently loaded Map

onready var startTranslation	# The start translation of the camera.
onready var startRotation		# The start rotation of the camera.

var translatey = 0

var _v_scroll	# Scrollbar container reference
var _v_scroller	# Vscroll reference

# Graphics menu button pressed
func _on_Graphics_pressed():
	var sub = create_submenu(graphicsmenu)
	sub.get_node("Panel").translatey = translatey
	sub.get_node("Panel").rect_position.y += translatey
	sub.get_node("Panel/Control").translatey = translatey

# Sound menu button press
func _on_Sound_pressed():
	var sub = create_submenu(soundmenu)
	sub.get_node("Panel").translatey = translatey
	sub.get_node("Panel").rect_position.y += translatey
	sub.get_node("Panel/Control").translatey = translatey
	
# Gameplay menu button press
func _on_Gameplay_pressed():
	var sub = create_submenu(gameplaymenu)
	sub.get_node("Panel").translatey = translatey
	sub.get_node("Panel").rect_position.y += translatey
	sub.get_node("Panel/Control").translatey = translatey

# Accessibility menu button press
func _on_Accessibility_pressed():
	var sub = create_submenu(accessmenu)
	sub.get_node("Panel").translatey = translatey
	sub.get_node("Panel").rect_position.y += translatey
	sub.get_node("Panel/Control").translatey = translatey

# Input menu button press
func _on_Input_pressed():
	var sub = create_submenu(inputmenu)
	sub.get_node("Panel").translatey = translatey
	sub.get_node("Panel").rect_position.y += translatey
	sub.get_node("Panel/Control").translatey = translatey

# Runs when the menu is added to the scene tree.
# Stores the camera translation and rotation, populates the map point list
# and starts the animations/sounds
func onReady():
	items[0].grab_focus()
	$Options/HelpClose.translatey = translatey
	rect_global_position = Vector2(
							(1920-$Options.rect_size.x) * 0.5, 
							(1080-$Options.rect_size.y) * 0.5 + translatey
							)
	_v_scroll = get_node("Options/ScrollContainer")
	_v_scroller = _v_scroll.get_node("_v_scroll")
	main = get_tree().get_root().get_node("Main/ViewportContainer/Viewport")
	$Options/HelpClose.base_parent = self
	if !anim_exec:
		modulate.a = 0
		anim_exec = true
		$AnimationPlayer.play("Load")
		
# If presses the quit button, kills the game.
func _on_Quit_pressed():
	get_tree().quit()
	
# Handles cancelling and opening the menu.
# Note: if you kill the menu with the key, you can escape any number of menus.
# But if its with a controller, it acts as "accept" instead and only will
# destroy if you're on the base menu.
func _unhandled_input(event):
	if !viewing && !warping:
		if Input.is_action_just_pressed("ui_cancel"):
			if(!disabled):
				Input.action_release("ui_cancel")
				_on_CancelButton_pressed()
		# only close the menu tree with Key
		if event is InputEventKey:
			if Input.is_action_just_pressed("open_menu"):
					if(!destroyed):
						Input.action_release("open_menu")
						$AnimationPlayer.play("Unload")
						$Cancel.play()
						destroyed = true
		else:
			if(!disabled):
				if Input.is_action_just_pressed("open_menu"):
						if(!destroyed):
							Input.action_release("open_menu")
							$AnimationPlayer.play("Unload")
							$Cancel.play()
							destroyed = true
				
# If the animation is finished, kill itself
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		queue_free()
			
# Unload if the cancel button is pressed
func _on_CancelButton_pressed():
	if base_parent.has_method("regrab"):
		base_parent.regrab()
	$AnimationPlayer.play("Unload")
	$Cancel.play()

# Same thing as _on_CancelButton_Pressed()
func _on_Close_pressed():
	_on_CancelButton_pressed()
