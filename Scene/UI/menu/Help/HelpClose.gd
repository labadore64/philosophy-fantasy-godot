## Help Close
## The little ? X menu on the top of menus.
extends Control

var base_parent = null		# The parent of this control's accessible node.
var offset_x = 0			# How much to offset the help menu x.
var offset_y = 0			# How much to offset the help menu y.
var help_menu = null		# The reference to the opened help menu.
var resource_name = "res://help/Default.tres"	# Name of the help resource.

var translatey = 0

# Help menu preloaded object
onready var menuobj = preload("res://Scene/UI/menu/Help/Help.tscn")

# Determines if the help button displays at start
func _ready():
	configureHelp()

# When the button for help is pressed.
func _on_Button_pressed():
	displayHelp()

# When the button for closing is pressed. Requires parent to have doClose().
func _on_Button2_pressed():
	if get_parent().has_method("doClose"):
		get_parent().doClose()
		
# determines if the help button displays.
# If you have help not enabled, it doesnt display. Or if the resource sucks
func configureHelp():
	if !Global.help_enabled || resource_name == "":
		$GridContainer/HelpButton.queue_free()
	
# Displays the help menu with its contents.
func displayHelp():
	if !is_instance_valid(help_menu):
		var loader = load(resource_name)
		if loader != null:
			var help = menuobj.instance()
			help.translatey = translatey
			add_child(help)
			help_menu = help
			help.populate(loader.help_name,loader.help_desc)
			var base = base_parent;
			
			if base_parent is Panel:
				base = base_parent.get_parent()
			
			if is_instance_valid(base):
				help.base_parent = base
				base.help_open = true
				base.help_mode = true
				base.disable()
			help.rect_position.x += offset_x
			help.rect_position.y += offset_y
			get_tree().set_input_as_handled()
	else:
		help_menu.closePanel()
