## HelpContents
## Represents the contents of a Help entry.

extends "res://access/NewAccessible.gd"

var translatey = 0

# Just plays the loading animation on load.
func onReady():
	rect_global_position = Vector2(
							(1920-$Panel.rect_size.x) * 0.5 - 20, 
							(1080-$Panel.rect_size.y) * 0.5 - 20 + translatey
							)
	$AnimationPlayer.play("Load")
	$Open.play()

# Sets the text of the help contents and also reads them aloud.
# Note: Godot uses BBCode for Rich Text, some of which is stripped from the
# text when read by TTS.
func setText(label,text):
	$Panel/Label.text = label
	$Panel/Label2.bbcode_text = text
	Global.tts_stop()
	var texter = text
	
	texter = texter.replace("[b]","").replace("[i]","").replace("[/b]","").replace("[/i]","")
	
	Global.tts_say(label + ": " + texter)

# Cancels out on cancel/select input signal.
func _unhandled_input(event):
	if Input.is_action_just_pressed("ui_cancel") || Input.is_action_just_pressed("ui_select"):
		if(!disabled):
			Input.action_release("ui_select")
			Input.action_release("ui_cancel")
			closePanel()

# Closes the panel
func closePanel():
	$AnimationPlayer.play("Unload")
	$Cancel.play()

# If the OK button is pressed, close the panel.
func _on_OKButton_pressed():
	closePanel()

# When the closing animation is done, it closes the menu.
func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "Unload"):
		queue_free()
