## DialogBoxHandler
## Handles commands from NPCs and other assorted things.
extends Node

# Preloaded Textbox scene
onready var textboxscn = preload("res://Scene/UI/dialog/DialogBox.tscn")
# Preloaded emotion popup scene
onready var emotescn = preload("res://Scene/UI/dialog/Emote.tscn")
# Preloaded message box scene
onready var msgboxscn = preload("res://Scene/UI/dialog/MessageBox.tscn")

onready var dialogActions = []			# Dialog actions to complete
onready var dialogPosition := -1		# Position within dialog actions
onready var destroyed = false;			# Whether or not this is destroyed
onready var tweencounter = 0			# How many tweens are completed.
onready var npc_parent = null			# What NPC triggered the DialogBoxHandler	
onready var player = null				# Reference to the player.
onready var monitor = null				# Watches for an animation to complete
onready var emote = null				# Emotion for 

# Reference to Main
onready var main = get_tree().get_root().get_node("Main/ViewportContainer/Viewport")

onready var activeAction = null			# Which action is currently active.
onready var wait = -1.0					# Wait duration for wait action
onready var warping = false				# Whether or not the player is being warped
onready var tweentime = 0				# Tween time

onready var start_translation = Vector3.ZERO	# Initial translation for the camera.
onready var start_rotation =Vector3.ZERO		# Initial rotation for the camera.
onready var start_scale = Vector3.ONE			# Initial scale for the camera.

onready var start_player_rotation =Vector3.ZERO	# Initial rotation for the player.

# Captures the start values and sets the selected object in Main to null
func _ready():
	pass

# Starts a set of actions
func startAction(actions):
	dialogActions = actions
	
# Jumps to processDialog
func _process(delta):
	processDialog(delta)

# Performs an action when the monitor is set to null and the timer is complete,
# and you aren't warping or some shit like that
func processDialog(delta):
	if(!destroyed && !warping):
		if(monitor == null):
			if(wait <= -1):
				if(activeAction == null):
					dialogPosition+=1
					if(dialogPosition >= dialogActions.size()):
						destroy()
					else:
						var action = dialogActions[dialogPosition]
						performAction(action[0],action[1])
			else:
				wait -= delta
		else:
			if(monitor.player.is_playing()):
				monitor = null
	
# Destroys the DialogBoxHandler, fixes up that camera.
func destroy():
	if(!destroyed):
		destroyed = true
		queue_free()
	
# Performs an action based on its identifier.
# The args contain arguments for each action,
# which is unique to the action.
func performAction(action,args):
	
	if(action == "textbox"):
		if(args.size() == 1):
			doTextbox(args[0].character_name,args[0].sprite_name,args[0].text)
	elif(action == "msgbox"):
		if(args.size() == 1):
			doMsgbox(args[0])
	elif(action == "wait"):
		wait = args[0]*0.01
	elif(action == "sfx"):
		if(args.size() == 1):
			Global.playSoundFx(args[0])
	elif(action == "music"):
		if(args.size() == 1):
			main.get_node("MusicPlayer").playSong(args[0])

# Displays a message box.
func doMsgbox(text):
	activeAction = msgboxscn.instance()
	add_child(activeAction)
	activeAction.get_node("Box").visible = false
	activeAction.npc_parent = npc_parent
	activeAction.player = player
	activeAction.setText(text)

# Displays a text box
func doTextbox(name,img,texts):
	activeAction = textboxscn.instance()
	add_child(activeAction)
	activeAction.visible = false
	activeAction.setNameAndText(name,img,texts)
