## MessageBox
## Displays a message box, does not have dialog options.
extends Node2D

onready var text := ""; 		# Msgbox text
onready var npc_parent = null	# NPC parent of this textbox
onready var player = null		# Refrence to player

var clicked = false

# The max time for the countdown for displaying this text
onready var countdown_max = (Global.auto_continue_text_speed+3) * 20

onready var countdown = 0		# Countdown timer

var destroyed = false;			# Whether the box is destroyed

# Loads the animation and also clears
# the states of the keys that can move it forward
func _ready():
	#global_position = Vector2(
	#						(1920-$Box.rect_size.x) * 0.5, 
	#						(1080-$Box.rect_size.y) * 0.5
	#						)
	$OpenSound.play()
	$AnimationPlayer.play("Load")
	Input.action_release("ui_select")
	Input.action_release("interact")
	Input.action_release("ui_cancel")

# Updates the auto continue countdown
func _process(delta):
	if Input.is_action_just_pressed("ui_select") || Input.is_action_just_pressed("ui_accept") || Input.is_action_just_pressed("interact") || Input.is_action_just_pressed("ui_cancel"):
		if clicked:
			updateDisplayText()
		clicked = true
	if Global.auto_continue_text:
		countdown += delta * 60
		if(countdown > countdown_max):
			updateDisplayText()
			countdown = 0

# If it is playing the unload animation, destroy the msgbox.
func _on_AnimationPlayer_animation_finished(anim_name):
	if(get_node("AnimationPlayer").get_assigned_animation() == "Unload"):
		killTextbox()

# Set the text in the msgbox, and say it.
func setText(texts):
	$Box/Label.text = texts;
	Global.tts_say(texts)

# Destroys the msgbox.
func killTextbox():
	Global.tts_stop()
	get_parent().activeAction = null
	queue_free()
		
# Closes the msgbox (plays its animation)
func updateDisplayText():
	get_node("AnimationPlayer").play("Unload")

# If the animation is unload, play the sound.
func _on_AnimationPlayer_animation_started(anim_name):
	if(anim_name == "Unload"):
		$CancelSound.play()
