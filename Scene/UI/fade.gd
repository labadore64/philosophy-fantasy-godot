## fade
## Controls fade in and out.
extends Node2D

onready var mapkill = null 		# The stored map to destroy when fading is complete
onready var mapdest = null		# The stored map to enter when fading is complete
onready var mapspawn = ""		# The map spawn point to use when fading is complete

# Reference to main
onready var main = get_tree().get_root().get_node("Main")

# Perform a fade in.
func fadeIn():
	$AnimationPlayer.play("FadeIn")
	
# Perform a fade out.
func fadeOut():
	$AnimationPlayer.play("FadeOut")
	
# If mapdest is a Vector3, move the player to that position.
# Otherwise, load a new map based on the variable contents.
func _on_AnimationPlayer_animation_finished(anim_name):
	if mapdest is Vector3:
		# Warp Code for map positions
		var player = get_tree().get_root().get_node("Main/ViewportContainer/Viewport/Map/map/Player")
		var cam = get_tree().get_root().get_node("Main/ViewportContainer/Viewport/Map/map/Camera")
		var nav = get_tree().get_root().get_node("Main/ViewportContainer/Viewport/Map/map/Navigation")
		var camnav = get_tree().get_root().get_node("Main/ViewportContainer/Viewport/Map/map/CameraNav")
		var diff = player.translation - mapdest
		player.translation = mapdest
		cam.translation -= diff
		# forces the player/camera to fit into the designated areas
		player.translation = nav.get_closest_point(player.translation)
		cam.translation = camnav.get_closest_point(cam.translation)
		mapspawn.queue_free()
		main.emit_signal("loadMapTeleport")
		mapdest = null
		mapspawn = ""
	else:
		# warp code for changing maps/spawns
		if(mapkill != null):
			mapkill.saveMapPoints()
			mapkill.queue_free()
			mapkill = null
			if(mapdest != null):
				main.emit_signal("loadMap",mapdest,mapspawn)
				mapdest = null
				mapspawn = ""

