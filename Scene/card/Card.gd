extends Node2D

const MOVECARD_X = -350
const MOVECARD_Y = -675

var type_textures = [
	null,
	preload("res://texture/UI/card/elements1.png"),
	preload("res://texture/UI/card/elements2.png"),
	preload("res://texture/UI/card/elements3.png"),
	preload("res://texture/UI/card/elements4.png"),
	preload("res://texture/UI/card/elements5.png")
]

var element_colors = [
	Color(0,0,0),
	Color(.2,.2,.7),
	Color(.7,.2,.2),
	Color(.2,.7,.2),
	Color(.2,.2,.2),
	Color(1,1,1)
]

enum kind {
  NONE,
  PHILOSOPHER,
  EFFECT,
  METAPHYSICS,
}

enum element {
	NONE,
	MIND,
	HEART,
	MATTER
}

enum modifier {
	NONE,
	ONCEPERTURN,
	PERMADEATH
}

enum archetype {
	NONE,
	GREEK,
	PSYCH,
	MARXIST
}

var element_names = [
	"none",
	"Mind",
	"Heart",
	"Matter",
	"Praxis",
	"Theory"
]

var type_names = [
	"none",
	"Philosopher",
	"Praxis",
	"Theory"
]

var modifier_names = [
	"none",
	"Once-Per-Turn",
	"Permadeath"
]

var archetype_names = [
	"none",
	"Greek",
	"Psych",
	"Marxist"
]

var base_attack = 0
var attack = 0
var attack_overlay = 0
var attack_boost = 0
var base_defense = 0
var defense = 0
var defense_overlay = 0
var defense_boost = 0
var card_name = ""
var card_type = 0
var card_element = 0
var weakness = 0
var resist = 0
var weakness_amount = 0
var resist_amount = 0
var modifiers = []
var archetypes = []
var card_text = ""
var card_flavor = ""
var overlayed_cards = []
var overlay_count = 1
var card_script

var hidemode = false
var parent = null

# Called when the node enters the scene tree for the first time.
func _ready():
	populate("guattari")

func copy(copie):
	if copie != null:
		attack_boost = copie.attack_boost
		defense_boost = copie.defense_boost
		base_attack = copie.base_attack
		base_defense = copie.base_defense
		attack_overlay = copie.attack_overlay
		defense_overlay = copie.defense_overlay
		card_name = copie.card_name
		card_type = copie.card_type
		card_element = copie.card_element
		$CardSprite.texture = copie.get_node("CardSprite").texture
		weakness = copie.weakness
		resist = copie.resist
		weakness_amount = copie.weakness_amount
		resist_amount = copie.resist_amount
		modifiers = copie.modifiers
		archetypes = copie.archetypes
		card_text = copie.card_text
		card_flavor = copie.card_flavor
		overlay_count = copie.overlay_count
		overlayed_cards = copie.overlayed_cards
		card_script = copie.card_script
		parent = copie.parent
		updateDisplay()
		
func populate(reference_name):
	if card_name == "":
		visible = false
		var data = load("res://data/card/"+reference_name+".tres")
		base_attack = data.attack
		base_defense = data.defense
		attack_overlay = data.overlay_attack
		defense_overlay = data.overlay_defense
		card_name = data.card_name
		card_type = data.card_type
		card_element = data.card_element
		$CardSprite.texture = data.art
		weakness = data.weakness
		resist = data.resist
		weakness_amount = data.weakness_amount
		resist_amount = data.resist_amount
		modifiers = data.modifiers
		archetypes = data.archetypes
		card_text = data.card_text
		card_flavor = data.card_flavor
		overlay_count = data.overlay_count
		card_script = data.battle_script
		updateDisplay()
	
func resetBoosts():
	attack_boost = 0
	defense_boost = 0
	updateDisplay()
	
func updateDisplay():
	$Card.self_modulate = element_colors[card_element]
	$Name.text = card_name
	$Name/Element.texture = type_textures[card_element]
	attack = base_attack + attack_boost
	defense = base_defense + defense_boost
	for c in overlayed_cards:
		attack += c.attack_overlay
		defense += c.defense_overlay
	$Attack.text = str(attack) + "/+" + str(attack_overlay)
	$Defense.text = str(defense) + "/+" + str(defense_overlay)
	$Flavor.text = card_flavor
	if archetypes.size() == 1:
		$Archetype.text = archetype_names[archetypes[0]]
	else:
		$Archetype.text = archetype_names[archetypes[0]] + "/" + archetype_names[archetypes[1]]
	var modifier = ""
	for c in modifiers:
		modifier += modifier_names[c] + ","
	if modifier != "":
		modifier.erase(modifier.length() - 1, 1)
	$Modifier.text = modifier
	$Desc.text = card_text
	$Weak.text = "Weak +" + str(weakness_amount)
	$Weak/Element.texture = type_textures[weakness]
	$Resist.text = "Resist +" + str(resist_amount)
	$Resist/Element.texture = type_textures[resist]


func _on_Node2D_item_rect_changed():
	if scale.x < 0.75 || scale.y < 0.75:
		$Archetype.visible = false
		$Modifier.visible = false
		$Desc.visible = false
	else:
		$Archetype.visible = true
		$Modifier.visible = true
		$Desc.visible = true

func showCard():
	visible = true
	$AnimationPlayer.play("Load")

func hideCard():
	$AnimationPlayer.play("Unload")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Unload":
		if hidemode:
			if get_parent() != null:
				get_parent().remove_child(self)
				visible = false
				hidemode = false
		else:
			position.x = MOVECARD_X
			position.y = MOVECARD_Y
	if anim_name == "Bench" || anim_name == "Activate" || anim_name == "Overlay" || anim_name == "Use":
		if get_parent() != null:
			get_parent().remove_child(self)
