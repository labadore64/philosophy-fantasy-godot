extends Control

onready var cards = []
var selected = false
var complete = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func populate(reference_name):
	if !complete:
		var data = load("res://data/deck/"+reference_name+".tres")
		cards = data.cards;
		$Control/Deck/Name.text = data.deck_name
		$Control/Deck/Subtitle.text = data.subtitle
		$Control/CardArt.texture = data.art
		$Control/Deck.self_modulate = data.color

func selected():
	if !complete:
		$AnimationPlayer.play("selected")
	
func unselected():
	if !complete:
		$AnimationPlayer.play("RESET")

func dismiss():
	$AnimationPlayer.play("nope")

func _on_Button_pressed():
	if !get_parent().selected:
		get_parent().selected = true
		selected = true
		get_parent().doneDeck()
		Global.playSoundFx("card/deckselect")

func grabFocus():
	$Button.grab_focus()

func _on_Button_focus_entered():
	if !get_parent().selected:
		Global.playSoundFx("card/card_choose")
		var text = $Control/Deck/Name.text
		if Global.TTS_verbose:
			text += " " + $Control/Deck/Subtitle.text
		Global.tts_say(text)
		for c in get_parent().items:
			c.unselected()
		selected()


func _on_Button_mouse_entered():
	if !complete:
		grabFocus()
