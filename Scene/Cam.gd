extends Camera2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var activeAction = null # added so i can use messagebox without major edits

# Called when the node enters the scene tree for the first time.
func _ready():
	$ColorRect/zone.text = ""
	resetText()

func resetText():
	$ColorRect/Label.text = ""
	$ColorRect/Attack.text = ""
	$ColorRect/Defence.text = ""
	$ColorRect/TextureRect.visible = false
	
	
func updateCardText(card):
	$ColorRect/zone.text = ""
	if card != null:
		if card.location.name == "Deck" || card.location.name == "Discard" || card.location.name == "Limbo":
			if card.location.name == "Deck" && card.location.get_parent().enemy:
				$ColorRect/zone.text = card.location.name 
			else:
				$ColorRect/zone.text = card.location.name + ": " + str(card.location.cards.size())
		else:
			$ColorRect/zone.text = card.location.name
			if card.location.name == "Active":
				if card.location.cards.find(card) > 0:
					$ColorRect/zone.text = "Overlay"
		if card.flipped || card.location.name == "Limbo" || card.location.name == "Discard":
			resetText()
		else:
			$ColorRect/TextureRect.visible = true
			$ColorRect/TextureRect.texture.current_frame = card.card_ref.card_element
			$ColorRect/Label.text = card.card_ref.card_name
			$ColorRect/Attack.text = str(card.card_ref.attack) + "/+" + str(card.card_ref.attack_overlay)
			$ColorRect/Defence.text = str(card.card_ref.defense)+"/+"+ str(card.card_ref.defense_overlay)
	else:
		resetText()

# Does the complete battle animation
func doDialogAnimation():
	var ref1 = null
	var ref2 = null
	var reff1 = null
	var reff2 = null
	var duel = get_parent()
	
	duel.addAction("push",0)
	
	var other_player = (duel.current_player+1) % 2
	# this displays the card for battle
	$ColorRect2/YourCard.visible = false
	$ColorRect2/EnemyCard.visible = false
	if get_parent().players[duel.current_player].get_node("Active").cards.size():
		reff1 = get_parent().players[duel.current_player].get_node("Active").cards[0].card_ref
	if get_parent().players[0].get_node("Active").cards.size() > 0:
		ref1 = get_parent().players[0].get_node("Active").cards[0].card_ref
		$ColorRect2/YourCard.visible = true
		$ColorRect2/YourCard.copy(ref1)
	if get_parent().players[other_player].get_node("Active").cards.size() > 0:
		reff2 = get_parent().players[other_player].get_node("Active").cards[0].card_ref
	if get_parent().players[1].get_node("Active").cards.size() > 0:
		ref2 = get_parent().players[1].get_node("Active").cards[0].card_ref
		$ColorRect2/EnemyCard.visible = true
		$ColorRect2/EnemyCard.copy(ref2)
	
	duel.addAnimation(self,"BattleLoad")
	duel.addAction("wait",0.5)
	
	# calculate the damage
	var attack = 0
	var defense = 0
	
	if reff1 != null:
		attack = reff1.attack
	
	if reff2 != null:
		defense = reff2.defense
		
	var test1 = 0
	var test2 = 0
	var supereffective = 0
	if reff1 != null && reff2 != null:
		# calculate weakness/resistance
		if reff1.card_element == reff2.weakness:
			attack += reff2.weakness_amount
			test1 = reff2.weakness_amount
		if reff1.card_element == reff2.resist:
			attack -= reff2.resist_amount
			test2 = reff2.resist_amount
			
		if test1 > test2:
			supereffective = 1
		elif test1 < test2:
			supereffective = 2
			
	var damage = attack - defense
			
	if abs(damage) > 0:
		if supereffective == 0:
			duel.addAction("sfx","card/duel_hit_neutral")
		elif supereffective == 1:
			duel.addAction("sfx","card/duel_hit_good")
		elif supereffective == 2:
			duel.addAction("sfx","card/duel_hit_bad")
	
	if damage > 0:
		var shakeAnim = "ShakeYou"
		if duel.current_player == 0:
			shakeAnim = "ShakeEnemy"
		duel.addAnimation(self,shakeAnim)
		duel.addAnimation(self,"BattleUnload")
		duel.addAction("wait",0.15)
		if other_player == 0:
			duel.addAction("sfx","card/duel_you_hit")
			duel.addAction("wait",0.5)
			$damageYou.text = "-" + str(damage)
			
			var player = duel.players[0]
			player.get_node("Active").moveCard(
											player.get_node("Active").cards.size()-1,
											player.get_node("Limbo")
											)
		else:
			duel.addAction("sfx","card/duel_enemy_hit")
			duel.addAction("wait",0.5)
			$damageEnemy.text = "-" + str(damage)
			var player = duel.players[1]
			player.get_node("Active").moveCard(
											player.get_node("Active").cards.size()-1,
											player.get_node("Limbo")
											)	
		duel.addAction("hp",[duel.players[other_player],abs(damage)])
		var nameer = ""
		if other_player == 0:
			nameer = $YourHealth/Panel/Panel/Label.text
		else:
			nameer = $EnemyHealth/Panel/Panel/Label.text
		var actual_string = ""
		if damage > 0:
			var format_string = "%s took %s damage!"
			actual_string = format_string % [nameer, str(damage)]
		
		duel.addAction("tts",actual_string)
		
		duel.addAction("wait",0.5)
	elif damage < 0:
		var shakeAnim = "ShakeYou"
		if duel.current_player == 1:
			shakeAnim = "ShakeEnemy"
		duel.addAnimation(self,shakeAnim)
		duel.addAnimation(self,"BattleUnload")
		duel.addAction("wait",0.15)
		if duel.current_player == 0:
			duel.addAction("wait",0.15)
			duel.addAction("sfx","card/duel_you_hit")
			$damageYou.text = "-" + str(abs(damage))
			var player = duel.players[0]
			duel.addAction("hp",[duel.players[duel.current_player],abs(damage)])
			duel.addAction("wait",0.5)
			player.get_node("Active").moveCard(
											player.get_node("Active").cards.size()-1,
											player.get_node("Limbo")
											)
		else:
			duel.addAction("wait",0.15)
			duel.addAction("sfx","card/duel_enemy_hit")
			$damageEnemy.text = "-" + str(abs(damage))
			var player = duel.players[1]
			duel.addAction("hp",[duel.players[duel.current_player],abs(damage)])
		
			var nameer = ""
			if duel.current_player == 0:
				nameer = $YourHealth/Panel/Panel/Label.text
			else:
				nameer = $EnemyHealth/Panel/Panel/Label.text
			var actual_string = ""
			if damage > 0:
				var format_string = "%s took %s damage!"
				actual_string = format_string % [nameer, str(damage)]
			
			duel.addAction("tts",actual_string)
			
			duel.addAction("wait",0.5)
			player.get_node("Active").moveCard(
											player.get_node("Active").cards.size()-1,
											player.get_node("Limbo")
											)
		duel.addAction("wait",0.5)
	else:
		duel.addAction("wait",1.65)
		duel.addAnimation(self,"BattleUnload")
	
	var playa = [null,null]
	playa[0] = duel.players[duel.current_player]
	playa[1] = duel.players[(duel.current_player+1) % 2]
	
	var cando = true
	
	# Do card effects that trigger before battle
	for c in playa:
		var activea = c.get_node("Active")
		if activea.cards.size() > 0:
			if activea.cards[0].doActiveAfterBattleTest():
				activea.cards[0].doActiveAfterBattle()
		for g in range(1,activea.cards.size()):
			if activea.cards[g].doOverlayAfterBattleTest():
				activea.cards[g].doOverlayAfterBattle()
	

	duel.addAction("phase",0)
	if !duel.act_working:
		duel.batchMovement()
