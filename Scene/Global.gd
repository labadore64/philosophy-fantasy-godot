## Global
## Represents global variables and functions. 
## If there's an option in the menu, it resides here.

extends Node

# Cutscene preloaded scene
onready var cutscne = preload("res://map/gameplay/Cutscene.tscn")

onready var loaded_duel = ""

# visual settings
onready var show_screen = true			# Whether to show the screen.
onready var camera_follow = true		# Whether control follows the camera.
onready var show_portrait = true		# Whether to show portraits in convos.
onready var window_width = 1920			# Window width.
onready var window_height = 1080		# Window height.
onready var enable_zoom = true			# Enables the magnifier.
onready var pan_inverse = false			# Whether or not the magnifier's inputs
										# are reversed.
onready var colorblind_mode = true		# colorblind mode

onready var brightness = 0				# Brightness of 3D scene
onready var contrast = 0				# Contrast of 3D scene
onready var saturation = 0				# Saturation of 3D scene
onready var render_distance = 0			# Render distance of 3D scene

# TTS settings
onready var tts_last = ""				# Last text spoken by TTS.
onready var TTS_enabled = false;		# Whether or not TTS is enabled.
onready var TTS_verbose = true			# Whether or not verbose TTS is enabled.

# movement settings
onready var run_toggle = false;			# Automatically runs if toggled.
onready var axis_reverse = false		# Reverses the axises used for movement/panning

# Accessibility Settings
onready var acc_sound = false;			# Whether access sounds are enabled.
onready var wall_sound = true			# Whether the wall detection sound is enabled.
onready var bonk_sound = true			# Whether wall bonk sounds are enabled.
onready var help_enabled = true			# Whether help is enabled.

onready var camera_dist = 839.7			# Default camera distance.

# Mouse settings
onready var mouse_enabled = true		# Whether mouse is enabled.
onready var mouse_picking_enabled = true	# Whether object picking is enabled.


# text settings
onready var textSpeed = 1.0				# Text display speed.

# text delay
onready var auto_continue_text = false		# Whether text will auto-continue.
onready var auto_continue_text_speed = 5	# Speed of text auto-continue.

# Audio buses
onready var musicBus = AudioServer.get_bus_index("Music")		# The music bus.
onready var sfxBus = AudioServer.get_bus_index("SoundEffect")	# The Sound Effects bus.
onready var accBus = AudioServer.get_bus_index("AccessSound")	# The access bus.
onready var ambBus = AudioServer.get_bus_index("Ambience")	# The ambience bus.
onready var masterBus = AudioServer.get_bus_index("Master")		# The master bus.

onready var introAnimationPlayed = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Sets the screen visible or invisible.
func setScreenVisible(_show_screen):
	show_screen = _show_screen
	var main = get_tree().get_root().get_node("Main")
	var mapper = main.get_node("Map")
	var camera = null
	if(mapper != null):
		camera = mapper.get_node("map").get_node("Camera")
	if(camera != null):
		if show_screen:
			camera.far = camera_dist
		else:
			camera.far = 0.001
		
# Get-Set Definition for Music Volume
onready var musicVolume = 1.0 setget setMusicVolume, getMusicVolume

func setMusicVolume(volume):
	musicVolume = volume
	AudioServer.set_bus_volume_db(musicBus, linear2db(volume))

func getMusicVolume():
	return musicVolume
	
# Get-Set Definition for Sound Effect Volume
onready var sfxVolume = 1.0 setget setSfxVolume, getSfxVolume

func setSfxVolume(volume):
	sfxVolume = volume
	AudioServer.set_bus_volume_db (sfxBus, linear2db(volume))

func getSfxVolume():
	return sfxVolume
	
# Get-Set Definition for Access Volume
onready var accessVolume = 1.0 setget setAccVolume, getAccVolume

func setAccVolume(volume):
	accessVolume = volume
	AudioServer.set_bus_volume_db (accBus, linear2db(volume))

func getAccVolume():
	return accessVolume
	
# Get-Set Definition for Ambience Volume
onready var ambienceVolume = 1.0 setget setAmbVolume, getAmbVolume

func setAmbVolume(volume):
	ambienceVolume  = volume
	AudioServer.set_bus_volume_db (ambBus, linear2db(volume))

func getAmbVolume():
	return ambienceVolume 
	
# Get-Set Definition for Master Volume
onready var  masterVolume = 1.0 setget setMasterVolume, getMasterVolume

func setMasterVolume(volume):
	masterVolume = volume
	AudioServer.set_bus_volume_db (masterBus, linear2db(volume))

func getMasterVolume():
	return masterVolume
	
# Functions for save files, settings ect

# saves settings to a file
func saveSettings():
	var file = File.new()
	file.open("user://settings.dat", File.WRITE)
	file.store_float(Global.musicVolume)
	file.store_float(Global.sfxVolume)
	file.store_float(Global.accessVolume)
	file.store_float(Global.masterVolume)
	file.store_float(Global.textSpeed)
	file.store_8(int(Global.run_toggle))
	file.store_8(int(Global.TTS_enabled))
	file.store_8(int(Global.acc_sound))
	file.store_8(int(Global.wall_sound))
	file.store_8(int(Global.bonk_sound))
	file.store_8(int(OS.window_fullscreen))
	file.store_8(int(Global.auto_continue_text))
	file.store_8(Global.auto_continue_text_speed)
	file.store_float(TTS.rate)
	file.store_8(int(Global.camera_follow))
	file.store_8(int(Global.show_portrait))
	file.store_8(int(Global.enable_zoom))
	file.store_8(int(Global.pan_inverse))
	file.store_8(int(Global.TTS_verbose))
	file.store_8(int(Global.axis_reverse))
	file.store_8(int(Global.help_enabled))
	file.store_8(int(Global.mouse_enabled))
	file.store_8(int(Global.mouse_picking_enabled))
	file.store_16(Global.window_height)
	file.store_16(Global.window_width)
	file.store_float(Global.brightness)
	file.store_float(Global.contrast)
	file.store_float(Global.saturation)
	file.store_float(Global.render_distance)
	file.store_float(Global.ambienceVolume)
	file.close()
	
# loads settings from a file if it exists
func loadSettings():
	var file = File.new()
	if(file.file_exists("user://settings.dat")):
		file.open("user://settings.dat", File.READ)
		Global.musicVolume = file.get_float()
		Global.sfxVolume = file.get_float()
		Global.accessVolume = file.get_float()
		Global.masterVolume = file.get_float()
		Global.textSpeed = file.get_float()
		Global.run_toggle = bool(file.get_8())
		Global.TTS_enabled = bool(file.get_8())
		Global.acc_sound = bool(file.get_8())
		Global.wall_sound = bool(file.get_8())
		Global.bonk_sound = bool(file.get_8())
		OS.window_fullscreen = bool(file.get_8())
		Global.auto_continue_text = bool(file.get_8())
		Global.auto_continue_text_speed = file.get_8()
		TTS.rate = file.get_float()
		Global.camera_follow = bool(file.get_8())
		Global.show_portrait = bool(file.get_8()) 
		Global.enable_zoom = bool(file.get_8())
		Global.pan_inverse = bool(file.get_8())
		Global.TTS_verbose = bool(file.get_8())
		Global.axis_reverse = bool(file.get_8())
		Global.help_enabled = bool(file.get_8())
		Global.mouse_enabled = bool(file.get_8())
		Global.mouse_picking_enabled = bool(file.get_8())
		Global.window_height = file.get_16()
		Global.window_width = file.get_16()
		Global.brightness = file.get_float()
		Global.contrast = file.get_float()
		Global.saturation = file.get_float()
		Global.render_distance = file.get_float()
		Global.ambienceVolume = file.get_float()
		
		file.close()
		
		if window_height == 0:
			window_height = 1080
		if window_width == 0:
			window_width = 1920
		
		# afterward stuff
		setWindowSize()
		
	
	var config = ConfigFile.new()

	# Load data from a file.
	var err = config.load("access.ini")

	# If the file didn't load, ignore it.
	if err != OK:
		return

	# Iterate over all sections.
	for player in config.get_sections():
		Global.TTS_enabled = config.get_value(player,"tts")
		Global.acc_sound = config.get_value(player,"env_sound")
	
# Saves keyboard input
func saveKeyboardInput():
	var file = File.new()
	file.open("user://keyboard.dat", File.WRITE)
	saveKeyboardAction(file,"ui_accept")
	saveKeyboardAction(file,"ui_cancel")
	saveKeyboardAction(file,"open_menu")
	saveKeyboardAction(file,"ui_up")
	saveKeyboardAction(file,"ui_down")
	saveKeyboardAction(file,"ui_left")
	saveKeyboardAction(file,"ui_right")
	saveKeyboardAction(file,"ui_focus_next")
	saveKeyboardAction(file,"ui_focus_previous")
	saveKeyboardAction(file,"move_up")
	saveKeyboardAction(file,"move_down")
	saveKeyboardAction(file,"move_left")
	saveKeyboardAction(file,"move_right")
	saveKeyboardAction(file,"run")
	saveKeyboardAction(file,"run_toggle")
	saveKeyboardAction(file,"interact")
	saveKeyboardAction(file,"tts_stop")
	saveKeyboardAction(file,"tts_repeat")
	saveKeyboardAction(file,"tts_look")
	saveKeyboardAction(file,"tts_read_coordinates")
	saveKeyboardAction(file,"access_tooltip")
	saveKeyboardAction(file,"zoom_in")
	saveKeyboardAction(file,"zoom_out")
	saveKeyboardAction(file,"pan_left")
	saveKeyboardAction(file,"pan_right")
	saveKeyboardAction(file,"pan_up")
	saveKeyboardAction(file,"pan_down")
	saveKeyboardAction(file,"map_place")
	saveKeyboardAction(file,"map_menu")
	file.close()
	
# Saves a single keyboard action
func saveKeyboardAction(file,action):
	var obj = InputMap.get_action_list(action)
	for o in obj:
		if o is InputEventKey:
			file.store_32(o.scancode)	
			
# Loads keyboard input
func loadKeyboardInput():
	var file = File.new()
	if(file.file_exists("user://keyboard.dat")):
		file.open("user://keyboard.dat", File.READ)
		loadKeyboardAction(file,"ui_accept")
		loadKeyboardAction(file,"ui_cancel")
		loadKeyboardAction(file,"open_menu")
		loadKeyboardAction(file,"ui_up")
		loadKeyboardAction(file,"ui_down")
		loadKeyboardAction(file,"ui_left")
		loadKeyboardAction(file,"ui_right")
		loadKeyboardAction(file,"ui_focus_next")
		loadKeyboardAction(file,"ui_focus_previous")
		loadKeyboardAction(file,"move_up")
		loadKeyboardAction(file,"move_down")
		loadKeyboardAction(file,"move_left")
		loadKeyboardAction(file,"move_right")
		loadKeyboardAction(file,"run")
		loadKeyboardAction(file,"run_toggle")
		loadKeyboardAction(file,"interact")
		loadKeyboardAction(file,"tts_stop")
		loadKeyboardAction(file,"tts_repeat")
		loadKeyboardAction(file,"tts_look")
		loadKeyboardAction(file,"tts_read_coordinates")
		loadKeyboardAction(file,"access_tooltip")
		loadKeyboardAction(file,"ui_help")
		loadKeyboardAction(file,"ui_close")
		loadKeyboardAction(file,"zoom_in")
		loadKeyboardAction(file,"zoom_out")
		loadKeyboardAction(file,"pan_left")
		loadKeyboardAction(file,"pan_right")
		loadKeyboardAction(file,"pan_up")
		loadKeyboardAction(file,"pan_down")
		loadKeyboardAction(file,"map_place")
		loadKeyboardAction(file,"map_menu")
		file.close()

# Loads a single keyboard action
func loadKeyboardAction(file,action):
	var obj = InputMap.get_action_list(action)
	for o in obj:
		if o is InputEventKey:
			o.scancode = file.get_32()

# Saves controller input
func saveGamepadInput():
	var file = File.new()
	file.open("user://Gamepad.dat", File.WRITE)
	saveGamepadAction(file,"ui_accept")
	saveGamepadAction(file,"ui_cancel")
	saveGamepadAction(file,"open_menu")
	saveGamepadAction(file,"ui_up")
	saveGamepadAction(file,"ui_down")
	saveGamepadAction(file,"ui_left")
	saveGamepadAction(file,"ui_right")
	saveGamepadAction(file,"ui_focus_next")
	saveGamepadAction(file,"ui_focus_previous")
	saveGamepadAction(file,"move_up")
	saveGamepadAction(file,"move_down")
	saveGamepadAction(file,"move_left")
	saveGamepadAction(file,"move_right")
	saveGamepadAction(file,"run")
	saveGamepadAction(file,"run_toggle")
	saveGamepadAction(file,"interact")
	saveGamepadAction(file,"tts_stop")
	saveGamepadAction(file,"tts_repeat")
	saveGamepadAction(file,"tts_look")
	saveGamepadAction(file,"tts_read_coordinates")
	saveGamepadAction(file,"access_tooltip")
	saveGamepadAction(file,"map_place")
	saveGamepadAction(file,"map_menu")
	file.close()

# Saves a single controller action
func saveGamepadAction(file,action):
	var obj = InputMap.get_action_list(action)
	for o in obj:
		if o is InputEventKey:
			file.store_8(o.button_index)

# Loads Gamepad input
func loadGamepadInput():
	var file = File.new()
	if(file.file_exists("user://Gamepad.dat")):
		file.open("user://Gamepad.dat", File.READ)
		loadGamepadAction(file,"ui_accept")
		loadGamepadAction(file,"ui_cancel")
		loadGamepadAction(file,"open_menu")
		loadGamepadAction(file,"ui_up")
		loadGamepadAction(file,"ui_down")
		loadGamepadAction(file,"ui_left")
		loadGamepadAction(file,"ui_right")
		loadGamepadAction(file,"ui_focus_next")
		loadGamepadAction(file,"ui_focus_previous")
		loadGamepadAction(file,"move_up")
		loadGamepadAction(file,"move_down")
		loadGamepadAction(file,"move_left")
		loadGamepadAction(file,"move_right")
		loadGamepadAction(file,"run")
		loadGamepadAction(file,"run_toggle")
		loadGamepadAction(file,"interact")
		loadGamepadAction(file,"tts_stop")
		loadGamepadAction(file,"tts_repeat")
		loadGamepadAction(file,"tts_look")
		loadGamepadAction(file,"tts_read_coordinates")
		loadGamepadAction(file,"access_tooltip")
		loadGamepadAction(file,"ui_help")
		loadGamepadAction(file,"ui_close")
		loadGamepadAction(file,"map_place")
		loadGamepadAction(file,"map_menu")
		file.close()
		
# Loads a single controller action
func loadGamepadAction(file,action):
	var obj = InputMap.get_action_list(action)
	for o in obj:
		if o is InputEventKey:
			o.button_index = file.get_8()


# Sound and Music players

# Play a sound effect.
func playSoundFx(resource):
	get_node("AudioStream").stream = load("res://sound/sfx/"+ resource +".wav")
	get_node("AudioStream").play()
	
# Play a sound effect with the resource
func playSoundFxWithResource(resource):
	get_node("AudioStream").stream = resource
	get_node("AudioStream").play()
	

# Play ambience sound effect.
func playAmbience(resource):
	get_node("AmbiencePlayer").stream = load("res://sound/sfx/"+ resource +".wav")
	get_node("AmbiencePlayer").play()
	
# Play ambience sound effect with the resource
func playAmbienceWithResource(resource):
	get_node("AmbiencePlayer").stream = resource
	get_node("AmbiencePlayer").play()
	
# Stops ambience sound effect
func ambienceStop():
	get_node("AmbiencePlayer").stop()
	
# Play a song
func playSong(resource):
	get_node("MusicPlayer").playSong(resource)
	
func stopSong():
	get_node("MusicPlayer").current_song = ""
	get_node("MusicPlayer").stop()
# Text to Speech Methods

# Talk with TTS.
func tts_say(text):
	if(text != ""):
		tts_stop()
		if(Global.TTS_enabled):
			tts_last = text;
			TTS.speak(text,false)

# Stop TTS.
func tts_stop():
	if(Global.TTS_enabled):
		TTS.stop()
		
# Read off map coordinates.
func readCoordinates(x,z):
	x = floor(x)
	z = floor(z)
	tts_say("X: " + str(x) + ", Z: " + str(z))
	
# set Window size
func setWindowSize():
	OS.window_size = Vector2(window_width,window_height)
	var screensize = OS.get_screen_size()
	OS.window_position = Vector2((screensize.x-window_width) * 0.5, (screensize.y-window_height) * 0.5)

# Configures the mouse filter for all children of x.
func mouseConfigure(x):
	if mouse_enabled:
		var child = x.get_children()
		for c in child:
			if c is Control:
				if c is ItemList || c is VBoxContainer || c is LineEdit || c is CheckBox || c is Range || c is Button || c is TabContainer:
					c.mouse_filter = c.MOUSE_FILTER_PASS
				else:
					c.mouse_filter = c.MOUSE_FILTER_IGNORE
			mouseConfigure(c)
	else:
		var child = x.get_children()
		for c in child:
			if c is Control:
				c.mouse_filter = c.MOUSE_FILTER_IGNORE
			mouseConfigure(c)

func doCutscene(scene_name):
	# free the current map
	var last = get_tree().get_root().get_node("Main").get_child(1).filename
	get_tree().get_root().get_node("Main").get_child(1).queue_free();
	var instance = cutscne.instance()
	var cutscene = load("res://data/cutscene/"+scene_name+".tres")
	instance.get_node("Background").texture = cutscene.background
	instance.actions = cutscene.commands;
	if cutscene.next_scene != null:
		instance.next = cutscene.next_scene.resource_path
		instance.duel_name = cutscene.duel
		if loaded_duel != "":
			instance.duel_name = loaded_duel
			loaded_duel = ""
	instance.goToLast = cutscene.return_last
	instance.last = last
	instance.music = cutscene.music
	instance.ambience = cutscene.ambience
	playSong(instance.music)
	playAmbienceWithResource(instance.ambience)
	
	instance.next_cutscene = cutscene.next_cutscene
	get_tree().get_root().get_node("Main").add_child(instance)
