## Main
## The base node for the whole game. Also manages map stuff.

extends Node2D


signal opendialog(actions, obj,player)		# Displays the actions from the source obj. Passes player as reference.
signal dialog(name, texts)					# Displays a dialog box in the overworld.

signal fadein()								# Fades the screen in.
signal fadeout()							# Fades the screen out.
signal fadeoutEndMap()						# Fades out the end of a map.
signal fadeoutTeleportMap()					# Fades out to teleport within a map.
signal loadMap()							# Loads the stored map file.
signal loadMapTeleport()					# Loads the stored teleport warp.
signal openMsgbox(text)						# Shows a message box on the screen.
signal settingsTrigger(setting)				# When the settings changes are triggered.

var mouse_scroll_value = 0

const MOUSE_SENSITIVITY_SCROLL_WHEEL = 0.08	# The amount of sensitivity to the scroll wheel.
											# Maybe make a variable?
const ZOOMSPEED = 0.05						# How fast the magnifier zooms.
											# Maybe make a variable?										
const PANSPEED = 20							# How fast the panning in the magnifier moves.
											# Maybe make a variable?
const DEADZONE_ZOOM = 0.3					# Deadzone for zooming with the controller axis.
const DEADZONE_PAN = 0.5					# Deadzone for panning with the controller axis.
const PAN_DIST = 5							# How much the pan moves with the axis.
const startmap = "MainMenu"					# First loaded map. (will be a title screen later)

# Message box preloaded scene
onready var msgboxscn = preload("res://Scene/UI/dialog/MessageBox.tscn")

onready var dialogbox = null				# The currently opened dialog handler.
onready var can_input = true				# Forces the player to be unable to input if true.
onready var mapwaiter = -1					# Countdown for loading the map.
onready var main_mapname = null				# Represents the map being loaded. Is a loaded scene.
onready var main_mapspawn = ""				# The string of the Spawn Point for the loaded map.
onready var selected_object = null			# If an object is selected with object picking.
onready var click_read_object = null		# The object read last by TTS through mouse.
onready var click_read_object_last = null	# The object before that one!
onready var mousemove_mode = false			# Detemines when the mouse is doing magnifier stuff.
onready var executor = self					# Determines if this object or "MenuHUD" has control.
onready var mouseposlast = get_global_mouse_position() # Last mouse position.
onready var zoom_mode = false				# Whether the magnifier is in zoom mode.
onready var zoom_wait = -1.1				# Delay variable for zoom_mode.

onready var rotate_axis = [0,0]				# Axis input data.

# Loads when inserted into the scene tree
func _ready():
	main_mapname = load("res://map/"+startmap+".tscn")
	Global.loadSettings()
	Global.loadKeyboardInput()
	
	# If screen reader is detected, automatically enables it.
	if(TTS._get_can_detect_screen_reader()):
		if(TTS._get_has_screen_reader()):
			Global.TTS_enabled = true
			
	loadMap()
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(mapwaiter > -1):
		if(mapwaiter > 0):
			mapwaiter-=1
		else:
			mapwaiter=-1
			loadMap()
	if zoom_wait > -1 :
		zoom_wait -= delta*60

# Creates a fade in sequence.
func _on_Main_fadein(mapdest=null):
	$ViewportContainer/Viewport/Fade.visible = true;
	$ViewportContainer/Viewport/Fade.mapdest = mapdest
	$ViewportContainer/Viewport/Fade/AnimationPlayer.play("FadeIn")
	can_input = false

# Creates a fadeout sequence.
func _on_Main_fadeout(mapdest=null,mapspawn=""):
	$ViewportContainer/Viewport/Fade.visible = true;
	$ViewportContainer/Viewport/Fade.mapdest = mapdest
	$ViewportContainer/Viewport/Fade.mapspawn = mapspawn
	$ViewportContainer/Viewport/Fade/AnimationPlayer.play("FadeOut")
	can_input = false

# Fades out at the end of a map.
func _on_Main_fadeoutEndMap(mapdest=null,mapspawn="",songfade=true):
	if(is_instance_valid(dialogbox)):
		dialogbox.queue_free()
		dialogbox = null
	$ViewportContainer/Viewport/Fade.mapkill = $ViewportContainer/Viewport/Map
	#if(songfade):
		#Global.get_node("MusicPlayer").fadeout()
	_on_Main_fadeout(mapdest,mapspawn);
	
# Loads a new map.
func loadMap():
	var scene = main_mapname
	var instance = scene.instance()
	var respath = main_mapname.resource_path.get_file().get_basename()

	add_child(instance)
	
# When a map is loaded.
func _on_Main_loadMap(mapname,mapspawn=""):
	mapwaiter = 1
	main_mapname = mapname
	main_mapspawn = mapspawn

# Unhandled input from pretty much everything in the game.

# Oh and it also contains some TTS controls that can be used in the whole game.
func _unhandled_input(event):
	
	# Gets rotate_axis values
	if event is InputEventJoypadMotion:
		var adder = 0
		if Global.axis_reverse:
			adder = 2
		if event.axis < 4-adder && event.axis >= 2-adder:
			rotate_axis[event.axis-(2-adder)] = event.axis_value
		
	# TTS Stop and TTS Repeat functions.
	if(Global.TTS_enabled):
		if Input.is_action_pressed("tts_stop"):
			TTS.stop()
		if Input.is_action_pressed("tts_repeat"):
			Global.tts_say(Global.tts_last)

# When the program exits, stop TTS.
func _on_Main_tree_exiting():
	TTS.stop()

# Opens a message box
func _on_Main_openMsgbox(text):
	dialogbox = msgboxscn.instance()
	add_child(dialogbox)
	dialogbox.setText(text)

# updates settings for ya
func _on_Main_settingsTrigger(setting):
	# once finished doing settings then save them to the file
	Global.saveSettings()
