# Resource: Warp
# Represents a resource that stores warp data.

extends Resource
class_name Duel

export(String) var opponent_name
export(Texture) var opponent_sprite
export(AudioStream) var music
export(AudioStream) var ambience
export(Dictionary) var opponent_player_sprite
export(Array,String) var cards
export(int) var max_difficulty
export(bool) var victory_screen
export(String) var next_cutscene
export(Resource) var next_scene

# Make sure that every parameter has a default value.
# Otherwise, there will be problems with creating and editing
# your resource via the inspector.
func _init(_opponent_name="",_next_cutscene="",_victory_screen=false,_opponent_sprite=null,_opponent_player_sprite={},
			_cards=[],_max_difficulty=1,_next_scene=null):
		opponent_name = _opponent_name
		opponent_sprite = _opponent_sprite
		opponent_player_sprite = _opponent_player_sprite
		cards = _cards
		max_difficulty = _max_difficulty
		victory_screen = _victory_screen
		next_scene = _next_scene
		next_cutscene=_next_cutscene

