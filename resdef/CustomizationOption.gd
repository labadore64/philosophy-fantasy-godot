# Resource: Warp
# Represents a resource that stores warp data.

extends Resource
class_name CustomizationOption

export(Array,String) var customize_name

# Make sure that every parameter has a default value.
# Otherwise, there will be problems with creating and editing
# your resource via the inspector.
func _init(_customize_name=[""]):
	customize_name = _customize_name
