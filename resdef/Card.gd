# Resource: Warp
# Represents a resource that stores warp data.

extends Resource
class_name Card

enum kind {
  NONE,
  PHILOSOPHER,
  EFFECT,
  METAPHYSICS,
}

enum element {
	NONE,
	MIND,
	HEART,
	MATTER
}

enum modifier {
	NONE,
	ONCEPERTURN,
	PERMADEATH
}

enum archetype {
	NONE,
	GREEK,
	PSYCH,
	MARXIST
}

export(String) var card_name	
export(kind) var card_type
export(element) var card_element			
export(Texture) var art		
export(int)	var attack
export(int) var overlay_attack
export(int) var defense
export(int) var overlay_defense
export(int) var overlay_count = 1
export(element) var weakness
export(int)	var weakness_amount
export(element) var resist
export(int) var resist_amount
export(Array,modifier) var modifiers
export(String, MULTILINE) var card_text
export(String, MULTILINE) var card_flavor
export(String, MULTILINE) var quote
export(Array,archetype) var archetypes
export(Script) var battle_script


# Make sure that every parameter has a default value.
# Otherwise, there will be problems with creating and editing
# your resource via the inspector.
func _init(_card_name="",_card_type=0,_card_element=0,_art=null,
			_attack=0,_overlay_attack=0,_defense=0,_overlay_defense=0,
			_weakness=0,_resist=0,_resist_amount=0,_weakness_amount=0,
			_card_text="",_card_flavor="",_modifiers=[],_archetypes=[],
			_script=null,_overlay_count=1,_quote=""):
	card_name = _card_name
	card_type = _card_type
	card_element = _card_element
	art = _art
	attack = _attack
	defense = _defense
	overlay_attack = _overlay_attack
	overlay_defense = _overlay_defense
	weakness = _weakness
	resist = _resist
	weakness_amount = _weakness_amount
	resist_amount = _resist_amount
	card_text = _card_text
	card_flavor = _card_flavor
	modifiers = _modifiers
	archetypes = _archetypes
	battle_script = _script
	overlay_count = _overlay_count
	quote = _quote
