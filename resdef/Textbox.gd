# Resource: Warp
# Represents a resource that stores warp data.

extends Resource
class_name Textbox

export(String) var character_name
export(String) var sprite_name
export(Array,String,MULTILINE) var text	

# Make sure that every parameter has a default value.
# Otherwise, there will be problems with creating and editing
# your resource via the inspector.
func _init(_character_name="",_sprite_name="",_text=[]):
	text = _text
	character_name = _character_name
	sprite_name = _sprite_name

