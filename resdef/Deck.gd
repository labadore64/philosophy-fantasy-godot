# Resource: Warp
# Represents a resource that stores warp data.

extends Resource
class_name Deck

export(String) var deck_name		# Name of deck
export(String) var subtitle			# Subtitle of Deck
export(Array,String) var cards 		# Cards in Deck
export(Resource) var art			# Card art for deck
export(Color) var color				# Color of Deck



# Make sure that every parameter has a default value.
# Otherwise, there will be problems with creating and editing
# your resource via the inspector.
func _init(_deck_name="",_subtitle="",_cards=[],_art=null,_color=Color()):
	deck_name = _deck_name
	subtitle = _subtitle
	cards = _cards
	art = _art 
	color = _color
