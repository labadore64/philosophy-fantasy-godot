# Resource: Warp
# Represents a resource that stores warp data.

extends Resource
class_name Cutscene

export(Array,Array) var commands	# Commands displayed in dialog
export(Resource) var next_scene		# Next scene to go to
export(bool) var return_last		# Returns back to the last map after completion
export(Texture) var background		# Background of the cutscene
export(String) var next_cutscene	# Next cutscene to display after this one.
export(String) var music			# Music of the next cutscene
export(String) var duel				# Duel ID of the next cutscene
export(AudioStream) var ambience	# Ambience of the next cutscene


# Make sure that every parameter has a default value.
# Otherwise, there will be problems with creating and editing
# your resource via the inspector.
func _init(_commands={},_next_scene=null,
			_return_last=false,_background=null,
			_next_cutscene="",_music=null,_ambience=null,
			_duel=""):
	commands = _commands
	next_scene = _next_scene
	return_last = _return_last
	background = _background
	next_cutscene = _next_cutscene
	music = _music
	ambience = _ambience
	duel = _duel
