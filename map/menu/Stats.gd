extends Control

var base_parent = null
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var line = 0

var loaded = false
var destroyed = false

var _v_scroll = null
const VSCROLLAMOUNT = 60

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("Load")
	$CharacterPortrait/Stats.text = getStats()
	$CharacterPortrait/NameCharacter.text = SaveFile.player_name
	
func getStats():
	var stats = "Wins: " + str(SaveFile.duel_stats["wins"])
	stats += "\n"
	stats += "Losses: " + str(SaveFile.duel_stats["losses"])
	stats += "\n"
	stats += "Draws: " + str(SaveFile.duel_stats["draw"])
	stats += "\n"
	return stats
	
func _exit_tree():
	Global.tts_stop()
	get_parent().canClickStats = true

func _input(event):
	if Input.is_action_just_pressed("ui_select") || Input.is_action_just_pressed("ui_cancel") || Input.is_action_just_pressed("ui_accept"):
		$AnimationPlayer.play("Unload")
		destroyed = true
		accept_event()
		if get_parent().has_method("fadeIn"):
			get_parent().fadeIn()

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Unload":
		queue_free()
	elif anim_name == "Load":
		loaded = true


func _on_CloseButton_pressed():
	if !destroyed:
		destroyed = true
		accept_event()
		if get_parent().has_method("fadeIn"):
			get_parent().fadeIn()
