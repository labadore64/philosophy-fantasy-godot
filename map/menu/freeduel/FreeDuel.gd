extends Control

var myduels = []

onready var menuobj = preload("res://Scene/UI/menu/Help/Help.tscn")
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var selected_obj = null
var menu = null
var help_menu = null

var duel_do = ""

onready var mainMenu = preload("res://map/menu/MainMenu.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("Load")
	calculateDuels()

func selected(obj):
	selected_obj = obj
	$Sprite.texture = load("res://texture/portrait/"+obj.value+".png")

func calculateDuels():
	var duels = SaveFile.duels_fought
	for d in duels:
		var duelist = d.substr(0,d.length()-2)
		if !myduels.has(duelist):
			myduels.append(duelist)
			var instance = preload("res://map/menu/freeduel/FreeDuelPart.tscn").instance()
			instance.value = duelist
			instance.get_node("Button").text = duelist.capitalize()
			$Panel/ScrollContainer/VBoxContainer/GridContainer.add_child(instance)
	$Panel/ScrollContainer/VBoxContainer/GridContainer.get_child(0).get_child(0).emit_signal("pressed")

func _on_NextPhase_pressed():
	if duel_do == "":
		# calculate the latest duelist with this power
		var vals = []
		var duels = SaveFile.duels_fought
		for d in duels:
			if d.match(selected_obj.value + "*"):
				var val = d.split(selected_obj.value,false)
				vals.append(val[0])
				
		var largest = "00"
		for c in vals:
			if int(c) > int(largest):
				largest = c
				
		duel_do = selected_obj.value + largest
		$AnimationPlayer.play("Unload")


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Unload":
		queue_free()
		if duel_do != "":
			Global.loaded_duel = duel_do
			Global.doCutscene("LoadFreeduel")
		else:
			var instancez = mainMenu.instance()
			var lel = get_tree().get_root().get_node("Main")
			lel.call_deferred("add_child",instancez)

func _on_CloseButton_pressed():
	$AnimationPlayer.play("Unload")
	duel_do = ""


func _on_HelpButton_pressed():
	if !is_instance_valid(menu):
		if !is_instance_valid(help_menu):
			var loader = load("res://help/DeckBuild.tres")
			if loader != null:
				var help = menuobj.instance()
				help.translatey = 0
				get_parent().add_child(help)
				help_menu = help
				help.populate(loader.help_name,loader.help_desc)
				get_tree().set_input_as_handled()
		else:
			help_menu.closePanel()
