extends Control

onready var carde = load("res://Scene/card/Card.tscn")
onready var mainMenu = preload("res://map/menu/MainMenu.tscn")

var array = []
var trunkarray = []
var arrayObject
var destroyed = false
var location = null
var selected = null
var queued_action = ""
var selected_index = 0
var menu = null
var help_menu = null

const DECK_TOTAL = 30

onready var menuobj = preload("res://Scene/UI/menu/Help/Help.tscn")

var is_deck = false

var _v_scroll	# Scrollbar container reference
var _v_scroller	# Vscroll reference
var _v_scroll1	# Scrollbar container reference
var _v_scroller1# Vscroll reference

# Called when the node enters the scene tree for the first time.
func _ready():
	_v_scroll = get_node("TabContainer/Deck/ScrollContainer")
	_v_scroller = _v_scroll.get_node("_v_scroll")
	_v_scroll1 = get_node("TabContainer/Trunk/ScrollContainer")
	_v_scroller1 = _v_scroll1.get_node("_v_scroll")
	#SaveFile.decks.append(["guattari","guattari","deleuze"])
	#SaveFile.decks.append(["guattari","guattari","zeno","deleuze","aristotle"])
	Global.playSong("deck_build")
	Global.playSoundFx("card/duel_list")
	$AnimationPlayer.play("Load")
	$Node2D.visible = true
	setDeck(true)
	if !Global.help_enabled:
		$HelpButton.queue_free()

func updateDeck():
	var deck = []
	var trunk = []
	
	for c in array:
		for q in range(0,c.quantity):
			deck.append(c.card_name)
			
	for c in trunkarray:
		for q in range(0,c.quantity):
			trunk.append(c.card_name)
			
	SaveFile.decks[0] = deck
	if SaveFile.decks.size() > 1:
		SaveFile.decks[1] = trunk
			

func setDeck(value):
	setCards()

func setCards():
	var arr = SaveFile.decks[0]
	array.clear()
	arrayObject = preload("res://map/menu/cardlist/CardMenuPart.tscn")
	# populate deck
	for c in arr:
		var cardexist = cardExists(c)
		
		if cardexist != null:
			cardexist.increaseQuantity()
		else:
			var obj = arrayObject.instance()
			array.append(obj)
			$TabContainer/Deck/ScrollContainer/VBoxContainer/GridContainer.add_child(obj)
			obj.card_ref = carde.instance()
			obj.card_name = c
			obj.card_ref.populate(c)
			obj.increaseQuantity()
			obj.isDeck = true
			obj.call_deferred("setCard")
			
	if SaveFile.decks.size() > 1:
		arr = SaveFile.decks[1]
		
		if arr.size() > 0:
			# populate trunk
			for c in arr:
				var cardexist = cardTrunkExists(c)
				
				if cardexist != null:
					cardexist.increaseQuantity()
				else:
					var obj = arrayObject.instance()
					trunkarray.append(obj)
					$TabContainer/Trunk/ScrollContainer/VBoxContainer/GridContainer.add_child(obj)
					obj.card_ref = carde.instance()
					obj.card_name = c
					obj.card_ref.populate(c)
					obj.increaseQuantity()
					obj.call_deferred("setCard")
			array[0].call_deferred("selected")
		else:
			$TabContainer/Trunk.queue_free()
	else:
		$TabContainer/Trunk.queue_free()
	setTotalCards()
	
func cardExists(card):
	for c in array:
		if c.card_name == card:
			return c;
	return null
	
func cardTrunkExists(card):
	for c in trunkarray:
		if c.card_name == card:
			return c;
	return null
	
func selected(card):
	if !is_instance_valid(help_menu):
		for c in array:
			if c != card:
				if is_instance_valid(c):
					c.unselected()
		for c in trunkarray:
			if c != card:
				if is_instance_valid(c):
					c.unselected()
		selected_index = array.find(card)
		if selected_index == -1:
			selected_index = trunkarray.find(card)
		selected = card
		$Node2D.copy(card.card_ref)
		selected.get_node("Button").grab_focus()
	
func showUse(card):
	$Use.visible = false
	var cardo = card.card_ref.parent
	if location.name == "Limbo":
		if cardo.doAsLimboTest():
			$Use.visible = true
	elif location.name == "Deck":
		if cardo.doAsDeckTest():
			$Use.visible = true
	elif location.name == "Discard":
		if cardo.doAsDiscardTest():
			$Use.visible = true

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Unload":
		var instancez = mainMenu.instance()
		var lel = get_tree().get_root().get_node("Main")
		lel.call_deferred("add_child",instancez)
		queue_free()

func _input(event):
	if !is_instance_valid(help_menu):
		if event is InputEventKey:
			if selected != null:
				for c in range(0,10):
					if Input.is_action_just_pressed("access_key"+str(c)):
						selected.infokey(c)
							
			if menu == null:
				if Input.is_action_just_pressed("ui_up"):
					if $TabContainer.current_tab == 0:
						array[(selected_index-1+array.size()) % array.size()].selected()
					else:
						trunkarray[(selected_index-1+trunkarray.size()) % trunkarray.size()].selected()
				elif Input.is_action_just_pressed("ui_down"):
					if $TabContainer.current_tab == 0:
						array[(selected_index+1+array.size()) % array.size()].selected()
					else:
						trunkarray[(selected_index+1+trunkarray.size()) % trunkarray.size()].selected()
				if Input.is_action_just_pressed("ui_left") || Input.is_action_just_pressed("ui_right"):
					if SaveFile.decks.size() > 1:
						$TabContainer.current_tab = ($TabContainer.current_tab + 1) % 2
						_on_TabContainer_tab_selected($TabContainer.current_tab)
				if Input.is_action_just_pressed("ui_select") || Input.is_action_just_pressed("ui_accept"):
					selected._on_Button_pressed()
					get_tree().set_input_as_handled()
					
	# align the last value if it lines up
	if Input.is_action_just_pressed("ui_up") || Input.is_action_just_pressed("ui_down"):
		var grid = _v_scroll.get_node("VBoxContainer/GridContainer")
		if grid.get_child(grid.get_child_count()-1) == selected:
			_v_scroller.value = _v_scroller.max_value

		if is_instance_valid(_v_scroll1):
			grid = _v_scroll1.get_node("VBoxContainer/GridContainer")
			if grid.get_child(grid.get_child_count()-1) == selected:
				_v_scroller1.value = _v_scroller1.max_value
				
	if event is InputEventKey:
		if Input.is_action_just_pressed("ui_left"):
			Global.playSoundFx("ui/menu_left")
		if Input.is_action_just_pressed("ui_right"):
			Global.playSoundFx("ui/menu_right")
		if Input.is_action_just_pressed("ui_up"):
			Global.playSoundFx("ui/menu_up")
		if Input.is_action_just_pressed("ui_down"):
			Global.playSoundFx("ui/menu_down")
		if Input.is_action_just_pressed("ui_accept") || Input.is_action_just_pressed("ui_select"):
			Global.playSoundFx("ui/menu_open")
			
func moveCardToDeck():
	if selected != null:
		if trunkarray.size() > 1 || selected.quantity > 1:
			var exists = cardExists(selected.card_name)
			var result = true
			var adjust = false
			
			if exists != null:
				if exists.quantity < 3:
					exists.increaseQuantity()
					result = selected.decreaseQuantity()
					Global.playSoundFx("card/card_add_deck")
			else:
					var obj = arrayObject.instance()
					array.append(obj)
					$TabContainer/Deck/ScrollContainer/VBoxContainer/GridContainer.add_child(obj)
					obj.card_ref = carde.instance()
					obj.isDeck = true
					obj.card_name = selected.card_name
					obj.card_ref.populate(selected.card_name)
					obj.increaseQuantity()
					obj.call_deferred("setCard")
					result = selected.decreaseQuantity()
					Global.playSoundFx("card/card_add_deck")
			
			for c in trunkarray:
				if !is_instance_valid(c) || c.is_queued_for_deletion():
					trunkarray.erase(c)

			if !result:
				if trunkarray.size() > 0:
					trunkarray[(selected_index-1) % trunkarray.size()].selected()
			
			if !is_instance_valid(selected):
				selected = null

	setTotalCards()

func moveCardToTrunk():
	if selected != null:
		if array.size() > 1 || selected.quantity > 1:
			var exists = cardTrunkExists(selected.card_name)
			
			var adjust = false
			
			if exists != null:
				exists.increaseQuantity()
			else:
				var obj = arrayObject.instance()
				trunkarray.append(obj)
				$TabContainer/Trunk/ScrollContainer/VBoxContainer/GridContainer.add_child(obj)
				obj.card_ref = carde.instance()
				obj.card_name = selected.card_name
				obj.card_ref.populate(selected.card_name)
				obj.increaseQuantity()
				obj.call_deferred("setCard")
			var result = selected.decreaseQuantity()
			
			for c in array:
				if !is_instance_valid(c) || c.is_queued_for_deletion():
					array.erase(c)

			if !result:
				if array.size() > 0:
					array[(selected_index-1) % array.size()].selected()
			
			if !is_instance_valid(selected):
				selected = null
			Global.playSoundFx("card/card_add_trunk")
	setTotalCards()

func _on_TabContainer_tab_selected(tab):
	if tab == 2:
		updateDeck()
		_on_CloseButton_pressed()
	else:
		if tab == 0:
			$TabContainer/Deck/ScrollContainer/VBoxContainer/GridContainer.get_child(0).selected()
		else:
			if $TabContainer/Trunk == null:
				updateDeck()
				_on_CloseButton_pressed()
			else:
				$TabContainer/Trunk/ScrollContainer/VBoxContainer/GridContainer.get_child(0).selected()

func canOpenMenu():
	if $TabContainer.current_tab == 0:
		# deck
		if array.size() <= 1:
			var exists = cardExists(selected.card_name)
			if exists != null:
				if exists.quantity == 1:
					return false
	else:
		var exists = cardExists(selected.card_name)
		if exists != null:
			if exists.quantity >= 3:
				return false
	return true

func setTotalCards():
	var stringer = "Total Cards: "
	var arrayCount = 0
	
	for c in array:
		arrayCount+= c.quantity
	
	$TabContainer/Deck/Quantity.text = stringer + str(arrayCount)
	if SaveFile.decks.size() > 1:
		arrayCount = 0
		
		for c in trunkarray:
			arrayCount+= c.quantity
		
		$TabContainer/Trunk/Quantity.text = stringer + str(arrayCount)


func _on_CloseButton_pressed():
	if !is_instance_valid(menu):
		if !destroyed:
			$AnimationPlayer.play("Unload")
			destroyed = true
			Global.playSoundFx("ui/menu_cancel")
			Global.playSong("main_menu")


func _on_HelpButton_pressed():
	if !is_instance_valid(menu):
		if !is_instance_valid(help_menu):
			var loader = load("res://help/DeckBuild.tres")
			if loader != null:
				var help = menuobj.instance()
				help.translatey = 0
				get_parent().add_child(help)
				help_menu = help
				help.populate(loader.help_name,loader.help_desc)
				get_tree().set_input_as_handled()
		else:
			help_menu.closePanel()

