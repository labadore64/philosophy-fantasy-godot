extends Control

var toTrunk = false
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	if Global.get_node("AudioStream").playing:
		Global.get_node("AudioStream").playing = false
		Global.get_node("AudioStream").stop()
	Global.playSoundFx("card/card_open_list")
	$Panel/GridContainer/ToPlace.grab_focus()
	if get_parent().selected.isDeck:
		$Panel/GridContainer/ToPlace.text = "To Trunk"
		toTrunk = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Cancel_pressed():
	if !Global.get_node("AudioStream").playing:
		Global.playSoundFx("card/card_close_list")
	get_tree().set_input_as_handled()
	queue_free()
	get_parent().menu = null
	get_parent().selected.selected()

func _on_ToPlace_pressed():
	if toTrunk:
		var killOnComplete = false
		if get_parent().selected.quantity == 1:
			killOnComplete = true
		get_parent().moveCardToTrunk()
		if killOnComplete:
			_on_Cancel_pressed()
	else:
		var killOnComplete = false
		if get_parent().selected.quantity == 1:
			killOnComplete = true
		get_parent().moveCardToDeck()
		if killOnComplete:
			_on_Cancel_pressed()

func _input(event):
	if event is InputEventKey:
		if Input.is_action_just_pressed("ui_up"):
			Global.playSoundFx("ui/menu_up")
		if Input.is_action_just_pressed("ui_down"):
			Global.playSoundFx("ui/menu_down")
	if Input.is_action_just_pressed("ui_accept") || Input.is_action_just_pressed("ui_select"):
		Global.playSoundFx("ui/menu_open")
