extends Control

onready var optionsMenu = preload("res://Scene/UI/menu/newMenu/newMenu.tscn")
onready var credits = preload("res://map/Credits.tscn")
var instance = null

var canClickCredits = true
var canClickStats = true
var canFreeDuel = true

var textures = []
var quotes = []

var titleScreenGoto = false
var deckBuildGoto = false

var node = null

var has_list = []

# Called when the node enters the scene tree for the first time.
func _ready():
	SaveFile.saveFile()
	Global.playSong("main_menu")
	$AnimationPlayer.play("FadeIn")
	reloadTextures()
	$GridContainer.get_child(0).grab_focus()
	Global.tts_say($GridContainer.get_child(0).text)
	$GridContainer/ContinueStory.queue_free()

func reloadTextures():
	textures = []
	has_list = []
	if SaveFile.decks.size() > 0:
		for c in SaveFile.decks[0]:
			if !has_list.has(c):
				has_list.append(c)
				var data = load("res://data/card/"+c+".tres")
				textures.append(data.art)
				quotes.append(data.quote)
		
	if textures.empty():
		var data = load("res://data/card/archimedes.tres")
		textures.append(data.art)
		quotes.append(data.quote)

func updateTexture():
	var val = randi() % textures.size()
	$SprCardArchimedes.texture = textures[val] 
	$SprCardArchimedes/Quote.text = quotes[val] 

func _on_Options_pressed():
	if !is_instance_valid(instance):
		instance = create_submenu(optionsMenu)

func _on_Credits_pressed():
	if canClickCredits:
		canClickCredits = false
		canClickCredits = false
		$AnimationPlayer.play("FadeOutCredits")
		var instance = credits.instance()
		add_child(instance)
		
func create_submenu(menu):
	var instance = menu.instance()
	instance.base_parent = self
	add_child(instance)
	return instance
	
func fadeIn():
	canClickCredits = true
	$AnimationPlayer.play("FadeIn")
	
func fadeOut():
	canClickCredits = false
	$AnimationPlayer.play("FadeOut")


func _on_Title_Screen_pressed():
	if !titleScreenGoto:
		titleScreenGoto = true
		fadeOut()


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "FadeOut":
		if titleScreenGoto:
			var instancez = load("res://map/MainMenu.tscn").instance()
			var lel = get_tree().get_root().get_node("Main")
			lel.call_deferred("add_child",instancez)
			queue_free()
		if deckBuildGoto:
			var instancez = load("res://map/menu/cardlist/card_menu.tscn").instance()
			var lel = get_tree().get_root().get_node("Main")
			lel.call_deferred("add_child",instancez)
			queue_free()
		if !canClickStats:
			var instancez = load("res://map/start/CharacterBuilder.tscn").instance()
			instancez.main_menu = true
			var lel = get_tree().get_root().get_node("Main")
			lel.call_deferred("add_child",instancez)
			queue_free()
		if !canFreeDuel:
			var instancez = load("res://map/menu/freeduel/FreeDuel.tscn").instance()
			var lel = get_tree().get_root().get_node("Main")
			lel.call_deferred("add_child",instancez)
			queue_free()

func _on_DeckViewer_pressed():
	if !deckBuildGoto:
		deckBuildGoto = true
		fadeOut()
		
func _input(event):
	if get_focus_owner() != node:
		node = get_focus_owner()
		if node != null:
			if node is Button:
				Global.tts_say(node.text)
	
	if event is InputEventKey:
		if Input.is_action_just_pressed("ui_up"):
			Global.playSoundFx("ui/menu_up")
		if Input.is_action_just_pressed("ui_down"):
			Global.playSoundFx("ui/menu_down")
	if Input.is_action_just_pressed("ui_accept") || Input.is_action_just_pressed("ui_select"):
		Global.playSoundFx("ui/menu_open")
		if Input.is_action_just_pressed("ui_select"):
			get_focus_owner().emit_signal("pressed")
func regrab():
	$GridContainer/Options.grab_focus()


func _on_Stats_pressed():
	if canClickStats:
		canClickStats = false
		fadeOut()

func _on_FreeDuel_pressed():
	if canFreeDuel:
		canFreeDuel = false
		fadeOut()
