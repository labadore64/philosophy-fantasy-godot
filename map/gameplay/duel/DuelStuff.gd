# The top menu

extends Control

var opened = false
var optionsmenu = null
var help_menu = null
var forfeit_menu = null
var translatey = 400
var resource_name = "res://help/Battle.tres"
# Help menu preloaded object
onready var forfeitObj = preload("res://map/gameplay/duel/ForfeitDialog.tscn")
onready var menuobj = preload("res://Scene/UI/menu/Help/Help.tscn")
onready var optionsMenu = preload("res://Scene/UI/menu/newMenu/newMenu.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_BurgerBun_pressed():
	if !is_instance_valid(optionsmenu):
		var duel = get_parent().get_parent()
		if !duel.actions_working() && !is_instance_valid(duel.menu):
			if opened:
				unload()
			else:
				duel.duelStuff_open = false
				Global.playSoundFx("card/duel_menuopen")
				$AnimationPlayer.play("Load")
				$Panel/GridContainer/NextPhase.grab_focus()
				opened = !opened

func unload():
	var duel = get_parent().get_parent()
	duel.duelStuff_open = true
	Global.playSoundFx("card/duel_menuclose")
	$AnimationPlayer.play("Unload")
	if duel.lookat_you:
		duel.players[0].initFocus()
	else:
		duel.players[1].initFocus()
	opened = false

func _on_EndTurn_pressed():
	if opened:
		if !is_instance_valid(help_menu):
			if !is_instance_valid(forfeit_menu):
				if !is_instance_valid(optionsmenu):
					var duel = get_parent().get_parent()
					duel.endPhase()
					$AnimationPlayer.play("Unload")
					Global.playSoundFx("card/duel_menuaccept")
					opened = false
					duel.duelStuff_open = true
					$Panel/SideView.rect_rotation = 0
					duel.lookat_you = true
	
func _on_NextPhase_pressed():
	if opened:
		if !is_instance_valid(help_menu):
			if !is_instance_valid(forfeit_menu):
				if !is_instance_valid(optionsmenu):
					var duel = get_parent().get_parent()
					duel.nextPhase()
					$AnimationPlayer.play("Unload")
					Global.playSoundFx("card/duel_menuaccept")
					opened = false
					duel.duelStuff_open = true
					$Panel/SideView.rect_rotation = 0
					duel.lookat_you = true

func _on_SideView_pressed():
	if !is_instance_valid(help_menu):
		if !is_instance_valid(forfeit_menu):
			if !is_instance_valid(optionsmenu):
				var duel = get_parent().get_parent()
				if !duel.actions_working() && !is_instance_valid(duel.menu):

					duel.lookat_you = !duel.lookat_you
					if duel.lookat_you:
						Global.playSoundFx("card/duel_movedown")
						$Panel/SideView.rect_rotation = 0
						duel.addAction("camera",[duel.boardBottom,0.15])
						duel.batchMovement()
						duel.players[0].initFocus()
					else:
						Global.playSoundFx("card/duel_moveup")
						$Panel/SideView.rect_rotation = 180
						duel.addAction("camera",[duel.boardTop,0.15])
						duel.batchMovement()
						duel.players[1].initFocus()


func _on_Options_pressed():
	if opened:
		if !is_instance_valid(help_menu):
			if !is_instance_valid(forfeit_menu):
				if !is_instance_valid(optionsmenu):
					Global.playSoundFx("card/duel_options")
					optionsmenu = create_submenu(optionsMenu)
					var duel = get_parent().get_parent().get_node("Camera2D")
					remove_child(optionsmenu)
					duel.add_child(optionsmenu)
					optionsmenu.translatey = 400
					optionsmenu.onReady()


func _on_SideView2_pressed():
	var duel = get_parent().get_parent()
	if duel.current_phase == 3 || duel.current_phase == 5:
		if !is_instance_valid(optionsmenu):
			if !is_instance_valid(forfeit_menu):
				if !is_instance_valid(help_menu):
					var loader = load(resource_name)
					if loader != null:
						var help = menuobj.instance()
						help.translatey = translatey
						duel = get_parent().get_parent().get_node("Camera2D")
						duel.add_child(help)
						help_menu = help
						help.populate(loader.help_name,loader.help_desc)
						get_tree().set_input_as_handled()
				else:
					help_menu.closePanel()


func _on_Forfeit_pressed():
	if opened:
		if !is_instance_valid(help_menu):
			if !is_instance_valid(forfeit_menu):
				if !is_instance_valid(optionsmenu):
					var duel = get_parent().get_parent().get_node("Camera2D")
					forfeit_menu = forfeitObj.instance()
					forfeit_menu.parent = self
					Global.playSoundFx("card/duel_forfeit_confirm")
					duel.add_child(forfeit_menu)

func create_submenu(menu):
	var instance = menu.instance()
	instance.base_parent = self
	add_child(instance)
	return instance
	
func regrab():
	$Panel/GridContainer/Options.grab_focus()
	
func regrab2():
	$Panel/GridContainer/Forfeit.grab_focus()	
	


func _on_NextPhase_focus_entered():
	var text = $Panel/GridContainer/NextPhase.text
	Global.tts_say(text)


func _on_EndTurn_focus_entered():
	var text = $Panel/GridContainer/EndTurn.text
	Global.tts_say(text)


func _on_Forfeit_focus_entered():
	var text = $Panel/GridContainer/Forfeit.text
	Global.tts_say(text)


func _on_Options_focus_entered():
	var text = $Panel/GridContainer/Options.text
	Global.tts_say(text)
