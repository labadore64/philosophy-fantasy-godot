extends Polygon2D

onready var cardTemplate = preload("res://map/gameplay/duel/DuelCard.tscn")
onready var cardInfo = preload("res://Scene/card/Card.tscn")
var cards = []
var card_data
onready var duel = get_parent().get_parent().get_parent()

signal received_card(card)
signal sent_card(card)

# Returns the movement position for moving cards
func getOpenPosition():
	return position

func tweenCard(card,destination):
	get_parent().get_parent().get_parent().get_node("Tween").interpolate_property(
		card.get_node("Sprite"),
		"position",
		card.get_node("Sprite").position,
		destination,
		0.1,
		Tween.TRANS_LINEAR,
		Tween.EASE_OUT
	)
	get_parent().get_parent().get_parent().get_node("Tween").start()

func populate(cards):
	card_data = cards;
	produce()
	
func produce():
	var instance
	for c in card_data:
		# create the card info
		var card_info = cardInfo.instance()
		card_info.populate(c)
		# create the card
		var cardz = cardTemplate.instance()
		get_parent().add_child(cardz)
		cardz.setCard(card_info)
		cardz.cardPopulate(card_info)
		cardz.get_node("Sprite").position = getOpenPosition()
		cardz.location = self
		cardz.duel = duel
		cardz.side = get_parent()
		if cardz.side == cardz.duel.players[0]:
			cardz.other_side = cardz.duel.players[1]
		else:
			cardz.other_side = cardz.duel.players[0]
		cards.append(cardz)
	card_data = null

func setFocus():
	for c in cards:
		c.updateFocus()


func moveCard(card_id,destination):
	if cards.size() > 0:
		get_parent().get_parent().get_parent().addAction("card",[card_id,self,destination])
		
func switchCard(source_id,dest_id,destination):
	if cards.size() > 0:
		get_parent().get_parent().get_parent().addAction("switch",[source_id,dest_id,self,destination])
		
		
func doColorblind():
	for c in cards:
		c.updateColorblindVisibility()
		
func resetBoosts():
	for c in cards:
		c.resetBoosts()

