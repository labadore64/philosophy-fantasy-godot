extends "res://map/gameplay/duel/CardPlace.gd"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Only draw the top card
func _on_Limbo_received_card(card):
	card.flipped = false
	for c in range(0,cards.size()):
		cards[c].get_node("Sprite").visible = true
	#if cards.size() > 0:
	#	cards[cards.size()-1].get_node("Sprite").visible = true
	
	# card effect
	duel.injectAction("cardeffect",[card,"doToLimbo"])

func _on_Limbo_sent_card(card):
	duel.injectAction("cardeffect",[card,"doFromLimbo"])
