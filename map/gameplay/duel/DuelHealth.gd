extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Panel/Health/Label.text = str(floor($Panel/Health.value)) + " HP"

func setEnemy(is_cpu):
	if is_cpu:
		get_node("Node2D").visible = false
	else:
		get_node("Polygon2D").visible = false
		get_node("Thomas").visible = false
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Health_value_changed(value):
	$Panel/Health/Label.text = str(floor($Panel/Health.value)) + " HP"
