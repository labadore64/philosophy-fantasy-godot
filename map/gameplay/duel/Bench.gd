extends "res://map/gameplay/duel/CardPlace.gd"

# Returns the movement position for moving cards
func getOpenPosition(size=cards.size()):
	return position + Vector2((size-2)*300,0)



func _on_Bench_received_card(card):
	card.get_node("Sprite").visible = true
	card.flipped = false
	
	var counter = 0
	for c in cards:
		c.get_node("Sprite").position = getOpenPosition(counter+1)
		counter+=1


func _on_Bench_sent_card(card):
	_on_Bench_received_card(card)
