extends "res://map/gameplay/duel/CardPlace.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Theory_received_card(card):
	card.get_node("Sprite").visible = true
	card.flipped = false


func _on_Theory_sent_card(card):
	pass # Replace with function body.
