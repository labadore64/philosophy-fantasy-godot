extends Node2D

#var card_move_queue = []
#var card_move_destination = []
#var card_move_location = []

var next_scene = null
var next_cutscene = ""
var wait_for_speech = false

var lastAnimator = null

onready var msgbox = preload("res://Scene/UI/dialog/MessageBox.tscn")

var restrictions = {}
var action_history = []
var actions = []
var actions_data = []
var actions_stack = []
var actions_data_stack = []
var act_working = false
var turns = 0

var selected_card = -1
var selected_zone = null
var selected_zones = []
var selected_conditions = {}

var lookat_you = true

var music
var ambience

var duelStuff_open = true
var duelMenu_open = false

var last_spoken_location = null

var box = null
var menu = null
var current_player = 0
var computerOpponent = true
onready var players = [$Node2D/PlayerField,$Node2D/OpponentField]

var boardBottom = Vector2(980,920)
var boardTop = Vector2(980,-60)
var boardMiddle = Vector2(980,500)

var current_phase = 0

var victory_state = 0

enum PHASES {
	START,
	LIMBO,
	MAIN,
	BATTLE,
	RETURN,
	END
}

enum VICTORY {
	NONE,
	VICTORY,
	DEFEAT,
	DRAW
}

const PHASE_STRINGS = [
	"Draw",
	"Limbo",
	"Main",
	"Dialog",
	"Return",
	"End"
]

var duel_name = ""

signal card_move_complete()
signal perform_action(action,user)
signal speech_completed()
signal card_selected()

func _process(delta):
	if wait_for_speech:
		if TTS.is_speaking:
			if Input.is_action_just_pressed("ui_accept") || Input.is_action_just_pressed("ui_select") || Input.is_action_just_pressed("ui_cancel"):
				TTS.stop()
		else:
			wait_for_speech = false
			emit_signal("speech_completed")
		get_tree().set_input_as_handled()

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.playSong("")
	if current_player == 0:
		$Camera2D.position = boardBottom
	else:
		$Camera2D.position = boardTop
	$Node2D/PlayerField.player = 0
	$Node2D/OpponentField.player = 1
	$Node2D/OpponentField.enemy = true
	$Node2D/PlayerField.hp = $Camera2D/YourHealth/Panel/Health
	$Node2D/OpponentField.hp = $Camera2D/EnemyHealth/Panel/Health
	var root = get_tree().get_root().get_node("Main")
	#duel_name = "thomas01"
	SaveFile.addDuel(duel_name)
	call_deferred("loadDuelData",duel_name)
	call_deferred("startGame")

func loadDuelData(filename):
	var data = load("res://data/duel/"+filename+".tres")
	var player_name = SaveFile.player_name
	var enemy_name = data.opponent_name
	var enemy_sprite = data.opponent_sprite
	next_cutscene = data.next_cutscene
	$Node2D/OpponentField/Deck.startDeck(data.cards)
	$Node2D/PlayerField/Deck.startDeck(SaveFile.decks[0])
	$Node2D/OpponentField.difficulty = data.max_difficulty # edit this so it accounts for player defined difficulty
	next_scene = data.next_scene
	
	music = data.music.resource_path.get_file().get_basename()
	ambience = data.ambience.resource_path.get_file().get_basename()
	$Camera2D/YourHealth/Panel/Panel/Label.text = player_name
	$Camera2D/EnemyHealth/Panel/Panel/Label.text = enemy_name
	
	# this is not a CPU player then
	if enemy_name == null:
		$Camera2D/EnemyHealth.setEnemy(false)
		pass	# not CPU stuff goes here
	else:
		$Camera2D/EnemyHealth.setEnemy(true)

func endPhase():
	addAction("wait",0.5)
	addAction("endphase",0)
	batchMovement()
		
func nextPhase():
	addAction("wait",0.5)
	addAction("phase",0)
	batchMovement()	
	
func clearAnimations():
	actions = []
	actions_data = []
	actions_stack = []
	actions_data_stack = []
	selected_card = -1
	selected_zone = null
	selected_zones = []
	selected_conditions = {}
	
func doVictory():
	victory_state = VICTORY.VICTORY
	clearAnimations()
	addAction("music","")
	addAction("amb","")
	SaveFile.duel_stats["wins"] += 1
	addAction("camera",[boardMiddle,0.15])
	addAction("sfx","card/duel_victory")
	$VictoryText.text = "Victory!"
	addAnimation(self,"victory")
	addAction("wait",0.25)
	addAnimation(self,"end")
	addAction("end",0)
	
func doDefeat():
	victory_state = VICTORY.DEFEAT
	clearAnimations()
	addAction("music","")
	addAction("amb","")
	SaveFile.duel_stats["losses"] += 1
	addAction("camera",[boardMiddle,0.15])
	$VictoryText.text = "Defeat..."
	addAction("sfx","card/duel_defeat")
	addAnimation(self,"victory")
	addAction("wait",0.25)
	addAnimation(self,"end")
	addAction("end",0)
	
func doDraw():
	SaveFile.duel_stats["draw"] += 1
	victory_state = VICTORY.DRAW
	clearAnimations()
	addAction("music","")
	addAction("amb","")
	addAction("camera",[boardMiddle,0.15])
	$VictoryText.text = "Draw?!"
	addAnimation(self,"victory")
	addAction("wait",0.25)
	addAnimation(self,"end")
	addAction("end",0)
	
func calculateVictory():
	if victory_state == 0:
		var you_health = $Camera2D/YourHealth/Panel/Health.value
		var enemy_health = $Camera2D/EnemyHealth/Panel/Health.value
		
		if you_health == 0 && enemy_health == 0:
			doDraw()
		elif you_health == 0:
			doDefeat()
		elif enemy_health == 0:
			doVictory()
		
func startGame():
	var other_player = (current_player+1) % 2
	players[current_player].deckDraw()
	players[current_player].deckDraw()
	players[current_player].deckDraw()
	players[current_player].deckDraw()
	players[current_player].deckDraw()
	addAction("wait",0.25)
	addAction("camera",[boardTop,0.15])
	players[other_player].deckDraw()
	players[other_player].deckDraw()
	players[other_player].deckDraw()
	players[other_player].deckDraw()
	players[other_player].deckDraw()
	addAction("wait",0.25)
	addAction("camera",[boardMiddle,0.15])
	addAction("text","Start!")
	addAction("music",music)
	addAction("amb",ambience)
	tutorialText("BattleTutorial_01")
	addAction("wait",0.25)
	addAction("phase",0)
	batchMovement()

func tutorialText(event):
	if !SaveFile.hasEvent(event):
		SaveFile.addEvent(event)
		addAction("textbox",event)
		
func batchMovement():
	if !act_working:
		doAction()

func addAction(action,arg):
	actions.append(action)
	actions_data.append(arg)

func injectAction(action,arg):
	if actions.size() > 1:
		actions.insert(1,action)
		actions_data.insert(1,arg)
	else:
		actions.append(action)
		actions_data.append(arg)

func safetyString(string,string_len):
	return string.dedent().substr(0,string_len)

func doAction():
	act_working = true
	actions[0] = safetyString(actions[0],11)
	while actions.size() > 0:
		if actions[0] == "card":
			doCard()
		elif actions[0] == "wait":
			doWait()
		elif actions[0] == "push":
			doPush()
		elif actions[0] == "effect":
			doEffect()
		elif actions[0] == "cardeffect":
			doCardEffect()
		elif actions[0] == "camera":
			doCamera()
		elif actions[0] == "text":
			doStartText()
		elif actions[0] == "phase":
			doPhase()
		elif actions[0] == "endphase":
			doEndPhase()
		elif actions[0] == "ai":
			doAIAction()
		elif actions[0] == "switch":
			doSwitch()
		elif actions[0] == "sfx":
			doSfx()
		elif actions[0] == "music":
			doMusic()
		elif actions[0] == "amb":
			doAmb()
		elif actions[0] == "msgbox":
			doMsgbox()
		elif actions[0] == "textbox":
			doTextbox()
		elif actions[0] == "hp":
			doHP()
		elif actions[0] == "animate":
			doAnimation()
		elif actions[0] == "focus":
			doFocus()
		elif actions[0] == "tts":
			doTTS()
		elif actions[0] == "select":
			doSelect()
		elif actions[0] == "end":
			doEndDuel()
		yield(self,"card_move_complete")
		if actions.size() > 0:
			actions.remove(0)
			actions_data.remove(0)
			
		if actions.size() == 0:
			while(actions.size() == 0 && actions_stack.size() > 0):
				if actions_stack.size() > 0:
					popAction()
			
		calculateVictory()
	act_working = false
	selected_card = -1
	selected_zone = null
	selected_zones = []
	selected_conditions = {}
	
func actions_working():
	return act_working
	
func addAnimation(obj,player):
	addAction("animate",[player,obj])
	
func pushAction():
	actions_stack.append(actions)
	actions_data_stack.append(actions_data)
	actions = []
	actions_data = []
	
func doPush():
	pushAction()
	yield(get_tree().create_timer(0.01), "timeout")
	emit_signal("card_move_complete")
	
func doEffect():
	var data = actions_data[0];
	# data[0] = object
	# data[1] = method
	# data[2] = arg
	if data[0].has_method(data[1]):
		if data.size() == 2:
			data[0].call(data[1])
		else:
			data[0].call(data[1],data[2])
		
	yield(get_tree().create_timer(0.01), "timeout")
	emit_signal("card_move_complete")
	
func doCardEffect():
	var data = actions_data[0];
	# data[0] = object
	# data[1] = method
	if data[0].has_method(data[1]):
		if data[0].call(data[1]+"Test"):
			actions.remove(0)
			actions_data.remove(0)
			pushAction()
			if data[0].side.hasStatus("block"):
				data[0].side.decrementStatusEffect("block")
				addAction("wait",0.01)
				addAction("sfx","card/card_deny")
				addAction("wait",0.5)
				addAction("msgbox","The effect of " + data[0].card_ref.card_name + " was blocked!")
			else:
				data[0].setCardActivated()
				data[0].doUse()
				data[0].call(data[1])
	
	yield(get_tree().create_timer(0.01), "timeout")
	emit_signal("card_move_complete")
	
	
# Will wait until a card is selected.
func doSelect():
	var data = actions_data[0]
	selected_zones = data[0]
	selected_conditions = data[1]
	# If the first selected zone is in these places,
	# you open the menu automatically
	if selected_zones[0].name == "Deck" || selected_zones[0].name == "Limbo" || selected_zones[0].name == "Discard":
		var menu = selected_zones[0].cards[0].openCardList()
		menu.get_node("NextPhase").visible = false
		menu.get_node("Select").visible = true
	yield(self,"card_selected")
	emit_signal("card_move_complete")
	
func doAIAction():
	var data = actions_data[0]
	actions.remove(0)
	actions_data.remove(0)
	pushAction()
	_on_Node2D_perform_action(data)
	yield(get_tree().create_timer(0.01), "timeout")
	emit_signal("card_move_complete")
	
func popAction():
	actions = actions_stack[actions_stack.size()-1]
	actions_data = actions_data_stack[actions_data_stack.size()-1]
	actions_stack.remove(actions_stack.size()-1)
	actions_data_stack.remove(actions_data_stack.size()-1)
	
	if actions.size() > 0:
		if actions[0] == "push":
			actions.remove(0)
			actions_data.remove(0)
	
func doMsgbox():
	box = msgbox.instance()
	box.setText(actions_data[0])
	box.position = Vector2(0,0)
	$Camera2D.add_child(box)
	yield(box,"tree_exited")
	emit_signal("card_move_complete")
	
func doTextbox():
	box = preload("res://Scene/UI/dialog/DialogBox.tscn").instance()
	$Camera2D.add_child(box)
	box.position.x = -250
	box.position.y = -200
	box.visible = false
	var obj
	# loads the blind text if available
	if Global.TTS_enabled:
		obj = load("res://data/textbox/"+ actions_data[0] + "_lv.tres")
		if obj == null:
			obj = load("res://data/textbox/"+ actions_data[0] + ".tres")
	else:
		obj = load("res://data/textbox/"+ actions_data[0] + ".tres")
	box.setNameAndText(obj.character_name,obj.sprite_name,obj.text)
	yield(box,"tree_exited")
	emit_signal("card_move_complete")
	
func doHP():
	var data = actions_data[0]
	var player = data[0]
	var amount = data[1]
	var hp = player.hp.value - amount
	if hp < 0:
		hp = 0
	if hp > 30:
		hp = 30
	
	player.setHP(hp)	
	
	yield($Tween,"tween_completed")
	emit_signal("card_move_complete")
	
func doAnimation():
	var data = actions_data[0]
	$Camera2D/AnimationPlayer.stop()
	if lastAnimator != null:
		lastAnimator.play("RESET")
		lastAnimator.stop()
		lastAnimator.clear_queue()
		lastAnimator.clear_caches()
	
	data[1].get_node("AnimationPlayer").stop()
	data[1].get_node("AnimationPlayer").clear_queue()
	data[1].get_node("AnimationPlayer").clear_caches()
	data[1].get_node("AnimationPlayer").play(data[0])
	lastAnimator = data[1].get_node("AnimationPlayer")
	
	# make sure if its a card its attached to the display
	if data[1].has_method("showCard"):
		if data[1].get_parent() == null:
			$Camera2D.add_child(data[1])
		
	yield(data[1].get_node("AnimationPlayer"),"animation_finished")
	emit_signal("card_move_complete")

func doFocus():
	if current_player == 0:
		duelMenu_open = false
		actions_data[0].grabFocus()
	yield(get_tree().create_timer(0.01), "timeout")
	emit_signal("card_move_complete")

func doTTS():
	if Global.TTS_enabled:
		Global.tts_say(actions_data[0].replace(",",""))
		yield(get_tree().create_timer(0.1), "timeout")
		if TTS.can_detect_is_speaking:
			wait_for_speech = true
			yield(self,"speech_completed")

	yield(get_tree().create_timer(0.01), "timeout")
	emit_signal("card_move_complete")
	
func doMusic():
	Global.playSong(actions_data[0])
	yield(get_tree().create_timer(0.01), "timeout")
	emit_signal("card_move_complete")
	
func doAmb():
	Global.playAmbience("env/" + actions_data[0])
	yield(get_tree().create_timer(0.01), "timeout")
	emit_signal("card_move_complete")

func doSfx():
	Global.playSoundFx(actions_data[0])
	yield(get_tree().create_timer(0.01), "timeout")
	emit_signal("card_move_complete")

func doWait():
	yield(get_tree().create_timer(actions_data[0]), "timeout")
	emit_signal("card_move_complete")

func doCamera():
	cameraTween(actions_data[0])
	yield(get_node("Tween"),"tween_completed")
	emit_signal("card_move_complete")

func doStartText():
	$AnimationPlayer.play("StartGame")
	Global.tts_say("Start!")
	Global.playSoundFx("card/start_duel")
	$Label.text = actions_data[0]
	yield(get_node("AnimationPlayer"),"animation_finished")
	emit_signal("card_move_complete")

func doCard():
	var data = actions_data[0]
	var card_id
	var source
	var dest
	
	if data[0] == -1:
		card_id = selected_card
		source = selected_zone
		dest = data[1]
	else:
		card_id = data[0]
		source = data[1]
		dest = data[2]
	
	if source.cards.size() > card_id:
		var card = source.cards[card_id]
		
		dest.cards.append(card)
		source.cards.remove(card.index())
		source.emit_signal("sent_card",card)
		source.tweenCard(card,dest.getOpenPosition())
		Global.playSoundFx("card/card_play")
		yield(get_node("Tween"),"tween_completed")
		card.location = dest
		dest.emit_signal("received_card",card)
		card.to_move = false
		$Node2D/PlayerField.updateFocus()
		$Node2D/OpponentField.updateFocus()
	else:
		yield(get_tree().create_timer(0.01), "timeout")
	emit_signal("card_move_complete")
	
func doSwitch():
	var data = actions_data[0]
	var source_card = data[0]
	var dest_card = data[1]
	var source = data[2]
	var dest = data[3]
	if source.cards.size() > source_card && dest.cards.size() > dest_card:
		var card1 = source.cards[source_card]
		var card2 = dest.cards[dest_card]
		
		dest.cards[dest_card] = card1
		source.cards[source_card] = card2
		
		#dest.cards.append(card)
		#source.cards.remove(card.index())
		
		
		dest.emit_signal("switch_card",card1,card2)
		source.emit_signal("switch_card",card2,card1)
		
		get_node("Tween").interpolate_property(
			card1.get_node("Sprite"),
			"position",
			card1.get_node("Sprite").position,
			card2.get_node("Sprite").position,
			0.1,
			Tween.TRANS_LINEAR,
			Tween.EASE_OUT
		)
		get_node("Tween").start()
		
		get_node("Tween2").interpolate_property(
			card2.get_node("Sprite"),
			"position",
			card2.get_node("Sprite").position,
			card1.get_node("Sprite").position,
			0.1,
			Tween.TRANS_LINEAR,
			Tween.EASE_OUT
		)
		get_node("Tween2").start()
		
		Global.playSoundFx("card/card_play")
		yield(get_node("Tween"),"tween_completed")
		yield(get_node("Tween2"),"tween_completed")
		card1.to_move = false
		card2.to_move = false
		$Node2D/PlayerField.updateFocus()
		$Node2D/OpponentField.updateFocus()
	else:
		yield(get_tree().create_timer(0.01), "timeout")
	emit_signal("card_move_complete")

# Call this to end the duel
func doEndDuel():
	var instance = null
	
	if next_scene != null:
		var test = load(next_scene)
		instance = test.instance()

	if instance != null:
		var lel = get_tree().get_root().get_node("Main/ViewportContainer/Viewport")
		lel.call_deferred("add_child",instance)
	else:
		if next_cutscene != "":
			var suffix = "Lose"
			if victory_state == VICTORY.VICTORY:
				suffix = "Win"
			
			Global.doCutscene(next_cutscene+suffix)
	queue_free()

func doEndPhase():
	current_phase = 5
	doPhase()

func doPhase():
	if current_phase == PHASES.BATTLE:
		cameraTween([boardMiddle,0.15])
		$AnimationPlayer.play("phaseDisplayBattle")
	elif current_player == 0:
		cameraTween([boardBottom,0.15])
		$AnimationPlayer.play("phaseDisplay")
	else:
		$AnimationPlayer.play("phaseDisplayEnemy")
		cameraTween([boardTop,0.15])
		
	# If its turn 1 or the player doesn't have any active character,
	# skip the battle phase
	if current_phase == PHASES.BATTLE && turns == 0:
		current_phase+=1
	if current_phase == PHASES.BATTLE && players[current_player].get_node("Active").cards.size() == 0:
		current_phase +=1
		
	if current_phase == PHASES.LIMBO:
		if players[current_player].get_node("Limbo").cards.size() == 0:
			current_phase+=1
	if current_phase == PHASES.RETURN:
		if turns == 0 || players[current_player].get_node("Bench").cards.size() == 0:
			current_phase+=1
	var phase_string = "%s Phase" % PHASE_STRINGS[current_phase]
	Global.tts_say(phase_string)
	doPhaseAction()
	$phaseLabel.text = phase_string
	if current_phase == 5:
		Global.playSoundFx("card/duel_end_turn")
		current_phase = -1
		turns+=1
		current_player = (current_player+1) % 2
		$Node2D/OpponentField.resetTurn()
		$Node2D/PlayerField.resetTurn()
	else:
		if current_phase == PHASES.BATTLE:
			Global.playSoundFx("card/duel_dialog")
		else:
			Global.playSoundFx("card/duel_phase")
	current_phase+=1;
	yield(get_node("AnimationPlayer"),"animation_finished")
	if current_phase == 3 || current_phase == 5:
		if current_player == 0:
			players[current_player].initFocus()
	resetBoosts()
	emit_signal("card_move_complete")
	
	# To Do: add proper animation

func doPhaseAction():
	if current_phase == PHASES.START:
		players[current_player].deckDraw()
		nextPhase()
	elif current_phase == PHASES.END:
		nextPhase()
	elif current_phase == PHASES.LIMBO:
		for c in players[current_player].get_node("Limbo").cards:
			if !c.card_ref.modifiers.has(c.card_ref.modifier.PERMADEATH):
				players[current_player].limboDeck(c.index())
		nextPhase()
	elif current_phase == PHASES.MAIN:
		# Computer opponent just activates his first card
		if current_player == 1 && computerOpponent:
			if players[1].testHandBench():
				addAction("ai","1:bench:Hand:0")
			addAction("ai","1:activate:Hand:0")
			if turns > 0:
				addAction("phase",0)
				#nextPhase()
				pass
			else:
				addAction("endphase",0)
				#endPhase()
				pass
		
		if current_player == 0:
			tutorialText("BattleTutorial_02")
	elif current_phase == PHASES.BATTLE:
		var playa = [null,null]
		playa[0] = players[current_player]
		playa[1] = players[(current_player+1) % 2]
		
		var cando = true
		
		# Do card effects that trigger before battle
		for c in playa:
			var activea = c.get_node("Active")
			if activea.cards.size() > 0:
				injectAction("cardeffect",[activea.cards[0],"doActiveBeforeBattle"])
			for g in range(1,activea.cards.size()):
				injectAction("cardeffect",[activea.cards[g],"doOverlayBeforeBattle"])
		
		# Do card effects that replace battle
		for c in playa:
			var activea = c.get_node("Active")
			if activea.cards.size() > 0:
				if activea.cards[0].doActiveReplaceBattleTest():
					injectAction("cardeffect",[activea.cards[0],"doActiveReplaceBattle"])
					cando = false
			for g in range(1,activea.cards.size()):
				if activea.cards[g].doOverlayReplaceBattleTest():
					injectAction("cardeffect",[activea.cards[g],"doOverlayReplaceBattle"])
					cando = false
		if cando:
			addAction("effect",[$Camera2D,"doDialogAnimation"])
		else:
			addAction("phase",0)
	elif current_phase == PHASES.RETURN:
		# Computer opponent just skips to the end phase in debugging
		if current_player == 1 && computerOpponent || players[0].switch_count == 0:
			addAction("endphase",0)
		elif current_player == 0:
			addAction("msgbox","Select a card from the bench to switch with the Active card.")
			
func cameraTween(dest):
	var pos = dest[0]
	var speed = dest[1]
	.get_node("Tween").interpolate_property(
		$Camera2D,
		"position",
		$Camera2D.position,
		pos,
		speed,
		Tween.TRANS_SINE,
		Tween.EASE_OUT
	)
	get_node("Tween").start()
	
func _on_Node2D_card_move_complete():
	pass

func resetBoosts():
	$Node2D/PlayerField.resetBoosts()
	$Node2D/OpponentField.resetBoosts()

func doColorblind():
	$Node2D/PlayerField.doColorblind()
	$Node2D/OpponentField.doColorblind()

func _input(event):
	if !is_instance_valid(box):
		if actions_working():
			if Input.is_action_just_pressed("ui_accept") || Input.is_action_just_pressed("ui_cancel"):
				if actions[0] == "text" || actions[0] == "phase" || actions[0] == "endphase":
					$AnimationPlayer.play("RESET")
				elif actions[0] == "animate":
					var data = actions_data[0]
					
					if !($Camera2D/AnimationPlayer.is_playing()):
						data[1].get_node("AnimationPlayer").play("RESET")

# Adds the action to the action history, then
# executes it
func _on_Node2D_perform_action(action):
	action = safetyString(action,25)
	action_history.append(action)
	var data = action.split(":",false)
	addAction("wait",0.01)
	if data[1] == "activate":
		players[int(data[0])].get_node(data[2]).cards[int(data[3])].doActivate()
	elif data[1] == "overlay":
		players[int(data[0])].get_node(data[2]).cards[int(data[3])].doOverlay()
	elif data[1] == "bench":
		players[int(data[0])].get_node(data[2]).cards[int(data[3])].doBench()
	elif data[1] == "switch":
		players[int(data[0])].get_node(data[2]).cards[int(data[3])].doSwitch()
