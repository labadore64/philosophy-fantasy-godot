extends Control

var destroy = false
var card = null
var card_ref = null
var viewing = false
var card_in_menu = 0

# This is the menu that pops up when you click on a card.

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("Load")
	# If you're blind you just use the infokeys instead...
	if Global.TTS_enabled:
		$ColorRect/GridContainer/View.visible = false
	$ColorRect/GridContainer/Activate.visible = false
	$ColorRect/GridContainer/Overlay.visible = false
	$ColorRect/GridContainer/Discard.visible = false
	$ColorRect/GridContainer/Select.visible = false
	$ColorRect/GridContainer/Switch.visible = false
	$ColorRect/GridContainer/Bench.visible = false
	$ColorRect/GridContainer/Use.visible = false
	Global.playSoundFx("card/duel_open_menu")
	rect_global_position = Vector2.ZERO
	call_deferred("setCardTexture")
	var duel = get_parent().get_parent()
	duel.duelMenu_open = true

func _input(event):
	for c in range(1,10):
		if Input.is_action_just_pressed("access_key"+str(c)):
			card.infokey(c-1)

func setCardTexture():
	if card_ref != null:
		$ColorRect/Light2D/Sprite.texture = card_ref.get_node("CardSprite").texture
	setFocus()
		
func setFocus():
	var visibleNodes = []
	for c in $ColorRect/GridContainer.get_children():
		if c.visible:
			visibleNodes.append(c)
	
	var sizer = visibleNodes.size()
	for c in range(0,sizer):
		visibleNodes[c].focus_neighbour_top = visibleNodes[(c-2)%sizer].get_path()
		visibleNodes[c].focus_neighbour_bottom = visibleNodes[(c+2)%sizer].get_path()
	visibleNodes[0].grab_focus()

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Unload":
		queue_free()

func _on_Cancel_pressed():
	if !destroy:
		destroy = true
		var duel = get_parent().get_parent()
		duel.duelMenu_open = false
		card.grabFocus()
		Global.playSoundFx("ui/close_dialog")
		$AnimationPlayer.play("Unload")
		get_parent().get_node("AnimationPlayer").play("Unload")
		if viewing:
			card_ref.hideCard()


func _on_View_pressed():
	if !viewing:
		Global.playSoundFx("card/duel_view")
		viewing = true
		card_ref.showCard()
		card_ref.hidemode = true
		card_ref.position.y = rect_position.y + 250
		card_ref.scale = Vector2(1.25,1.25)
		if card_ref.get_parent() == null:
			get_parent().add_child(card_ref)
		card_ref.updateDisplay()
	else:
		viewing = false
		card_ref.hideCard()
	
func _exit_tree():
	if card_ref.get_parent() != null:
		if card_ref.get_parent() == self:
			get_parent().remove_child(card_ref)


func _on_Activate_pressed():
	var duel = get_parent().get_parent()
	duel.emit_signal("perform_action","0:activate:"+card.location.name+":"+str(card.index()))
	$AnimationPlayer.play("Unload")
	get_parent().get_node("AnimationPlayer").play("Unload")
	if viewing:
		card_ref.hideCard()


func _on_Overlay_pressed():
	var duel = get_parent().get_parent()
	duel.emit_signal("perform_action","0:overlay:"+card.location.name+":"+str(card.index()))
	$AnimationPlayer.play("Unload")
	get_parent().get_node("AnimationPlayer").play("Unload")
	if viewing:
		card_ref.hideCard()


func _on_Bench_pressed():
	var duel = get_parent().get_parent()
	duel.emit_signal("perform_action","0:bench:"+card.location.name+":"+str(card.index()))
	$AnimationPlayer.play("Unload")
	get_parent().get_node("AnimationPlayer").play("Unload")
	if viewing:
		card_ref.hideCard()


func _on_Switch_pressed():
	var duel = get_parent().get_parent()
	duel.emit_signal("perform_action","0:switch:"+card.location.name+":"+str(card.index()))
	$AnimationPlayer.play("Unload")
	get_parent().get_node("AnimationPlayer").play("Unload")
	if viewing:
		card_ref.hideCard()
	if duel.current_phase == 5:
		duel.nextPhase()


func _on_View_focus_entered():
	var text = $ColorRect/GridContainer/View.text
	Global.tts_say(text)

func _on_Use_focus_entered():
	var text = $ColorRect/GridContainer/Use.text
	Global.tts_say(text)

func _on_Activate_focus_entered():
	var text = $ColorRect/GridContainer/Activate.text
	Global.tts_say(text)

func _on_Overlay_focus_entered():
	var text = $ColorRect/GridContainer/Overlay.text
	Global.tts_say(text)

func _on_Bench_focus_entered():
	var text = $ColorRect/GridContainer/Bench.text
	Global.tts_say(text)

func _on_Discard_focus_entered():
	var text = $ColorRect/GridContainer/Discard.text
	Global.tts_say(text)


func _on_Select_focus_entered():
	var text = $ColorRect/GridContainer/Select.text
	Global.tts_say(text)


func _on_Switch_focus_entered():
	var text = $ColorRect/GridContainer/Switch.text
	Global.tts_say(text)


func _on_Cancel_focus_entered():
	var text = $ColorRect/GridContainer/Cancel.text
	Global.tts_say(text)


func _on_Select_pressed():
	var duel = get_parent().get_parent()
	duel.selected_zones = []
	duel.selected_card = card.index()
	duel.selected_zone = card.location
	duel.emit_signal("card_selected")
	$AnimationPlayer.play("Unload")
	get_parent().get_node("AnimationPlayer").play("Unload")
	if viewing:
		card_ref.hideCard()
