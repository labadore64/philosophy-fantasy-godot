extends Control

var card_ref
var to_move
var location

var duel
var side
var other_side

var donotread = false

const MOVECARD_X = -350
const MOVECARD_Y = -675

onready var cardList = preload("res://map/gameplay/duel/duel/card_menu.tscn")
onready var duelMenu = preload("res://map/gameplay/duel/DuelMenu.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func setCard(card):
	if card.card_script != null:
		set_script(card.card_script)
	
func isArchetype(archetype):
	for c in card_ref.archetypes:
		if c == archetype:
			return true
	return false
	
func isArchetypeString(archetype):
	for c in card_ref.archetypes:
		if archetype == card_ref.archetype_names[c]:
			return true
	return false
	
func isName(namez):
	return card_ref.card_name == namez
	
func isElement(ele):
	return card_ref.card_element == ele

func isElementString(ele):
	return card_ref.element_names[card_ref.card_element] == ele
	
func isType(type):
	return card_ref.card_type == type
	
func isTypeString(type):
	return type == card_ref.type_names[card_ref.card_type]
	
func isModifier(modifier):
	return card_ref.modifiers.has(modifier)

func isModifierString(modifier):
	for c in card_ref.modifiers:
		if modifier == card_ref.modifier_names[c]:
			return true
	return false
	
func _init():
	var card_ref = null
	var to_move = null
	var location = null
	cardList = preload("res://map/gameplay/duel/duel/card_menu.tscn")
	duelMenu = preload("res://map/gameplay/duel/DuelMenu.tscn")
	
func cardPopulate(card):
	card_ref = card
	card_ref.parent = self
	$Sprite.texture.current_frame = card_ref.card_element
	$Sprite/Sprite2.texture.current_frame = card_ref.card_element
	updateColorblindVisibility()
	
func index():
	for c in range(0,location.cards.size()):
		if location.cards[c] == self:
			return c
	return -1
# Whether or not the card is flipped
	
var flipped = false setget flipped_set, flipped_get

func flipped_set(new_value):
	flipped = new_value
	if flipped:
		$Sprite.texture.current_frame = 6
		$Sprite/Sprite2.texture.current_frame = 0
		$Sprite/Sprite2.visible = false
	else:
		$Sprite.texture.current_frame = card_ref.card_element
		$Sprite/Sprite2.texture.current_frame = card_ref.card_element
		$Sprite/Sprite2.visible = true
	updateColorblindVisibility()

func flipped_get():
	return flipped # Getter must return a value.

func updateColorblindVisibility():
	
	if !flipped:
		$Sprite/Sprite2.visible = Global.colorblind_mode
	else:
		$Sprite/Sprite2.visible = false

func _input(event):
	if get_focus_owner() == $Sprite/Button:
		for c in range(0,10):
			if Input.is_action_just_pressed("access_key"+str(c)):
				infokey(c)
				
func cameraFocusYou():
	if side == duel.players[0]:
		duel.addAction("wait",0.25)
		duel.addAction("camera",[duel.boardBottom,0.15])
		duel.addAction("wait",0.25)
	else:
		duel.addAction("wait",0.25)
		duel.addAction("camera",[duel.boardTop,0.15])
		duel.addAction("wait",0.25)

func cameraFocusEnemy():
	if side == duel.players[1]:
		duel.addAction("wait",0.25)
		duel.addAction("camera",[duel.boardBottom,0.15])
		duel.addAction("wait",0.25)
	else:
		duel.addAction("wait",0.25)
		duel.addAction("camera",[duel.boardTop,0.15])
		duel.addAction("wait",0.25)

func _on_Button_pressed():
	if !flipped:
		var duel = get_parent().get_parent().get_parent()
		if (!duel.actions_working() && duel.duelStuff_open) || duel.selected_zones.size() > 0:
			if location.name == "Limbo" || location.name == "Discard":
				openCardList()
			else:
				var card_in_menu = 0
				for c in range(0,location.cards.size()):
					if location.cards[c] == self:
						card_in_menu = c
						break;
				
				if !is_instance_valid(get_parent().get_parent().get_parent().menu):
					get_parent().get_parent().get_parent().menu = duelMenu.instance()
					var menu = get_parent().get_parent().get_parent().menu
					menu.card_ref = card_ref
					menu.card = self
					menu.card_in_menu = card_in_menu
					get_parent().get_parent().get_parent().get_node("Camera2D").add_child(menu)
					get_parent().get_parent().get_parent().get_node("Camera2D").move_child(menu,3)
					# Update the card text
					duel.get_node("Camera2D").updateCardText(self)
					
					if menu.card.location.get_parent().enemy:
						menu.rect_position.y =- 920
				else:
					get_parent().get_parent().get_parent().menu.queue_free()
					var menu = duelMenu.instance()
					menu.card_ref = card_ref
					menu.card = self
					menu.card_in_menu = card_in_menu
					get_parent().get_parent().get_parent().menu = menu
					get_parent().get_parent().get_parent().get_node("Camera2D").add_child(menu)
					get_parent().get_parent().get_parent().get_node("Camera2D").move_child(menu,3)
					# Update the card text
					duel.get_node("Camera2D").updateCardText(self)
				doLocationStuffOnPress()
				get_parent().get_parent().get_parent().get_node("Camera2D/AnimationPlayer").play("Load")

func openCardList():
	if !is_instance_valid(get_parent().get_parent().get_parent().menu):
		var menu = cardList.instance()
		get_parent().get_parent().get_parent().menu = menu
		var arr = []
		menu.location = location
		for c in location.cards:
			arr.append(c.card_ref)
		menu.setCards(arr)
		get_parent().get_parent().get_parent().get_node("Camera2D").add_child(menu)
		return menu
	return null

func _on_Button_mouse_entered():
	grabFocus()

func _on_Button_mouse_exited():
	if !is_instance_valid(get_parent().get_parent().get_parent().menu):
		var duel = get_parent().get_parent().get_parent()
		if !duel.actions_working() || duel.selected_zones.size() > 0:
			duel.get_node("Camera2D").updateCardText(null)
		
# Depending on the menu location, updates options
func doLocationStuffOnPress():
	var menu = get_parent().get_parent().get_parent().menu
	if !location.get_parent().enemy:
		# selected mode
		if duel.selected_zones.size() > 0:
			if duel.selected_zones.has(location):
				var cando = true
				
				# Card selection conditions.
				if duel.selected_conditions.has("archetype"):
					for c in duel.selected_conditions["archetype"]:
						if !isArchetypeString(c):
							cando = false
						
				if cando:
					menu.get_node("ColorRect/GridContainer/Select").visible = true
		# normal mode
		else:	
			if location.name == "Hand":
				if location.get_parent().activate_count > 0 && location.get_parent().testHandActive():
					menu.get_node("ColorRect/GridContainer/Activate").visible = true
				if location.get_parent().overlay_count > 0 && location.get_parent().testHandOverlay():
					menu.get_node("ColorRect/GridContainer/Overlay").visible = true
				if location.get_parent().bench_count > 0 && location.get_parent().testHandBench():
					menu.get_node("ColorRect/GridContainer/Bench").visible = true
			elif location.name == "Bench":
				if location.get_parent().activate_count > 0 && location.get_parent().testBenchActive():
					menu.get_node("ColorRect/GridContainer/Activate").visible = true
				if location.get_parent().switch_count > 0 && location.get_parent().testBenchSwitch():
					menu.get_node("ColorRect/GridContainer/Switch").visible = true
				if location.get_parent().overlay_count > 0 && location.get_parent().testBenchOverlay():
					menu.get_node("ColorRect/GridContainer/Overlay").visible = true
					
func doMoveCard(action,soundfx):

	duel.addAction("wait",0.25)
	if card_ref.get_parent() != null:
		card_ref.get_parent().remove_child(card_ref)
	duel.get_node("Camera2D").add_child(card_ref)
	duel.addAction("sfx","card/"+soundfx)
	duel.addAnimation(card_ref,action)
	card_ref.position.x = MOVECARD_X
	card_ref.position.y = MOVECARD_Y

func moveCard(destination):
	location.moveCard(index(),location.get_parent().get_node(destination))

# Activation animation of this card	
func doActivate():
	donotread = true
	var stringer = "summoned %s!"
	
	var card_in_menu = 0
	
	for c in location.cards:
		if(c == self):
			break;
		card_in_menu+=1	
	card_ref.hidemode = false
	# Do animation for activation
	# action is used later to record the duels for replays
	var action
	if location.name == "Hand":
		action = duel.players[duel.current_player].handActive(card_in_menu)
	elif location.name == "Bench":
		action = duel.players[duel.current_player].benchActive(card_in_menu)
	duel.addAction("camera",[duel.boardMiddle,0.15])
	duel.addAction("tts",stringer % card_ref.card_name)
	doMoveCard("Activate","duel_summon")
	if duel.current_player == 0:
		duel.addAction("camera",[duel.boardBottom,0.15])
	else:
		duel.addAction("camera",[duel.boardTop,0.15])
	duel.addAction("cardeffect",[self,"doSummon"])
	duel.addAction("focus",location.cards[card_in_menu])
	duel.batchMovement()
	
	# decrement the activation count.
	
	duel.players[duel.current_player].activate_count-=1
			

# Activation animation of this card	
func doSwitch():
	donotread = true
	var stringer = "switched %s!"
	
	var card_in_menu = 0
	
	for c in location.cards:
		if(c == self):
			break;
		card_in_menu+=1	
	card_ref.hidemode = false
	# Do animation for activation
	# action is used later to record the duels for replays
	
	var action = duel.players[duel.current_player].benchSwitch(card_in_menu)
	duel.addAction("camera",[duel.boardMiddle,0.15])
	duel.addAction("tts",stringer % card_ref.card_name)
	doMoveCard("Activate","duel_summon")
	if duel.current_player == 0:
		duel.addAction("camera",[duel.boardBottom,0.15])
	else:
		duel.addAction("camera",[duel.boardTop,0.15])
	duel.addAction("focus",location.cards[card_in_menu])
	duel.batchMovement()
	
	# decrement the activation count.
	
	duel.players[duel.current_player].switch_count-=1
			
# Overlay animation of this card	
func doOverlay():
	donotread = true
	var stringer = "overlayed %s!"
	
	var card_in_menu = 0
	
	for c in location.cards:
		if(c == self):
			break;
		card_in_menu+=1	
	card_ref.hidemode = false
	# Do animation for activation
	# action is used later to record the duels for replays
	var action = null
	if location.name == "Hand":
		action = duel.players[duel.current_player].handOverlay(card_in_menu)
	elif location.name == "Bench":
		action = duel.players[duel.current_player].benchOverlay(card_in_menu)
		
	duel.addAction("camera",[duel.boardMiddle,0.15])
	duel.addAction("tts",stringer % card_ref.card_name)
	doMoveCard("Overlay","duel_overlay")
	if duel.current_player == 0:
		duel.addAction("camera",[duel.boardBottom,0.15])
	else:
		duel.addAction("camera",[duel.boardTop,0.15])
	duel.addAction("focus",location.cards[card_in_menu])
	duel.batchMovement()
	
	# decrement the activation count.
	
	duel.players[duel.current_player].overlay_count-=1

# Does the hand-to-bench animation for this card.
func doBench():
	donotread = true
	var stringer = "benched %s!"
	
	var card_in_menu = 0
	
	for c in location.cards:
		if(c == self):
			break;
		card_in_menu+=1	
	card_ref.hidemode = false
	# Do animation for activation
	# action is used later to record the duels for replays
	var action = duel.players[duel.current_player].handBench(card_in_menu)
	duel.addAction("tts",stringer % card_ref.card_name)
	doMoveCard("Bench","duel_bench")
	card_ref.position.x = MOVECARD_X
	card_ref.position.y = MOVECARD_Y
	
	duel.addAction("focus",location.cards[card_in_menu])
	
	duel.batchMovement()
	
	# decrement the activation count.
	
	duel.players[duel.current_player].bench_count-=1


# Does the hand-to-bench animation for this card.
func doUse():
	var stringer = "activated %s!"
	
	var card_in_menu = 0
	
	for c in location.cards:
		if(c == self):
			break;
		card_in_menu+=1	
	card_ref.hidemode = false
	# Do animation for activation
	# action is used later to record the duels for replays
	duel.addAction("tts",stringer % card_ref.card_name)
	doMoveCard("Use","duel_use")
	card_ref.position.x = MOVECARD_X
	card_ref.position.y = MOVECARD_Y
	duel.batchMovement()

	# decrement the activation count.

func setCardActivated():
	var test = location.get_parent()
	testAndPopulate(test.times_used,self)
	testAndPopulate(test.name_times_used,card_ref.card_name)
	testAndPopulate(test.times_used_duel,self)
	testAndPopulate(test.name_times_used_duel,card_ref.card_name)

func testAndPopulate(test,value):
	if test.has(value):
		var val = test[value]
		test[value] = val+1
	else:
		test[value] = 1
		

func _on_Button_focus_entered():
	if !is_instance_valid(get_parent().get_parent().get_parent().menu):
		if !duel.actions_working():
			duel.get_node("Camera2D").updateCardText(self)
			readAccessibleName()
		
func updateFocus():
	$Sprite/Button.focus_neighbour_bottom = ""
	$Sprite/Button.focus_neighbour_right = ""
	$Sprite/Button.focus_neighbour_top = ""
	$Sprite/Button.focus_neighbour_left = ""
	
	var sizer = location.cards.size()
	
	if location.name == "Hand":
		if location.get_parent().enemy:
			if sizer > 1:
				$Sprite/Button.focus_neighbour_right = location.cards[(index()-1+sizer) % sizer].get_node("Sprite/Button").get_path()
				$Sprite/Button.focus_neighbour_left = location.cards[(index()+1+sizer) % sizer].get_node("Sprite/Button").get_path()
				if index() == sizer-1:
					$Sprite/Button.focus_neighbour_left = ""
				if index() == 0:
					$Sprite/Button.focus_neighbour_right = ""
		else:
			if sizer > 1:
				$Sprite/Button.focus_neighbour_left = location.cards[(index()-1+sizer) % sizer].get_node("Sprite/Button").get_path()
				$Sprite/Button.focus_neighbour_right = location.cards[(index()+1+sizer) % sizer].get_node("Sprite/Button").get_path()
				if index() == sizer-1:
					$Sprite/Button.focus_neighbour_right = ""
				if index() == 0:
					$Sprite/Button.focus_neighbour_left = ""
	
	if location.name == "Active":
		if location.get_parent().enemy:
			if sizer > 1:
				$Sprite/Button.focus_neighbour_bottom = location.cards[(index()-1+sizer) % sizer].get_node("Sprite/Button").get_path()
				$Sprite/Button.focus_neighbour_top = location.cards[(index()+1+sizer) % sizer].get_node("Sprite/Button").get_path()
				if index() == sizer-1:
					$Sprite/Button.focus_neighbour_bottom = ""
				if index() == 0:
					$Sprite/Button.focus_neighbour_top = ""
		else:
			if sizer > 1:
				$Sprite/Button.focus_neighbour_top = location.cards[(index()-1+sizer) % sizer].get_node("Sprite/Button").get_path()
				$Sprite/Button.focus_neighbour_bottom = location.cards[(index()+1+sizer) % sizer].get_node("Sprite/Button").get_path()
				if index() == sizer-1:
					$Sprite/Button.focus_neighbour_top = ""
				if index() == 0:
					$Sprite/Button.focus_neighbour_bottom = ""
	
func grabFocus():
	var duel = get_parent().get_parent().get_parent()
	if (duel.duelStuff_open && !duel.duelMenu_open):
		$Sprite/Button.grab_focus()
		readAccessibleName()

func _on_Button_focus_exited():
	Global.tts_stop()

func readAccessibleName():
	if Global.TTS_enabled:
		var duel = get_parent().get_parent().get_parent()
		var namez = ""
		
		if duel.last_spoken_location != location:
			namez = location.name
			duel.last_spoken_location = location
		
		if flipped:
			namez = location.name
			if !location.get_parent().enemy:
				namez += " cards: " + str(location.cards.size())
		else:
			namez += card_ref.card_name
			
		if location.get_parent().enemy:
			namez += " enemy"
		
		Global.tts_say(namez)

func resetBoosts():
	card_ref.resetBoosts()

func infokey(index):
	if index == 0:
		Global.tts_say(card_ref.card_name)
	elif index == 1:
		Global.tts_say(str(card_ref.attack) + "Attack")
	elif index == 2:
		Global.tts_say(str(card_ref.defense) + "Defense")
	elif index == 3:
		Global.tts_say(str(card_ref.attack_overlay) + "Overlay Attack")
	elif index == 4:
		Global.tts_say(str(card_ref.defense_overlay) + "Overlay Defense")
	elif index == 5:
		var card_text = "Archetypes: "
		
		for c in card_ref.archetypes:
			card_text += card_ref.archetype_names[c] + ", "
		
		card_text += "Modifiers: "
		
		for c in card_ref.modifiers:
			card_text += card_ref.modifier_names[c] + ", "
		
		Global.tts_say(card_text)
	elif index == 6:
		Global.tts_say(card_ref.element_names[card_ref.card_element] + " Type")
	elif index == 7:
		Global.tts_say(str(card_ref.card_text))
	elif index == 8:
		var card_text = "Weakness: " + card_ref.element_names[card_ref.weakness] + str(card_ref.weakness_amount)
		card_text += " Resist: " + card_ref.element_names[card_ref.resist] + str(card_ref.resist_amount)
		Global.tts_say(card_text)
	elif index == 9:
		Global.tts_say(card_ref.card_flavor)
# Battle commands

# Name structure:
# To - As the card moves to this section
# From - As the card moves out of this section
# As - An activate effect in this particular section
# Test - tests if the action can be performed

func doToLimbo():
	pass

func doToLimboTest():
	return false

func doFromLimbo():
	pass
	
func doFromLimboTest():
	return false
	
func doAsLimbo():
	pass
	
func doAsLimboTest():
	return false
	
func doToHand():
	pass
	
func doToHandTest():
	return false
	
func doFromHand():
	pass
	
func doFromHandTest():
	return false
	
func doAsHand():
	pass
	
func doAsHandTest():
	return false
	
func doToDeck():
	pass
	
func doToDeckTest():
	return false

func doFromDeck():
	pass
	
func doFromDeckTest():
	return false
	
# No do-as-deck because thats silly
	
func doToBench():
	pass
	
func doToBenchTest():
	return false
	
func doFromBench():
	pass
	
func doFromBenchTest():
	return false
	
func doAsBench():
	pass
	
func doAsBenchTest():
	return false
	
func doToTheory():
	pass
	
func doToTheoryTest():
	return false
	
func doFromTheory():
	pass
	
func doFromTheoryTest():
	return false
	
func doAsTheory():
	pass
	
func doAsTheoryTest():
	return false
	
func doToDiscard():
	pass
	
func doToDiscardTest():
	return false
	
func doFromDiscard():
	pass
	
func doFromDiscardTest():
	return false
	
func doAsDiscard():
	pass
	
func doAsDiscardTest():
	return false
	
func doActiveBeforeBattle():
	pass
	
func doActiveBeforeBattleTest():
	return false
	
func doOverlayBeforeBattle():
	pass
	
func doOverlayBeforeBattleTest():
	return false
	
func doActiveReplaceBattle():
	pass
	
func doActiveReplaceBattleTest():
	return false
	
func doOverlayReplaceBattle():
	pass
	
func doOverlayReplaceBattleTest():
	return false

func doActiveAfterBattle():
	pass
	
func doActiveAfterBattleTest():
	return false

func doOverlayAfterBattle():
	pass
	
func doOverlayAfterBattleTest():
	return false

## fill out more battle functions here

func doSummon():
	pass
	
func doSummonTest():
	return false

## some other useful functions!

func doPhaseAction(phase):
	pass
	
func doPhaseActionTest(phase):
	return false
