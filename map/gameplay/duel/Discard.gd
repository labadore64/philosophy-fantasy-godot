extends "res://map/gameplay/duel/CardPlace.gd"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Only draw the top card
func _on_Discard_received_card(card):
	card.flipped = false
	for c in range(0,cards.size()):
		if c != cards.size()-1:
			cards[c].get_node("Sprite").visible = false
		else:
			cards[c].get_node("Sprite").visible = true
