extends "res://map/gameplay/duel/CardPlace.gd"

# Returns the movement position for moving cards
func getOpenPosition(size=cards.size()):
	if size <= 5:
		return position + Vector2((size-2.5)*170,0)
	else:
		var spacer = 850/(size+1)
		var halver = size*0.5 - 0.5
		var counter = size
		return position + Vector2((counter-halver)*spacer,0)

# The problem is that the size of the section is always the final
# result every time. the size should update as the animation progresses.
# or some counter variable.

func _on_Hand_received_card(card):
	# get the card child indices
	var indices = []
	var lowest = 100000
	
	for c in cards:
		indices.append(c)
		lowest = min(get_node(c.get_path()).get_index(),lowest)
		
	for c in cards:
		get_parent().move_child(c,lowest)
	
	card.get_node("Sprite").visible = true
	if cards.size() > 5:
		var spacer = 850/(cards.size()+1)
		var halver = cards.size()*0.5 - 0.5
		var counter = 0
		for c in cards:
			counter+=1
			c.get_node("Sprite").position = position + Vector2((counter-halver)*spacer,0)
	else:
		var counter = 0
		for c in cards:
			counter+=1
			c.get_node("Sprite").position = position + Vector2((counter-2.5)*170,0)
	
	if get_parent().enemy:
		card.flipped = true


func _on_Hand_sent_card(card):
	_on_Hand_received_card(card)
