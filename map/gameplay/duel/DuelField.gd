extends Control

signal start_debug()

var enemy = false
var difficulty = 0

var game
var player = 0

var overlay_count = 1
var activate_count = 1
var bench_count = 1
var switch_count = 1
var effect_count = 1
var metaphys_count = 1
var discard_count = 1
var limbo_count = 1
var times_used = {}			# How many times a card has been used in a turn
var name_times_used = {}	# How many times a card of a certain name has been used in a turn
var times_used_duel = {}	# How many times a card has been used in a duel
var name_times_used_duel = {}	# How many times a card has been used with a certain name in a duel

var hp

var status_effect = {}

func addStatus(name,turns):
	if status_effect.has(name):
		status_effect[name] += turns
	else:
		status_effect[name] = turns
		
func hasStatus(name):
	return status_effect.has(name)
	
func decrementStatus():
	for c in status_effect:
		status_effect[c] -=1
		if status_effect[c] <= 0:
			status_effect.erase(c)
			
func decrementStatusEffect(name):
	status_effect[name] -=1
	if status_effect[name] <= 0:
		status_effect.erase(name)

func _ready():
	game = get_parent().get_parent()

# Movement functions
func deckDraw():
	$Deck.moveCard(0, $Hand)
	return "draw:0"

# Only returns true if at least 1 card is in the deck.
func testDeckDraw():
	if $Deck.size() > 0:
		return true
	return false

func handActive(index):
	# Removes all cards overlayed
	for c in range(0,$Active.cards.size()):
		$Active.moveCard(0,$Limbo)
	$Hand.moveCard(index, $Active)
	return "handActivate:"+str(index)
	
func updateFocus():
	get_node("Active").setFocus()
	get_node("Bench").setFocus()
	get_node("Deck").setFocus()
	get_node("Hand").setFocus()
	get_node("Limbo").setFocus()
	get_node("Discard").setFocus()
	get_node("Theory").setFocus()

func initFocus():
	get_node("Hand").cards[0].grabFocus()
	
# Can only activate in main phase
func testHandActive():
	return game.current_phase == 3
	
func handOverlay(index):
	if ($Active.cards.size() < $Active.cards[0].card_ref.overlay_count + 1):
		$Hand.moveCard(index, $Active)
	else:
		$Active.moveCard($Active.cards[0].card_ref.overlay_count-1, $Limbo)
		$Hand.moveCard(index, $Active)
	return "handOverlay:"+str(index)

func testHandOverlay():
	if $Active.cards.size() == 0:
		return false
	return game.current_phase <= 3

func handBench(index):
	$Hand.moveCard(index,$Bench)
	return "handBench:"+str(index)

func testHandBench():
	return $Bench.cards.size() < 3 && game.current_phase < 4
	
func handLimbo(index):
	$Hand.moveCard(index,$Limbo)
	return "handLimbo:"+str(index)
	
func testHandLimbo():
	return true
	
func handDiscard(index):
	$Hand.moveCard(index,$Discard)
	return "handDiscard:"+str(index)
	
func testHandDiscard():
	return true
	
func limboDeck(index):
	$Limbo.moveCard(index,$Deck)
	
func testLimboDeck():
	return $Limbo.cards.size() > 0
	
func benchOverlay(index):
	$Bench.moveCard(index, $Active)
	return "benchOverlay:"+str(index)

func testBenchOverlay():
	if $Active.cards.size() == 0:
		return false
	return ($Active.cards.size() < $Active.cards[0].card_ref.overlay_count + 1) && game.current_phase == 3

func benchActive(index):
	# Removes all cards overlayed
	for c in range(0,$Active.cards.size()):
		$Active.moveCard(0,$Limbo)
	$Bench.moveCard(index, $Active)
	return "benchActivate:"+str(index)
	
# Always returns true (you can always activate
func testBenchActive():
	return game.current_phase == 3
	
func benchSwitch(index):
	for c in range(1,$Active.cards.size()):
		$Active.moveCard(1,$Limbo)
	$Bench.switchCard(index,0,$Active)
	
func testBenchSwitch():
	return true
		
func _on_Node2D_start_debug():
	pass

func resetTurn():
	overlay_count = 1
	activate_count = 1
	effect_count = 1
	metaphys_count = 1
	discard_count = 1
	limbo_count = 1
	bench_count = 1
	switch_count = 1
	times_used = {}	
	name_times_used = {}	
	decrementStatus()
	
func setHP(amount):
	get_parent().get_parent().get_node("Tween").interpolate_property(
		hp,
		"value",
		hp.value,
		amount,
		0.15,
		Tween.TRANS_LINEAR,
		Tween.EASE_OUT
	)
	get_parent().get_parent().get_node("Tween").start()

func doColorblind():
	get_node("Active").doColorblind()
	get_node("Bench").doColorblind()
	get_node("Theory").doColorblind()
	get_node("Limbo").doColorblind()
	get_node("Discard").doColorblind()
	get_node("Deck").doColorblind()
	get_node("Hand").doColorblind()
	
func resetBoosts():
	get_node("Active").resetBoosts()
	get_node("Bench").resetBoosts()
	get_node("Theory").resetBoosts()
	get_node("Limbo").resetBoosts()
	get_node("Discard").resetBoosts()
	get_node("Deck").resetBoosts()
	get_node("Hand").resetBoosts()


func _on_Bench_sent_card(card):
	pass # Replace with function body.
