extends Panel

var card_ref = null

# Called when the node enters the scene tree for the first time.
func _ready():
	rect_global_position = Vector2(
							0,
							0
							)

func setCard():
	$TextureRect.visible = true
	$TextureRect.texture.current_frame = card_ref.card_element
	$Label.text = card_ref.card_name
	$Attack.text = str(card_ref.base_attack) + "/+" + str(card_ref.attack_overlay)
	$Defence.text = str(card_ref.base_defense)+"/+"+ str(card_ref.defense_overlay)
	
func selected():
	self_modulate = Color(1,1,1,1)
	get_parent().get_parent().get_parent().get_parent().selected(self)

func unselected():
	self_modulate = Color(1,1,1,0)

func _on_Panel_gui_input(event):
	if event is InputEventMouseButton:
		if Input.is_action_just_pressed("ui_accept"):
			selected()

func readAccessibleName():
	if Global.TTS_enabled:
		var duel = get_parent().get_parent().get_parent()
		var namez = ""

		namez += card_ref.card_name
		
		Global.tts_say(namez)

func infokey(index):
	if index == 0:
		Global.tts_say(card_ref.card_name)
	elif index == 1:
		Global.tts_say(str(card_ref.attack) + "Attack")
	elif index == 2:
		Global.tts_say(str(card_ref.defense) + "Defense")
	elif index == 3:
		Global.tts_say(str(card_ref.attack_overlay) + "Overlay Attack")
	elif index == 4:
		Global.tts_say(str(card_ref.defense_overlay) + "Overlay Defense")
	elif index == 5:
		var card_text = "Archetypes: "
		
		for c in card_ref.archetypes:
			card_text += card_ref.archetype_names[c] + ", "
		
		card_text += "Modifiers: "
		
		for c in card_ref.modifiers:
			card_text += card_ref.modifier_names[c] + ", "
		
		Global.tts_say(card_text)
	elif index == 6:
		Global.tts_say(card_ref.element_names[card_ref.card_element] + " Type")
	elif index == 7:
		Global.tts_say(str(card_ref.card_text))
	elif index == 8:
		var card_text = "Weakness: " + card_ref.element_names[card_ref.weakness] + str(card_ref.weakness_amount)
		card_text += " Resist: " + card_ref.element_names[card_ref.resist] + str(card_ref.resist_amount)
		Global.tts_say(card_text)
	elif index == 9:
		Global.tts_say(card_ref.card_flavor)
