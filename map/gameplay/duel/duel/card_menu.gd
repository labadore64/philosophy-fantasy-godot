extends Control

var array = []
var arrayObject
var destroyed = false
var location = null
var selected = null
var selected_index = 0
var queued_action = ""
var card_selected =false

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.playSoundFx("card/duel_list")
	$AnimationPlayer.play("Load")
	$Node2D.visible = true
	rect_position = Vector2(
							-1200, 
							-690
							)

func setCards(arr):
	arrayObject = preload("res://map/gameplay/duel/duel/CardMenuPart.tscn")
	for c in arr:
		var card = c.parent
		var duel = card.duel
		
		var cando = true;
		# Conditions
		if !duel.selected_conditions.empty():
			if duel.selected_conditions.has("archetype"):
				for z in duel.selected_conditions["archetype"]:
					if !card.isArchetypeString(z):
						cando = false
			if duel.selected_conditions.has("type"):
				for z in duel.selected_conditions["type"]:
					if !card.isTypeString(duel.selected_conditions[z]):
						cando = false
			if duel.selected_conditions.has("element"):
				for z in duel.selected_conditions["element"]:
					if !card.isElementString(duel.selected_conditions[z]):
						cando = false
			if duel.selected_conditions.has("modifier"):
				for z in duel.selected_conditions["modifier"]:
					if !card.isModifierString(duel.selected_conditions[z]):
						cando = false
			if duel.selected_conditions.has("att>"):
				if !card.card_ref.base_attack > duel.selected_conditions["att>"]:
					cando = false	
				else:
					cando = true
			if duel.selected_conditions.has("att<"):
				if !card.card_ref.base_attack < duel.selected_conditions["att<"]:
					cando = false	
				else:
					cando = true
			if duel.selected_conditions.has("att="):
				if !card.card_ref.base_attack < duel.selected_conditions["att="]:
					cando = false	
				else:
					cando = true
			if duel.selected_conditions.has("def>"):
				if !card.card_ref.base_defense > duel.selected_conditions["def>"]:
					cando = false
				else:
					cando = true
			if duel.selected_conditions.has("def<"):
				if !card.card_ref.base_defense < duel.selected_conditions["def<"]:
					cando = false	
				else:
					cando = true
			if duel.selected_conditions.has("def="):
				if !card.card_ref.base_defense < duel.selected_conditions["def="]:
					cando = false
				else:
					cando = true
		if cando:
			var obj = arrayObject.instance()
			array.append(obj)
			$ScrollContainer/VBoxContainer/GridContainer.add_child(obj)
			obj.card_ref = c
			obj.call_deferred("setCard")
	array[0].call_deferred("selected")
	$Label.text = location.name
	
func selected(card):
	clearSelected(card)
	selected = card
	selected_index = array.find(card)
	selected.readAccessibleName()
	$Node2D.copy(card.card_ref)
	
func showUse(card):
	$Use.visible = false
	var cardo = card.card_ref.parent
	if location.name == "Limbo":
		if cardo.doAsLimboTest():
			$Use.visible = true
	elif location.name == "Deck":
		if cardo.doAsDeckTest():
			$Use.visible = true
	elif location.name == "Discard":
		if cardo.doAsDiscardTest():
			$Use.visible = true
			
func _on_NextPhase_pressed():
	if !destroyed:
		$AnimationPlayer.play("Unload")
		destroyed = true
		Global.playSoundFx("ui/menu_cancel")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Unload":
		queue_free()
		
func clearSelected(card):
	for c in array:
		if c != card:
			c.unselected()
		
func _input(event):
	if event is InputEventKey:
		if selected != null:
			for c in range(0,10):
				if Input.is_action_just_pressed("access_key"+str(c)):
					selected.infokey(c)
		
		if get_focus_owner() == $NextPhase:
			if Input.is_action_just_pressed("ui_up"):
				selected.unselected()
				selected = null
				$NextPhase.release_focus()
				array[array.size()-1].selected()
			elif Input.is_action_just_pressed("ui_down"):
				selected.unselected()
				selected = null
				$NextPhase.release_focus()
				array[0].selected()
		else:
			if Input.is_action_just_pressed("ui_up"):
				if selected_index == 0:
					$NextPhase.grab_focus()
					Global.tts_say($NextPhase.text)
				else:
					array[(selected_index-1+array.size()) % array.size()].selected()
			elif Input.is_action_just_pressed("ui_down"):
				if selected_index == array.size()-1:
					$NextPhase.grab_focus()
					Global.tts_say($NextPhase.text)
				else:
					array[(selected_index+1+array.size()) % array.size()].selected()


func _on_Control_tree_exiting():
	location.cards[0].grabFocus()
	if card_selected:
		var card = selected.card_ref.parent
		var duel = card.duel
		duel.emit_signal("card_selected")


func _on_Use_pressed():
	if !destroyed:
		$AnimationPlayer.play("Unload")
		destroyed = true
		Global.playSoundFx("ui/menu_cancel")
		var card = selected.card_ref.parent
		var duel = card.duel
		duel.selected_zones = []
		duel.selected_card = card.index()
		duel.selected_zone = card.location
		card_selected = true
		
