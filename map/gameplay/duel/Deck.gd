extends "res://map/gameplay/duel/CardPlace.gd"

var upside_down = true

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func startDeck(deck):
	populate(deck)
	flipDeck()
	
func configureDeck():
	for c in cards:
		c.get_node("Sprite").visible = false
		c.flipped = !upside_down
	if cards.size() > 0:
		cards[0].get_node("Sprite").visible = true
		cards[0].get_node("Sprite").position = getOpenPosition()
		if cards.size() > 1:
			cards[1].get_node("Sprite").visible = true
			cards[1].get_node("Sprite").position = getOpenPosition()+Vector2(-10,-10)

func flipDeck():
	configureDeck()
	upside_down = !upside_down
	for c in cards:
		c.flipped = !c.flipped

func _on_Deck_received_card(card):
	card.get_node("Sprite").visible = true
	configureDeck()
	card.flipped = true
	
	if get_parent().enemy:
		card.flipped = true

func _on_Deck_sent_card(card):
	card.get_node("Sprite").visible = true
	configureDeck()
	card.flipped = false
	card.get_node("Sprite").visible = true
	
	if get_parent().enemy:
		card.flipped = true
	pass
