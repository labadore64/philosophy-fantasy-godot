extends Control

var selected = false
var parent

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("Load")
	$Control/GridContainer/EndTurn.grab_focus()

func _on_EndTurn_pressed():
	if !selected:
		selected = true
		$AnimationPlayer.play("Unload")
		Global.playSoundFx("ui/menu_cancel")
		parent.regrab2()

func _on_NextPhase_pressed():
	if !selected:
		selected = true
		$AnimationPlayer.play("Unload")
		parent.get_node("AnimationPlayer").play("Unload")
		var duel = get_parent().get_parent()
		duel.addAction("hp",[duel.players[0],30])
		duel.addAction("wait",0.1)
		duel.batchMovement()
		

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Unload":
		queue_free()
