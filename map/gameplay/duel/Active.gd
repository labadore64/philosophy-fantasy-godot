extends "res://map/gameplay/duel/CardPlace.gd"

# Returns the movement position for moving cards
func getOpenPosition(size=cards.size()):
	return position + Vector2(0,-40+(2-(size-2))*20)

func _on_Active_received_card(card):
	card.get_node("Sprite").visible = true
	
	# reverse the order of the cards
	
	# get the card child indices
	var indices = []
	var lowest = 100000
	
	for c in cards:
		indices.append(c)
		lowest = min(get_node(c.get_path()).get_index(),lowest)
		
	for c in cards:
		get_parent().move_child(c,lowest)
	card.flipped = false
	
	# update the active card's stats to match the overlays
	if cards.size() > 0:
		var overlays = []
		cards[0].get_node("Sprite").position = getOpenPosition(0)-Vector2(0,25)
		for c in range(1,cards.size()):
			cards[c].get_node("Sprite").position = getOpenPosition(c)-Vector2(0,25)
			overlays.append(cards[c].card_ref)
		cards[0].card_ref.overlayed_cards = overlays
		cards[0].card_ref.updateDisplay()

func _on_Active_sent_card(card):
	card.card_ref.overlayed_cards = []
	_on_Active_received_card(card)
