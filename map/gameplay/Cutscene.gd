extends Node2D

var actions = []

# Dialog box handler for loading commands preloaded scene
var textboxscn = preload("res://Scene/UI/dialog/DialogBoxHandler.tscn")
var menu = null
var next = null
var goToLast = false
var last = null
var next_cutscene
var music
var ambience
var duel_name

func _ready():
	$AnimationPlayer.play("FadeIn")

func startCommands():
	menu = textboxscn.instance()
	 # completes the scene when the manager dies
	add_child(menu)
	menu.connect("tree_exited",self,"completeScene")
	menu.startAction(actions)
	
func completeScene():
	$AnimationPlayer.play("fadeOut")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "FadeIn":
		startCommands()
	if anim_name == "fadeOut":
		var instance = null
		if next != null:
			var test = load(next)
			instance = test.instance()
		if goToLast:
			instance = load(last).instance()

		if instance != null:
			if instance.has_method("loadDuelData"):
				instance.duel_name = duel_name
			
			var lel = get_tree().get_root().get_node("Main")
			lel.call_deferred("add_child",instance)
		else:
			if next_cutscene != "":
				Global.doCutscene(next_cutscene)
		Global.ambienceStop()
		queue_free()
		
