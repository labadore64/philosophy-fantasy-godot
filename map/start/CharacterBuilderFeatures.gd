extends "res://access/NewAccessible.gd"

onready var button = preload("res://map/start/CharacterButton.tscn")
onready var buttonSpecial = preload("res://map/start/CharacterButtonSpecial.tscn")

var property = ""
var property2 = ""
var portrait = null

var _v_scroll1 = null
var scroll1_space = 1

var selectedIndex1 = 0

func init():
	_v_scroll1 = $Panel/ScrollContainer.get_v_scrollbar()
	yield(get_tree(), "idle_frame")
	scroll1_space = 210
	$AnimationPlayer.play("FadeIn")

func initer():
	var files = list_files_in_directory("res://texture/faces/")
	if property2 == "":
		for c in files:
			var instance = button.instance()
			instance.name = c
			instance.get_node("TextureRect").texture = load("res://texture/faces/" + c)
			instance.visible = true
			instance.connect("pressed",self,"buttonPress")
			$Panel/ScrollContainer/GridContainer.add_child(instance)
	else:
		for c in files:
			var instance = buttonSpecial.instance()
			instance.name = c
			instance.get_node("backTexture").texture = load("res://texture/faces/" + c)
			instance.get_node("frontTexture").texture = load("res://texture/faces/" + c.replace(property,property2))
			instance.visible = true
			instance.connect("pressed",self,"buttonPress")
			$Panel/ScrollContainer/GridContainer.add_child(instance)
	if name != "Control":
		add_text(name)
	get_nodes(self)
	for i in range(0,items.size()):
		setNewNeighbor(i)
	if items.size() > 0:
		items[selected_index].grab_focus()
	
func setNewNeighbor(index):
	if index+3 < items.size()-1:
		items[index].focus_neighbour_bottom = items[index+3].get_path()
	if index-3 > 0:
		items[index].focus_neighbour_top = items[index-3].get_path()
	
func buttonPress():
	if property2 == "":
		var texture = selected_node.get_node("TextureRect").texture
		portrait.setPropertyTexture(property,texture)
	else:
		var texture = selected_node.get_node("backTexture").texture
		portrait.setPropertyTexture(property,texture)
		texture = selected_node.get_node("frontTexture").texture
		portrait.setPropertyTexture(property2,texture)
	$AnimationPlayer.play("FadeOut")
	
func list_files_in_directory(path):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "":
			break
		if file.begins_with(property + "0") && not file.begins_with(".") && not file.ends_with(".import"):
			files.append(file)

	dir.list_dir_end()

	return files

func focused():
	.focused()
	selectedIndex1 = selected_index

func _input(event):
	if !disabled:
		if Input.is_action_just_pressed("ui_up"):
			selectedIndex1-=1
			if selectedIndex1 < 0:
				selectedIndex1 = items.size()-1
			# Selected item is above current pos
			if selectedIndex1 == items.size()-1:
				_v_scroll1.value = _v_scroll1.max_value
			else: 
				_v_scroll1.value = (selectedIndex1-2) * scroll1_space
			Global.playSoundFx("ui/menu_up")
		elif Input.is_action_just_pressed("ui_down"):
			selectedIndex1+=1
			# Selected item is above current pos
			if selectedIndex1 > items.size()-1:
				selectedIndex1 = 0
			# Selected item is above current pos
			if selectedIndex1 == 0:
				_v_scroll1.value = 0
			else: 
				_v_scroll1.value = (selectedIndex1-2) * scroll1_space
			Global.playSoundFx("ui/menu_down")
		elif Input.is_action_just_pressed("ui_left"):
			Global.playSoundFx("ui/menu_left")
		elif Input.is_action_just_pressed("ui_right"):
			Global.playSoundFx("ui/menu_right")
	if Input.is_action_just_pressed("ui_cancel"):
		$AnimationPlayer.play("FadeOut")
		accept_event()
		Global.playSoundFx("ui/menu_cancel")
		return
	elif Input.is_action_just_pressed("ui_selected") || Input.is_action_just_pressed("ui_accept"):
		Global.playSoundFx("ui/menu_open")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "FadeOut":
		queue_free()
