extends "res://access/NewAccessible.gd"

var pressed = false


# Called when the node enters the scene tree for the first time.
func _ready():
	SaveFile.clearFile()
	Global.playSong("")
	$AnimationPlayer.play("FadeIn")
	$Control/LineEdit.text = SaveFile.player_name

func _exit_tree():
	var instance = load("res://map/start/CharacterBuilder.tscn").instance()
	get_parent().call_deferred("add_child",instance)

func _on_Button_pressed():
	if !pressed:
		if $Control/LineEdit.text != "":
			SaveFile.player_name = $Control/LineEdit.text
			$AnimationPlayer.play("FadeOut")
			pressed = true
			Global.playSoundFx("ui/menu_open")
		else:
			Global.playSoundFx("ui/menu_error")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "FadeOut":
		queue_free()


func _on_LineEdit_text_entered(new_text):
	_on_Button_pressed()
	
func _input(event):
	if event is InputEventKey:
		if Input.is_action_just_pressed("ui_up"):
			Global.playSoundFx("ui/menu_up")
		if Input.is_action_just_pressed("ui_down"):
			Global.playSoundFx("ui/menu_down")
