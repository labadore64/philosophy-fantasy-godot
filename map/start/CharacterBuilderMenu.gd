extends "res://access/NewAccessible.gd"

onready var portrait = get_parent().get_node("CharacterPortrait")
onready var colorMenu = preload("res://UI/ColorSelect.tscn")
onready var featureMenu = preload("res://map/start/CharacterBuilderFeatures.tscn")

var _v_scroll1 = null
var _v_scroll2 = null

var scroll1_space = 1
var scroll2_space = 1

var selectedIndex1 = 0
var selectedIndex2 = 0

# Called when the node enters the scene tree for the first time.
func onReady():
	portrait = get_parent().get_node("CharacterPortrait")
	_v_scroll1 = $Selector/Features/ScrollContainer.get_v_scrollbar()
	_v_scroll2 = $Selector/Colors/ScrollContainer.get_v_scrollbar()
	loadSprite()
	loadColor()
	# check if you can modify any of the parts
	for c in SaveFile.available_properties:
		if SaveFile.available_properties[c].empty():
			if $Selector/Features/ScrollContainer/VBoxContainer/GridContainer.get_node(c + "Container") != null:
				$Selector/Features/ScrollContainer/VBoxContainer/GridContainer.get_node(c + "Container").queue_free()
			if $Selector/Features/ScrollContainer/VBoxContainer/GridContainer.get_node(c + "Sprite") != null:
				$Selector/Features/ScrollContainer/VBoxContainer/GridContainer.get_node(c + "Sprite").queue_free()
			if $Selector/Features/ScrollContainer/VBoxContainer/GridContainer.get_node(c + "SpriteLabel") != null:
				$Selector/Features/ScrollContainer/VBoxContainer/GridContainer.get_node(c + "SpriteLabel").queue_free()
			if $Selector/Colors/ScrollContainer/VBoxContainer/GridContainer.get_node(c + "Color") != null:
				$Selector/Colors/ScrollContainer/VBoxContainer/GridContainer.get_node(c + "Color").queue_free()
			if $Selector/Colors/ScrollContainer/VBoxContainer/GridContainer.get_node(c + "Label") != null:
				$Selector/Colors/ScrollContainer/VBoxContainer/GridContainer.get_node(c + "Label").queue_free()

	# redo accessibility stuff
	var erase_queue = []
	for c in items:
		if c.is_queued_for_deletion():
			erase_queue.append(c)
		else:
			c.focus_neighbour_top = ""
			c.focus_neighbour_bottom = ""
			c.focus_next = ""
			c.focus_previous = ""
	for c in erase_queue:
		items.erase(c)
	setNeighbors()
	
	yield(get_tree(), "idle_frame")
	scroll1_space = 192
	scroll2_space = 192
	
func loadColor():
	var container = $Selector/Colors/ScrollContainer/VBoxContainer/GridContainer.get_children()
	for c in container:
		if c is CenterContainer:
			setColor(c.name.replace("Color",""))
	# Do the special cases
	doBgColor()
	doSkinColor()
	
func loadSprite():
	var container = $Selector/Features/ScrollContainer/VBoxContainer/GridContainer.get_children()
	for c in container:
		if c is TextureRect:
			setSprite(c.name.replace("Sprite",""))
	# Do the special cases
	doEyes()
	doSnout()
	
func setColor(property):
	if property != "background":
		var sprite = $Selector/Colors/ScrollContainer/VBoxContainer/GridContainer.get_node(property + "Color")
		if is_instance_valid(sprite):
			if portrait.get_node(property) != null:
				sprite.get_node("color").color = portrait.get_node(property).modulate
			
func setSprite(property):
	var sprite = $Selector/Features/ScrollContainer/VBoxContainer/GridContainer.get_node(property + "Sprite")
	if sprite != null:
		sprite.texture = portrait.get_node(property).texture

func doBgColor():
	var bg = $Selector/Colors/ScrollContainer/VBoxContainer/GridContainer/backgroundColor
	if bg != null:
		bg.get_node("color").color = portrait.get_node("bgColor").color

func doSkinColor():
	var bg = $Selector/Colors/ScrollContainer/VBoxContainer/GridContainer/skinColor
	if bg != null:
		bg.get_node("color").color = portrait.get_node("body").modulate


func doEyes():
	var eyes = $Selector/Features/ScrollContainer/VBoxContainer/GridContainer/eyesContainer
	eyes.get_node("eyes_backSprite").texture = portrait.get_node("eyes_back").texture
	eyes.get_node("eyesSprite").texture = portrait.get_node("eyes").texture

func doSnout():
	var eyes = $Selector/Features/ScrollContainer/VBoxContainer/GridContainer/snoutContainer
	if is_instance_valid($Selector/Features/ScrollContainer/VBoxContainer/GridContainer/snoutContainer):
		eyes.get_node("snoutSprite").texture = portrait.get_node("snout").texture
		eyes.get_node("snout2Sprite").texture = portrait.get_node("snout2").texture

func focused():
	.focused()
	if $Selector.current_tab == 0:
		selectedIndex1 = selected_index
	else:
		selectedIndex2 = selected_index

func _input(event):
	if !disabled:
		if Input.is_action_just_pressed("ui_up"):
			if $Selector.current_tab == 0:
				selectedIndex1-=1
				if selectedIndex1 < 0:
					selectedIndex1 = items.size()-1
				# Selected item is above current pos
				if selectedIndex1 == items.size()-1:
					_v_scroll1.value = _v_scroll1.max_value
				else: 
					_v_scroll1.value = (selectedIndex1-2) * scroll1_space
			else:
				selectedIndex2-=1
				if selectedIndex2 < 0:
					selectedIndex2 = items.size()-1
				# Selected item is above current pos
				if selectedIndex2 == items.size()-1:
					_v_scroll2.value = _v_scroll2.max_value
				else: 
					_v_scroll2.value = (selectedIndex2-2) * scroll2_space
		elif Input.is_action_just_pressed("ui_down"):
			if $Selector.current_tab == 0:
				selectedIndex1+=1
				# Selected item is above current pos
				if selectedIndex1 > items.size()-1:
					selectedIndex1 = 0
				# Selected item is above current pos
				if selectedIndex1 == 0:
					_v_scroll1.value = 0
				else: 
					_v_scroll1.value = (selectedIndex1-2) * scroll1_space
			else:
				selectedIndex2+=1
				# Selected item is above current pos
				if selectedIndex2 > items.size()-1:
					selectedIndex2 = 0
				# Selected item is above current pos
				if selectedIndex2 == 0:
					_v_scroll2.value = 0
				else: 
					_v_scroll2.value = (selectedIndex2-2) * scroll2_space

		if event is InputEventKey:
			if Input.is_action_just_pressed("ui_left"):
				Global.playSoundFx("ui/menu_left")
			if Input.is_action_just_pressed("ui_right"):
				Global.playSoundFx("ui/menu_right")
			if Input.is_action_just_pressed("ui_up"):
				Global.playSoundFx("ui/menu_up")
			if Input.is_action_just_pressed("ui_down"):
				Global.playSoundFx("ui/menu_down")
		if Input.is_action_just_pressed("ui_accept") || Input.is_action_just_pressed("ui_select"):
			Global.playSoundFx("ui/menu_open")
			

func openColorMenu(property):
	var instance = create_submenu(colorMenu)
	instance.get_node("Panel").parent = portrait
	instance.get_node("Panel").color = $Selector/Colors/ScrollContainer/VBoxContainer/GridContainer/.get_node(property+"Color/color").color
	instance.get_node("Panel").property = property
	instance.get_node("Panel").init()

func openFeatureMenu(property):
	var instance = create_submenu(featureMenu)
	instance.portrait = portrait
	instance.property = property
	instance.initer()

func _on_backgroundLabel_pressed():
	var instance = create_submenu(colorMenu)
	instance.get_node("Panel").parent = portrait
	instance.get_node("Panel").color = $Selector/Colors/ScrollContainer/VBoxContainer/GridContainer/backgroundColor/color.color
	instance.get_node("Panel").property = "bgColor"
	instance.get_node("Panel").init()


func _on_back_hairLabel_pressed():
	openColorMenu("back_hair")


func _on_skinLabel_pressed():
	var instance = create_submenu(colorMenu)
	instance.get_node("Panel").parent = portrait
	instance.get_node("Panel").color = $Selector/Colors/ScrollContainer/VBoxContainer/GridContainer/skinColor/color.color
	instance.get_node("Panel").property = "body"
	instance.get_node("Panel").init()


func _on_shirtLabel_pressed():
	openColorMenu("shirt")


func _on_necklaceLabel_pressed():
	openColorMenu("necklace")


func _on_mouthLabel_pressed():
	openColorMenu("mouth")


func _on_eyes_backLabel_pressed():
	openColorMenu("eyes_back")


func _on_eyesLabel_pressed():
	openColorMenu("eyes")


func _on_eyebrowsLabel_pressed():
	openColorMenu("eyebrows")


func _on_snout2Label_pressed():
	openColorMenu("snout2")

func _on_glassesLabel_pressed():
	openColorMenu("glasses")

func _on_earringLabel_pressed():
	openColorMenu("earring")

func _on_hatLabel_pressed():
	openColorMenu("hat")


func _on_backgroundSpriteLabel_pressed():
	openFeatureMenu("background")


func _on_back_hairSpriteLabel_pressed():
	openFeatureMenu("back_hair")


func _on_bodySpriteLabel_pressed():
	openFeatureMenu("body")


func _on_neckLabel_pressed():
	openFeatureMenu("neck")


func _on_shirtSpriteLabel_pressed():
	openFeatureMenu("shirt")


func _on_necklaceSpriteLabel_pressed():
	openFeatureMenu("necklace")


func _on_faceSpriteLabel_pressed():
	openFeatureMenu("face")


func _on_noseSpriteLabel_pressed():
	openFeatureMenu("nose")


func _on_mouthSpriteLabel_pressed():
	openFeatureMenu("mouth")


func _on_eyebrowsSpriteLabel_pressed():
	openFeatureMenu("eyebrows")


func _on_glassesSpriteLabel_pressed():
	openFeatureMenu("glasses")


func _on_earSpriteLabel_pressed():
	openFeatureMenu("ear")


func _on_earringSpriteLabel_pressed():
	openFeatureMenu("earring")


func _on_front_hairSpriteLabel_pressed():
	openFeatureMenu("front_hair")


func _on_hatSpriteLabel_pressed():
	openFeatureMenu("hat")


func _on_eyesSpriteLabel_pressed():
	var instance = create_submenu(featureMenu)
	instance.portrait = portrait
	instance.property = "eyes_back"
	instance.property2 = "eyes"
	instance.initer()


func _on_snoutSpriteLabel_pressed():
	var instance = create_submenu(featureMenu)
	instance.portrait = portrait
	instance.property = "snout"
	instance.property2 = "snout2"
	instance.initer()


func _on_Button_pressed():
	get_parent().get_parent().complete()
