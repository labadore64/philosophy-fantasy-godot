extends Control

var selected = false
var items = []


# Called when the node enters the scene tree for the first time.
func _ready():
	$"Greek Deck".populate("Greek")
	$"Psych Deck".populate("Greek")
	$"Marx Deck".populate("Greek")
	items.append_array([
		$"Greek Deck",
		$"Psych Deck",
		$"Marx Deck"
	])
	$"Greek Deck".grabFocus()
	$AnimationPlayer.play("enter")
		
func doneDeck():
	$"Greek Deck".complete = true
	$"Psych Deck".complete = true
	$"Marx Deck".complete = true
	for c in items:
		if !c.selected:
			c.dismiss()
		else:
			SaveFile.addDeck(c.cards)
	$AnimationPlayer.play("bye")
	Global.tts_stop()

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "bye":
		Global.doCutscene("Tutorial")
