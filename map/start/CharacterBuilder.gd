extends Node2D

onready var mainMenu = preload("res://map/menu/MainMenu.tscn")
var main_menu = false
# Called when the node enters the scene tree for the first time.
func _ready():
	$Container/Name.text = SaveFile.player_name
	$AnimationPlayer.play("FadeIn")

func complete():
	$AnimationPlayer.play("FadeOut")
	


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "FadeOut":
		queue_free()
		if main_menu:
			var instancez = mainMenu.instance()
			var lel = get_tree().get_root().get_node("Main")
			lel.call_deferred("add_child",instancez)
			queue_free()
		else:
			Global.call_deferred("doCutscene","StartGame")

func _input(event):
	if main_menu:
		if Input.is_action_just_pressed("ui_cancel"):
			complete()
