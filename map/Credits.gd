extends Control

var base_parent = null
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var line = 0

var loaded = false
var destroyed = false

var _v_scroll = null
const VSCROLLAMOUNT = 60

# Called when the node enters the scene tree for the first time.
func _ready():
	_v_scroll = $RichTextLabel.get_v_scroll()
	$AnimationPlayer.play("Load")
	Global.tts_say($RichTextLabel.bbcode_text.replace("[i]","").replace("[/i]","").replace("[b]","").replace("[/b]",""))

func _exit_tree():
	Global.tts_stop()

func _input(event):
	if !destroyed && loaded:
		if event is InputEventMouseButton:
			if event.is_pressed():
				if event.button_index == BUTTON_WHEEL_UP:
					_v_scroll.value -= VSCROLLAMOUNT
				# zoom out
				if event.button_index == BUTTON_WHEEL_DOWN:
					_v_scroll.value += VSCROLLAMOUNT
		if Input.is_action_just_pressed("ui_select") || Input.is_action_just_pressed("ui_cancel") || Input.is_action_just_pressed("ui_accept"):
			$AnimationPlayer.play("Unload")
			destroyed = true
			accept_event()
			Global.playSoundFx("ui/menu_cancel")
			if get_parent().has_method("fadeIn"):
				get_parent().fadeIn()
		if Input.is_action_just_pressed("ui_up"):
			line-=1
			if line < 0:
				line = 0
			$RichTextLabel.scroll_to_line(line)
			accept_event()
		elif Input.is_action_just_pressed("ui_down"):
			line+=1
			if line >= $RichTextLabel.get_line_count():
				line = $RichTextLabel.get_line_count()-1
			$RichTextLabel.scroll_to_line(line)
			accept_event()

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Unload":
		queue_free()
	elif anim_name == "Load":
		loaded = true
