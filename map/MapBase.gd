extends Node

onready var mainMenu = preload("res://map/menu/MainMenu.tscn")

var ambience = null
var music = null
var map_name = null

signal music_play()

signal playSound1()
signal playSound2()
signal playSound3()

var canClickCredits = true
var startNewGame = false;
var continuez = false

func _ready():
	if !Global.introAnimationPlayed:
		$AnimationPlayer.play("Load")
		Global.introAnimationPlayed = true
	else:
		$AnimationPlayer.play("Idle")
		$Control2/GridContainer/NewGame.mouse_filter = Control.MOUSE_FILTER_STOP
		$Control2/GridContainer/Continue.mouse_filter = Control.MOUSE_FILTER_STOP
		$Control2/GridContainer/Options.mouse_filter = Control.MOUSE_FILTER_STOP
		$Control2/GridContainer/Credits.mouse_filter = Control.MOUSE_FILTER_STOP
		Global.playSong("music_title")


func playMusic():
	Global.playSong("music_title")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Load":
		$Control2.get_node("GridContainer/NewGame").mouse_filter = Control.MOUSE_FILTER_STOP
		$Control2.get_node("GridContainer/NewGame").grab_focus()
		if $Control2.get_node("GridContainer/Continue") != null:
			$Control2.get_node("GridContainer/Continue").mouse_filter = Control.MOUSE_FILTER_STOP
		$Control2.get_node("GridContainer/Options").mouse_filter = Control.MOUSE_FILTER_STOP
		$Control2.get_node("GridContainer/Credits").mouse_filter = Control.MOUSE_FILTER_STOP
		$ColorRect.queue_free()
	if anim_name == "FadeKill":
		queue_free()

func _on_Node2D_music_play():
	playMusic()

func fadeIn():
	canClickCredits = true
	$AnimationPlayer.play("FadeIn")
	
func fadeOut():
	canClickCredits = false
	$AnimationPlayer.play("FadeOut")
	
func fadeKill():
	startNewGame = true
	$AnimationPlayer.play("FadeKill")

func _exit_tree():
	if startNewGame:
		if continuez:
			var instancez = mainMenu.instance()
			var lel = get_tree().get_root().get_node("Main")
			lel.call_deferred("add_child",instancez)
		else:
			var instance = load("res://map/start/NameInput.tscn").instance()
			get_parent().call_deferred("add_child",instance)

func _on_playSound1():
	$Sound1.play()


func _on_playSound2():
	$Sound2.play()


func _on_playSound3():
	$Sound3.play()

func regrab2():
	$Control2/GridContainer/NewGame.grab_focus()
