extends Control

var selected = false
var parent

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.playSoundFx("ui/menu_alert")
	$AnimationPlayer.play("Load")
	$Control/GridContainer/EndTurn.grab_focus()

func _on_EndTurn_pressed():
	if !selected:
		selected = true
		$AnimationPlayer.play("Unload")
		Global.playSoundFx("ui/menu_cancel")
		get_parent().regrab2()

func _on_NextPhase_pressed():
	if !selected:
		selected = true
		if !get_parent().startNewGame:
			get_parent().fadeKill()
			$AnimationPlayer.play("Unload")
		

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Unload":
		queue_free()
		
func _input(event):
	if event is InputEventKey:
		if Input.is_action_just_pressed("ui_up") || Input.is_action_just_pressed("ui_left"):
			Global.playSoundFx("ui/menu_up")
		if Input.is_action_just_pressed("ui_down") || Input.is_action_just_pressed("ui_right"):
			Global.playSoundFx("ui/menu_down")
	if Input.is_action_just_pressed("ui_accept") || Input.is_action_just_pressed("ui_select"):
		Global.playSoundFx("ui/menu_open")
