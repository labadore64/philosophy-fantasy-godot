extends Control

var node
var last_node
var initial_focus = false
var initialized = false
var base_parent = null
var disabled = false;
var text_read = ""

var name_read = false

var position_in_children = 0
onready var buttons = []
onready var ranges = []
var items
var items_position = 0;

signal RestoreControl()

var column_in_row = 0

func add_text(text, nope=true):
	text_read += " " + text
	
func read_text():
	Global.tts_stop()
	Global.tts_say(text_read)
	text_read = ""

func get_siblings():
	var parent = node.get_parent()
	if parent:
		return parent.get_children()
	return null

func click(item := node, button_index = BUTTON_LEFT):
	print_debug("Click")
	var click = InputEventMouseButton.new()
	click.button_index = button_index
	click.pressed = true
	if item is Node:
		click.position = item.rect_global_position
	else:
		click.position = node.get_tree().root.get_mouse_position()
	node.get_tree().input_event(click)
	click.pressed = false
	node.get_tree().input_event(click)

func _get_nearby_label(n):
	if(!(n.get_parent() is ScrollBar)):
		var sib = n.get_parent().get_children()
		
		for c in range(0,sib.size()):
			if sib[c] == n:
				if c > 0:
					if sib[c-1] is Label:
						return sib[c-1];
	return null

func _guess_label():
	if node is Label:
		return
	if not node is LineEdit and not node is TextEdit and node.get("text"):
		return
	var tokens = PoolStringArray([])
	var to_check = node
	while to_check:
		if to_check.is_class("AcceptDialog"):
			return
		if to_check.is_class("EditorProperty") and to_check.label:
			tokens.append(to_check.label)
		if (
			(to_check.is_class("EditorProperty") or to_check.is_class("EditorInspectorCategory"))
			and to_check.get_tooltip_text()
		):
			tokens.append(to_check.get_tooltip_text())
		var label = tokens.join(": ")
		if label:
			return label
			
		# if node parent is a grid container, find the label next to the index
		# of the object
		if (to_check.is_class("GridContainer") && node.get_parent() == to_check):
			var node_index = node.get_index()
			
			if(node_index > 0):
				var childnode = to_check.get_child(node_index-1)
				if(childnode is Label):
					return childnode
					
			# if no label before, check if there's one after just in case
			if(node_index < to_check.get_child_count()-1):
				var childnode = to_check.get_child(node_index+1)
				if(childnode is Label):
					return childnode
					
			# if both checks fail do the default check
		for child in to_check.get_children():
			if child is Label:
				return child
		to_check = to_check.get_parent()

func _accept_dialog_speak():
	var text
	if node.dialog_text != "":
		text = node.dialog_text
	else:
		for c in node.get_children():
			if c is Label:
				text = c.text
	if text:
		add_text("Dialog: %s" % text, false)
	else:
		add_text("Dialog", false)
	#read_text()

func _accept_dialog_focused():
	_accept_dialog_speak()
	if node.get_parent() and node.get_parent().is_class("ProjectSettingsEditor"):
		yield(node.get_tree().create_timer(5), "timeout")
		node.get_ok().emit_signal("pressed")
	

func _accept_dialog_about_to_show():
	_accept_dialog_speak()
	#ScreenReader.should_stop_on_focus = false


func _basebutton_button_down():
	#read_text()
	pass


func checkbox_focused():
	var tokens = PoolStringArray([])
	if node.text:
		tokens.append(node.text)
	if node.pressed:
		tokens.append("checked")
	else:
		tokens.append("unchecked")
	tokens.append(" checkbox")
	add_text(tokens.join(" "), false)
	#read_text()


func _checkbox_or_checkbutton_toggled(checked):
	if node.has_focus():
		if checked:
			add_text("checked", true)
			Global.playSoundFx("ui/menu_change")
		else:
			add_text("unchecked", true)
			Global.playSoundFx("ui/close_dialog")
		read_text()


func _checkbutton_focused():
	var tokens = PoolStringArray([])
	if node.text:
		tokens.append(node.text)
	if node.pressed:
		tokens.append("checked")
	else:
		tokens.append("unchecked")
	tokens.append(" check button")
	add_text(tokens.join(" "), false)
	#read_text()


var spoke_hint_tooltip


func _button_focused():
	button_text(node)
	
func button_text(noder):
	var label = _get_nearby_label(noder)
	
	var tokens = PoolStringArray([])
		
	if noder.text:
		tokens.append(noder.text)
	if noder.hint_tooltip:
		spoke_hint_tooltip = true
		tokens.append(noder.hint_tooltip)
	else:
		tokens.append(_get_graphical_button_text(noder.icon))
		
	if is_instance_valid(label):
		tokens.append(label.text)
		
	tokens.append("button")
	if noder.disabled:
		tokens.append("disabled")
	add_text(tokens.join(": "), false)
	read_text()

func try_to_get_text_in_theme(theme, texture):
	if theme == null:
		return ""

	for type in theme.get_type_list(""):
		for icon in theme.get_icon_list(type):
			var icon_texture = theme.get_icon(icon, type)
			if icon_texture == texture:
				return icon

	return ""


func _get_graphical_button_text(texture):
	var default_theme_copy = Theme.new()
	default_theme_copy.copy_default_theme()
	var current = node
	while current != null:
		var text = try_to_get_text_in_theme(current.theme, texture)
		if text != "":
			return text
		current = current.get_parent_control()
	return try_to_get_text_in_theme(default_theme_copy, texture)


func _texturebutton_focused():
	var tokens = PoolStringArray([])
	tokens.append(_get_graphical_button_text(node.texture_normal))
	tokens.append("button")
	add_text(tokens.join(": "))


func item_list_item_focused(idx):
	var tokens = PoolStringArray([])
	var text = node.get_item_text(idx)
	if text:
		tokens.append(text)
	text = node.get_item_tooltip(idx)
	if text:
		tokens.append(text)
	tokens.append("%s of %s" % [idx + 1, node.get_item_count()])
	add_text(tokens.join(": "))
	#read_text()

func item_list_focused():
	var count = node.get_item_count()
	var selected = node.get_selected_items()
	print_debug(selected)
	if len(selected) == 0:
		if node.get_item_count() == 0:
			return add_text("list, 0 items", false)
		selected = 0
		node.select(selected)
		node.emit_signal("item_selected", selected)
	else:
		selected = selected[0]
	position_in_children = selected
	item_list_item_focused(selected)
	#read_text()

func item_list_item_selected(index):
	item_list_item_focused(index)


func item_list_multi_selected(index, selected):
	add_text("Multiselect", false)
	#read_text()


func item_list_nothing_selected():
	add_text("Nothing selected", false)
	#read_text()


func item_list_input(event):
	if event.is_action_pressed("ui_right") or event.is_action_pressed("ui_left"):
		return node.accept_event()
	var old_count = node.get_item_count()
	var old_pos = position_in_children
	if event.is_action_pressed("ui_up"):
		node.accept_event()
		if position_in_children == 0:
			return
		position_in_children -= 1
	elif event.is_action_pressed("ui_down"):
		node.accept_event()
		if position_in_children >= node.get_item_count() - 1:
			return
		position_in_children += 1
	elif event.is_action_pressed("ui_home"):
		node.accept_event()
		position_in_children = 0
	elif event.is_action_pressed("ui_end"):
		node.accept_event()
		position_in_children = node.get_item_count() - 1
	if old_pos != position_in_children:
		if position_in_children >= node.get_item_count():
			position_in_children = 0
		node.unselect_all()
		node.select(position_in_children)
		node.emit_signal("item_list_item_selected", position_in_children)
		item_list_item_focused(position_in_children)


func _label_focused():
	var tokens = PoolStringArray([])
	if node.get_parent() is WindowDialog:
		tokens.append("Dialog")
	var text = node.text
	if text == "":
		text = "blank"
	tokens.append(text)
	add_text(tokens.join(": "), false)
	#read_text()


func line_edit_focused():
	var text = "blank"
	if node.secret:
		text = "password"
	elif node.text != "":
		text = node.text
	elif node.placeholder_text != "":
		text = node.placeholder_text
	var type = "editable text"
	if not node.editable:
		type = "text"
	add_text("%s: %s" % [text, type], false)
	#read_text()


var old_text = ""

var old_pos


func line_edit_text_changed(text):
	if text == null or old_text == null:
		return
	if len(text) > len(old_text):
		for i in range(len(text)):
			if text.substr(i, 1) != old_text.substr(i, 1):
				add_text(text.substr(i, 1))
				return
	else:
		for i in range(len(old_text)):
			if old_text.substr(i, 1) != text.substr(i, 1):
				add_text(old_text.substr(i, 1))
				return


func line_edit_input(event):
	var pos = node.caret_position
	if old_pos != null and old_pos != pos:
		var text = node.text
		if old_text == text:
			if pos > len(text) - 1:
				add_text("blank", true)
			else:
				add_text(text[pos], true)
		old_pos = pos
	elif old_pos == null:
		old_pos = pos
	old_text = node.text
	#read_text()


func menu_button_focused():
	var tokens = PoolStringArray([])
	if node.text:
		tokens.append(node.text)
	if node.hint_tooltip:
		tokens.append(node.hint_tooltip)
		spoke_hint_tooltip = true
	tokens.append("menu")
	add_text(tokens.join(": "), false)
	#read_text()


func popup_menu_focused():
	add_text("menu", false)
	#read_text()

func popup_menu_item_id_focused(index):
	print_debug("item id focus %s" % index)
	var tokens = PoolStringArray([])
	var shortcut = node.get_item_shortcut(index)
	var name
	if shortcut:
		name = shortcut.resource_name
		if name:
			tokens.append(name)
		var text = shortcut.get_as_text()
		if text != "None":
			tokens.append(text)
	var item = node.get_item_text(index)
	if item and item != name:
		tokens.append(item)
	var submenu = node.get_item_submenu(index)
	if submenu:
		tokens.append(submenu)
		tokens.append("menu")
	if node.is_item_checkable(index):
		if node.is_item_checked(index):
			tokens.append("checked")
		else:
			tokens.append("unchecked")
	var tooltip = node.get_item_tooltip(index)
	if tooltip:
		tokens.append(tooltip)
	var disabled = node.is_item_disabled(index)
	if disabled:
		tokens.append("disabled")
	tokens.append(str(index + 1) + " of " + str(node.get_item_count()))
	add_text(tokens.join(": "), true)
	read_text()

func popup_menu_item_id_pressed(index):
	if node.is_item_checkable(index):
		if node.is_item_checked(index):
			add_text("checked", true)
		else:
			add_text("unchecked", true)
	read_text()

func range_focused():
	range_text(node)
	
func range_text(nodez):
	if !(nodez is ScrollBar):
		var tokens = PoolStringArray([])
		tokens.append(str(nodez.value))
		var guess = _get_nearby_label(nodez)
		if(guess != null):
			tokens.append(str(guess.text))
		if nodez is HSlider:
			tokens.append("horizontal slider")
		elif nodez is VSlider:
			tokens.append("vertical slider")
		elif nodez is SpinBox:
			tokens.append("spin box")
		else:
			tokens.append("range")
		tokens.append("minimum %s" % nodez.min_value)
		tokens.append("maximum %s" % nodez.max_value)
		if OS.has_touchscreen_ui_hint():
			tokens.append("Swipe up and down to change.")
		add_text(tokens.join(": "), false)
	#read_text()

func range_value_changed(value):
	if node.has_focus():
		if !(node is ScrollBar):
			add_text("%s" % value, true)
			read_text()

func text_edit_focus():
	var tokens = PoolStringArray([])
	if node.text:
		tokens.append(node.text)
	else:
		tokens.append("blank")
	if node.readonly:
		tokens.append("read-only edit text")
	else:
		tokens.append("edit text")
	add_text(tokens.join(": "), false)
	read_text()

func text_edit_input(event):
	pass

func scrollContainer_focus():
	if !node.is_a_parent_of(last_node):
		var children = node.get_children()
		for c in children:
			if !c.is_class("ScrollBar"):
				var childr = c.get_children()
				for cc in childr:
					if cc.focus_mode != FOCUS_NONE:
						cc.grab_focus()
						break;

		
func scrollBoxContainer_focus():
	if !node.is_a_parent_of(last_node):
		var children = node.get_children()
		for c in children:
			if c.focus_mode != FOCUS_NONE:
				c.grab_focus()
				break;
				
		
func gridContainer_focus():
	if !node.is_a_parent_of(last_node):
		var children = node.get_children()
		for c in children:
			if c.focus_mode != FOCUS_NONE:
				c.grab_focus()
				break;
	

func scrollContainer_input(event):
	pass

var _last_tree_item_tokens

var button_index


func _tree_item_render():
	if not node.has_focus():
		return
	var cell = node.get_selected()
	var tokens = PoolStringArray([])
	for i in range(node.columns):
		if node.select_mode == Tree.SELECT_MULTI or cell.is_selected(i):
			var title = node.get_column_title(i)
			if title:
				tokens.append(title)
			var column_text = cell.get_text(i)
			if column_text:
				tokens.append(column_text)
			if cell.get_children():
				if cell.collapsed:
					tokens.append("collapsed")
				else:
					tokens.append("expanded")
			var button_count = cell.get_button_count(i)
			if button_count != 0:
				var column
				for j in range(node.columns):
					if cell.is_selected(j):
						column = j
						break
				if column == i:
					button_index = 0
				else:
					button_index = null
				tokens.append(
					(
						str(button_count)
						+ " "
						+ TTS.singular_or_plural(button_count, "button", "buttons")
					)
				)
				if button_index != null:
					var button_tooltip = cell.get_button_tooltip(i, button_index)
					if button_tooltip:
						tokens.append(button_tooltip)
						tokens.append("button")
					if button_count > 1:
						tokens.append("Use Home and End to switch focus.")
	tokens.append("tree item")
	if tokens != _last_tree_item_tokens:
		add_text(tokens.join(": "), true)
	_last_tree_item_tokens = tokens
	read_text()

var prev_selected_cell


func _tree_item_or_cell_selected():
	button_index = null
	_tree_item_render()


func tree_item_multi_selected(item, column, selected):
	if selected:
		add_text("selected", true)
	else:
		add_text("unselected", true)
	read_text()

func _tree_input(event):
	if event.is_action_pressed("ui_up") or event.is_action_pressed("ui_down"):
		node.accept_event()
	var item = node.get_selected()
	var column
	if item:
		for i in range(node.columns):
			if item.is_selected(i):
				column = i
				break
	if item and event is InputEventKey and event.pressed and not event.echo:
		var area
		if column:
			area = node.get_item_area_rect(item, column)
		else:
			area = node.get_item_area_rect(item)
		var position = Vector2(
			node.rect_global_position.x + area.position.x + area.size.x / 2,
			node.rect_global_position.y + area.position.y + area.size.y / 2
		)
		node.get_tree().root.warp_mouse(position)
	if item and column != null and item.get_button_count(column):
		if Input.is_action_just_pressed("ui_accept"):
			node.accept_event()
			return node.emit_signal("button_pressed", item, column, button_index + 1)
		var new_button_index = button_index
		if event.is_action_pressed("ui_home"):
			node.accept_event()
			new_button_index += 1
			if new_button_index >= item.get_button_count(column):
				new_button_index = 0
		elif event.is_action_pressed("ui_end"):
			node.accept_event()
			new_button_index -= 1
			if new_button_index < 0:
				new_button_index = item.get_button_count(column) - 1
		if new_button_index != button_index and item.has_method("get_button_tooltip"):
			button_index = new_button_index
			var tokens = PoolStringArray([])
			var tooltip = item.get_button_tooltip(column, button_index)
			if tooltip:
				tokens.append(tooltip)
			tokens.append("button")
			add_text(tokens.join(": "), true)
	read_text()

func tree_focused():
	_last_tree_item_tokens = null
	if node.get_selected():
		_tree_item_render()
	else:
		add_text("tree", true)
	read_text()

var _was_collapsed


func _tree_item_collapsed(item):
	if node.has_focus():
		var selected = false
		for column in range(node.columns):
			if item.is_selected(column):
				selected = true
				break
		if selected and item.collapsed != _was_collapsed:
			if item.collapsed:
				add_text("collapsed", false)
			else:
				add_text("expanded", false)
	_was_collapsed = item.collapsed
	read_text()

func progress_bar_focused():
	var percentage = int(node.ratio * 100)
	add_text("%s percent" % percentage, false)
	add_text("progress bar", false)
	read_text()

var last_percentage_spoken

var last_percentage_spoken_at = 0


func progress_bar_value_changed(value):
	var percentage = node.value / (node.max_value - node.min_value) * 100
	percentage = int(percentage)
	if (
		percentage != last_percentage_spoken
		and OS.get_ticks_msec() - last_percentage_spoken_at >= 10000
	):
		add_text("%s percent" % percentage)
		last_percentage_spoken_at = OS.get_ticks_msec()
		last_percentage_spoken = percentage
	read_text()

func tab_container_focused():
	var text = node.get_tab_title(node.current_tab)
	text += ": tab: " + str(node.current_tab + 1) + " of " + str(node.get_tab_count())
	add_text(text, false)
	read_text()

func tab_container_tab_changed(tab):
	if node.has_focus():
		tab_container_focused()


func tab_container_input(event):
	# push the focus down to the content of the tab
	if event.is_action_pressed("ui_down"):
		node.accept_event()
		var child = node.get_tab_control(node.current_tab)
		if(is_instance_valid(child)):
			if(child is Panel):
				child = child.get_child(0)
			if(is_instance_valid(child)):
				child.grab_focus()
				focused()
		return
	
	var new_tab = node.current_tab
	if event.is_action_pressed("ui_right"):
		node.accept_event()
		new_tab += 1
	elif event.is_action_pressed("ui_left"):
		node.accept_event()
		new_tab -= 1
	if new_tab < 0:
		new_tab = node.get_tab_count() - 1
	elif new_tab >= node.get_tab_count():
		new_tab = 0
	if node.current_tab != new_tab:
		node.current_tab = new_tab

func focused():
	if !disabled:
		if(!name_read):
			add_text(name)
			name_read = true
		
		last_node = node;
		node = get_focus_owner()
		print_debug("Focus: %s" % node)
		
		if(node != null):
			var counter = 0;
			for c in items:
				if c == node:
					items_position = counter
					break;
				counter+=1
		#if ScreenReader.should_stop_on_focus:
		#	TTS.stop()
		#ScreenReader.should_stop_on_focus = true
		if not node is Label:
			var label = _guess_label()
			if label:
				if label is Label:
					label = label.text
				if label and label != "":
					add_text(label, false)
		if node is MenuButton:
			menu_button_focused()
		elif node is AcceptDialog:
			_accept_dialog_focused()
		elif node is CheckBox:
			checkbox_focused()
		elif node is CheckButton:
			_checkbutton_focused()
		elif node is Button:
			_button_focused()
		elif node.is_class("EditorInspectorSection"):
			editor_inspector_section_focused()
		elif node is ItemList:
			item_list_focused()
		elif node is Label or node is RichTextLabel:
			_label_focused()
		elif node is LineEdit:
			line_edit_focused()
		elif node is LinkButton:
			_button_focused()
		elif node is PopupMenu:
			popup_menu_focused()
		elif node is ProgressBar:
			progress_bar_focused()
		elif node is Range:
			range_focused()
		elif node is TabContainer:
			tab_container_focused()
		elif node is TextEdit:
			text_edit_focus()
		elif node is TextureButton:
			_texturebutton_focused()
		elif node is Tree:
			tree_focused()
		elif node is ScrollContainer:
			scrollContainer_focus()
		elif node is VBoxContainer:
			scrollBoxContainer_focus()
		elif node is GridContainer:
			gridContainer_focus()
		else:
			add_text(node.get_class(), true)
			read_text()
			print_debug("No handler")
		if node.hint_tooltip and not spoke_hint_tooltip:
			add_text(node.hint_tooltip, false)
			read_text()
		
		if(text_read != ""):
			read_text()
		spoke_hint_tooltip = false


func unfocused():
	if !disabled:
		position_in_children = 0


func click_focused():
	if !disabled:
		if(is_instance_valid(node)):
			if node.has_focus():
				return
			if node.focus_mode == Control.FOCUS_ALL:
				node.grab_focus()

func gui_input(event):
	if !disabled:

		if (
			event is InputEventKey
			and Input.is_action_just_pressed("ui_accept")
			and event.control
			and event.alt
		):
			add_text("click", false)
			click()
		elif event is InputEventKey and event.pressed and not event.echo and event.scancode == KEY_MENU:
			node.get_tree().root.warp_mouse(node.rect_global_position)
			return click(null, BUTTON_RIGHT)
		if node is TabContainer:
			return tab_container_input(event)
		elif node is ItemList:
			return item_list_input(event)
		elif node is LineEdit:
			return line_edit_input(event)
		elif node is TextEdit:
			return text_edit_input(event)
		elif node is ScrollContainer:
			return scrollContainer_input(event)
		elif node is Tree:
			return _tree_input(event)
		elif node.is_class("EditorInspectorSection"):
			return editor_inspector_section_input(event)
			
func _input(event):
	get_tree().get_root().get_node("Main")._gui_input(event)
	gui_input(event)
	
func _is_focusable(node):
	if !node.visible:
		return false
	if node.focus_mode == FOCUS_CLICK:
		return false
	if disabled:
		return false
	if node.is_class("SceneTreeEditor"):
		return false
	if node.is_class("MultiMeshEditor"):
		return false
	if node.is_class("MeshInstanceEditor"):
		return false
	if node.is_class("SpriteEditor"):
		return false
	if node.is_class("Skeleton2DEditor"):
		return false
	if node.is_class("CollisionShape2DEditor"):
		return false
	if node.is_class("ReferenceRect"):
		return false
	if node.is_class("ColorRect"):
		return false
	if node is Panel:
		return false
	if node is TabContainer:
		return true
	if node.is_class("EditorInspectorSection"):
		return true
	if node is AcceptDialog:
		return true
	if node is ScrollContainer:
		return true
	if node is VBoxContainer:
		return true
	if node is GridContainer:
		return true
	if node is Label and node.get_parent() and node.get_parent() is AcceptDialog:
		return false
	if node is Label and node.focus_mode == FOCUS_NONE:
		return false
	if (
		node is Container
		or node is Separator
		or node is ScrollBar
		or node is Popup
		or node.get_class() == "Control"
	):
		return false
	return true


func editor_inspector_section_focused():
	var child = node.get_children()[0]
	var tokens = PoolStringArray(["editor inspector section"])
	if child is CanvasItem or child is Spatial:
		var expanded = child.is_visible_in_tree()
		if expanded:
			tokens.append("expanded")
		else:
			tokens.append("collapsed")
	add_text(tokens.join(": "), false)
	read_text()

func editor_inspector_section_input(event):
	if event.is_action_pressed("ui_accept"):
		click()
		var child = node.get_children()[0]
		var expanded = child.is_visible_in_tree()
		if expanded:
			add_text("expanded", true)
		else:
			add_text("collapsed", true)
	read_text()

func uninitialize(node):
	name_read = false
	disabled = true
	initial_focus = false
	if node.is_class("Control"):
		node.focus_mode = FOCUS_NONE
		if(node.is_in_group("accessible")):
			node.remove_from_group("accessible")
		var child = node.get_children()
		for c in child:
			uninitialize(c)

func mouseReadOff():
	var mousePos = get_viewport().get_mouse_position()
	for nodez in buttons:
		if nodez.get_global_rect().has_point(mousePos):
			button_text(nodez)

func mouseReadOffRange():
	var mousePos = get_viewport().get_mouse_position()
	for nodez in ranges:
		if nodez.get_global_rect().has_point(mousePos):
			range_text(nodez)


func initialize(node):
	if node.is_class("Control"):
		#name = "Accessible for " + node.name
		if node.is_in_group("accessible"):
			return
		if !node.visible:
			return
		node.add_to_group("accessible")
		#node.add_child(node)
		self.node = node
		if _is_focusable(node):
			node.set_focus_mode(Control.FOCUS_ALL)
		var label = _guess_label()
		if label and label is Label:
			label.set_focus_mode(Control.FOCUS_NONE)
		if node is Panel:
			node.set_focus_mode(Control.FOCUS_NONE)
		if(!node.is_connected("focus_entered", self, "focused")):
			node.connect("focus_entered", self, "focused")
		if(!node.is_connected("mouse_entered", self, "click_focused")):
			node.connect("mouse_entered", self, "click_focused")
		if(!node.is_connected("focus_exited", self, "unfocused")):
			node.connect("focus_exited", self, "unfocused")
		if(!node.is_connected("mouse_exited", self, "unfocused")):
			node.connect("mouse_exited", self, "unfocused")
		if(!node.is_connected("gui_input", self, "gui_input")):
			node.connect("gui_input", self, "gui_input")
		if node is Button:
			buttons.append(node);
			if(!node.is_connected("mouse_entered",self,"mouseReadOff")):
				node.connect("mouse_entered",self,"mouseReadOff")
		if node is Range:
			ranges.append(node);
			if(!node.is_connected("mouse_entered",self,"mouseReadOffRange")):
				node.connect("mouse_entered",self,"mouseReadOffRange")
		if node is BaseButton:
			if(!node.is_connected("button_down", self, "_basebutton_button_down")):
				node.connect("button_down", self, "_basebutton_button_down")
		if node is AcceptDialog:
			if(!node.is_connected("about_to_show", self, "_accept_dialog_about_to_show")):
				node.connect("about_to_show", self, "_accept_dialog_about_to_show")
		elif node is CheckBox or node is CheckButton:
			if(!node.is_connected("toggled", self, "_checkbox_or_checkbutton_toggled")):
				node.connect("toggled", self, "_checkbox_or_checkbutton_toggled")
		elif node is ItemList:
			if(!node.is_connected("item_selected", self, "item_list_item_selected")):
				node.connect("item_selected", self, "item_list_item_selected")
			if(!node.is_connected("multi_selected", self, "item_list_multi_selected")):
				node.connect("multi_selected", self, "item_list_multi_selected")
			if(!node.is_connected("nothing_selected", self, "item_list_nothing_selected")):
				node.connect("nothing_selected", self, "item_list_nothing_selected")
		elif node is LineEdit:
			if(!node.is_connected("text_changed", self, "line_edit_text_changed")):
				node.connect("text_changed", self, "line_edit_text_changed")
		elif node is PopupMenu:
			if(!node.is_connected("id_focused", self, "popup_menu_item_id_focused")):
				node.connect("id_focused", self, "popup_menu_item_id_focused")
			if(!node.is_connected("id_pressed", self, "popup_menu_item_id_pressed")):
				node.connect("id_pressed", self, "popup_menu_item_id_pressed")
		elif node is ProgressBar:
			if(!node.is_connected("value_changed", self, "progress_bar_value_changed")):
				node.connect("value_changed", self, "progress_bar_value_changed")
		elif node is Range:
			if(!node.is_connected("value_changed", self, "range_value_changed")):
				node.connect("value_changed", self, "range_value_changed")
		elif node is TabContainer:
			if(!node.is_connected("tab_changed", self, "tab_container_tab_changed")):
				node.connect("tab_changed", self, "tab_container_tab_changed")

		elif node is Tree:
			if(!node.is_connected("item_collapsed", self, "_tree_item_collapsed")):
				node.connect("item_collapsed", self, "_tree_item_collapsed")
			if(!node.is_connected("multi_selected", self, "tree_item_multi_selected")):
				node.connect("multi_selected", self, "tree_item_multi_selected")
			
			if node.select_mode == Tree.SELECT_MULTI:
				if(!node.is_connected("cell_selected", self, "_tree_item_or_cell_selected")):
					node.connect("cell_selected", self, "_tree_item_or_cell_selected")
			else:
				if(!node.is_connected("item_selected", self, "_tree_item_or_cell_selected")):
					node.connect("item_selected", self, "_tree_item_or_cell_selected")
		
		if node.focus_mode == Control.FOCUS_ALL:
			items.append(node)
				
		# initialize children
		if node is ScrollContainer || VBoxContainer || TabContainer || node is Panel:
			var child = node.get_children()
			for c in child:
				if c is Control:
					initialize(c)
		#node.connect("tree_exiting", self, "queue_free", [], Object.CONNECT_DEFERRED)

func _initializeFocus(obj):
	items[items_position].grab_focus()
	if obj is Control && !initial_focus && !obj.is_queued_for_deletion():
		if obj.focus_mode == FOCUS_ALL:
			#obj.grab_focus()
			initial_focus = true
			return
		var children = obj.get_children()
		for c in children:
			_initializeFocus(c)

func _enter_tree():
	items = []

func _ready():
	items = []
	var children = get_children()
	for c in children:
		if !c.is_queued_for_deletion():
			initialize(c)
	node = children[0]
	if !is_connected("RestoreControl",self,"_on_Control_RestoreControl"):
		connect("RestoreControl", self, "_on_Control_RestoreControl")
	Global.mouseConfigure(node)
	#_initializeFocus(node)

func _process(delta):
	var newnode = last_node
	
	if(!is_instance_valid(last_node)):
		newnode = node
	var noder = null
	if Input.is_action_just_pressed("access_tooltip"):
		Global.tts_say(node.hint_tooltip)
	if Input.is_action_just_pressed("ui_left"):
		if(newnode.focus_neighbour_left != ""):
			noder = newnode.get_node(newnode.focus_neighbour_left)
			if(is_instance_valid(noder)):
				noder.grab_focus()
		Global.playSoundFx("ui/menu_left")
		get_tree().set_input_as_handled()
	if Input.is_action_just_pressed("ui_right"):
		if(newnode.focus_neighbour_right != ""):
			noder = newnode.get_node(newnode.focus_neighbour_right)
			if(is_instance_valid(noder)):
				noder.grab_focus()
		Global.playSoundFx("ui/menu_right")
		get_tree().set_input_as_handled()
	if Input.is_action_just_pressed("ui_up"):
		if(newnode.focus_neighbour_top != ""):
			noder = newnode.get_node(newnode.focus_neighbour_top)
			if(is_instance_valid(noder)):
				noder.grab_focus()
		Global.playSoundFx("ui/menu_up")
		get_tree().set_input_as_handled()
	if Input.is_action_just_pressed("ui_down"):
		if(newnode.focus_neighbour_bottom != ""):
			noder = newnode.get_node(newnode.focus_neighbour_bottom)
			if(is_instance_valid(noder)):
				noder.grab_focus()
		Global.playSoundFx("ui/menu_down")
		get_tree().set_input_as_handled()
	if Input.is_action_just_pressed("ui_focus_next"):
		if(newnode.focus_next != ""):
			noder = newnode.get_node(newnode.focus_next)
			if(is_instance_valid(noder)):
				noder.grab_focus()
		Global.playSoundFx("ui/menu_tabnext")
		get_tree().set_input_as_handled()
	if Input.is_action_just_pressed("ui_focus_prev"):
		if(newnode.focus_previous != ""):
			noder = newnode.get_node(newnode.focus_previous)
			if(is_instance_valid(noder)):
				noder.grab_focus()
		Global.playSoundFx("ui/menu_tabprev")
		get_tree().set_input_as_handled()


func reready():
	items = []
	var oldnode = node
	disabled = false
	add_text(name)
	var children = get_children()
	for c in children:
		if !c.is_queued_for_deletion():
			initialize(c)
	_initializeFocus(oldnode)

func _on_Control_RestoreControl():
	reready()
