## NewAccess
## New screenreader script
## Based on the work of Nolan Darilek

extends Control

var helpclose = []			# List of help/close nodes.
var help_mode = false		# If in help mode, ignores hiding child nodes.

var items = [] 				# List of valid nodes
var selected_node			# Selected focused node
var selected_index = 0		# Selected focused node's index
var disabled = false		# Whether this control is disabled
var text_read = ""			# Text to be read

var old_text = ""			# Old line edit text
var old_pos					# Old line edit position

var base_parent = null		# represents the menu parent (the menu that
							# created this one)

var help_open = false		# if true, does not capture input cuz
							# child help exists

var position_in_children = 0 # Used to keep track of itemlist variable.

#### TTS Functions ####

# Adds text to the next reading.
func add_text(text, nope=true):
	text_read += " " + text
	
# Reads off the text
func read_text():
	if text_read != "":
		print(text_read)
		Global.tts_stop()
		Global.tts_say(text_read)
		text_read = ""

#### Node Functions ####

func grabLabel(c):
	
	# first locate c
	var counter = 0
	var children = c.get_parent().get_children();
	for cc in children:
		if cc == c:
			break;
		counter+=1
	
	# now locate the last applicable label
	var label = null
	if counter != 0:
		if children[counter-1] is Label || children[counter-1] is RichTextLabel:
			label = children[counter-1]
	return label;
	
func isLabel(c):
	return c is Label || c is RichTextLabel

# Populates items with a node's children
# only selecting nodes that are designed with accessibility.
func get_nodes(node):
	var children = node.get_children()
	for c in children:
		if !c.is_queued_for_deletion():
			# Makes sure the child is a control and is visible.
			if c is Control:
				if c.visible:
					# If its a container, search all children.
					if c is Container || c is Panel:
						configure_container(c)
						get_nodes(c)
					# If its any of these types, add it to the collection.
					elif (c is Button || isLabel(c) || c is LineEdit 
							|| (c is Range && !(c is ScrollBar)) || c is ItemList):
						# To isolate other components, focus mode all must be set
						# to all accessible compnents.
						if c.focus_mode == c.FOCUS_ALL:
							configureSignals(c)
							add_node(c)
							# sometimes SOME idiot like me likes to make these have
							# children. so gotta check those too!
							get_nodes(c)
					else:
						get_nodes(c)
		
# Adds a node to the node list
func add_node(node):
	items.append(node)
				
# disables all nodes
func disable():
	if !help_mode:
		for c in helpclose:
			if is_instance_valid(c):
				c.visible = false
	disabled = true
	for c in items:
		# Makes sure the child is a control and is visible.
		if is_instance_valid(c):
			if c is Control:
				if c.visible:
					c.focus_mode = FOCUS_CLICK

# enables all nodes
func enable():
	for c in helpclose:
		if is_instance_valid(c):
			c.visible = true
	
	disabled = false
	for c in items:
		# Makes sure the child is a control and is visible.
		if is_instance_valid(c):
			if c is Control:
				if c.visible:
					if c.focus_mode == FOCUS_CLICK:
						c.focus_mode = FOCUS_ALL
	
	

#### Node Neighbor Partners

# Sets the node neighbors for the list.
func setNeighbors():
	var current = null
	var next = null
	var previous = null
	var count = 0
	var size = items.size()
	for i in items:
		current = items[count]
		if items[(count + 1 + size) % size].is_inside_tree():
			next = items[(count + 1 + size) % size].get_path()
		if items[(count - 1 + size) % size].is_inside_tree():
			previous = items[(count - 1 + size) % size].get_path()
		
		setBaseNeighbors(current,next,previous)
		count+=1
			
# Create base node configuration
# Notice how it does not change the focus
# neighbours if it is already set
func setBaseNeighbors(node,next,previous):
	if next != null && previous != null:
		if node.focus_neighbour_top == "":
			node.focus_neighbour_top = previous
		if node.focus_neighbour_bottom == "":
			node.focus_neighbour_bottom = next
		if node.focus_next == "":
			node.focus_next = next
		if node.focus_previous == "":
			node.focus_previous = previous
		
func clearNeighbors():
	var current = null
	var next = null
	var previous = null
	var count = 0
	var size = items.size()
	for i in items:
		current = items[count]
		if is_instance_valid(items[(count + 1 + size) % size]):
			next = items[(count + 1 + size) % size].get_path()
		if is_instance_valid(items[(count - 1 + size) % size]):
			previous = items[(count - 1 + size) % size].get_path()
		
		clearBaseNeighbors(current,next,previous)
		count+=1
		
func clearBaseNeighbors(node,next,previous):
	if is_instance_valid(node):
		if node.focus_neighbour_top == previous:
			node.focus_neighbour_top = ""
		if node.focus_neighbour_bottom == next:
			node.focus_neighbour_bottom = ""
		if node.focus_next == next:
			node.focus_next = ""
		if node.focus_previous == previous:
			node.focus_previous = ""

#### Configuration functions ####

func configureSignals(node):
	# default configuration
	configureNode(node,"focus_entered", "focused")
	configureNode(node,"gui_input", "gui_input")
	if(!node.is_connected("mouse_entered",self,"mouseReadOff")):
		node.connect("mouse_entered",self,"mouseReadOff",[node])
	if node is LineEdit:
		configureNode(node,"text_changed","line_edit_text_changed")
	if node is Range:
		configureNode(node,"value_changed","range_value_changed")
	if node is TabContainer:
		configureNode(node,"tab_changed","tabContainer_changed")

func configureNode(node,string1,string2):
	if(!node.is_connected(string1,self,string2)):
		node.connect(string1,self,string2)
		
func configure_container(container):
	#if container is ScrollContainer:
		#container.follow_focus = true
	# tab container is unique and should be configured
	if container is TabContainer:
		configureSignals(container)
		add_node(container)
		
func mouseReadOff(node):
	node.grab_focus()
#### Focused Text Functions ####

func doFocus():
	if !help_open:
		if selected_node is Button:
			button_focused()
		elif isLabel(selected_node):
			label_focused()
		elif selected_node is LineEdit:
			lineEdit_focused()
		elif selected_node is ProgressBar:
			progressBar_focused()
		elif selected_node is Range:
			range_focused()
		elif selected_node is TabContainer:
			tabContainer_focused()
		elif selected_node is ItemList:
			itemList_focused()

func button_focused():
	
	if selected_node.text:
		add_text(selected_node.text)
	
	if !(selected_node is CheckBox || selected_node is CheckButton):
		var label = grabLabel(selected_node);
		if label != null:
			add_text(label.text)
	
	if Global.TTS_verbose:
		if selected_node.hint_tooltip:
			add_text(selected_node.hint_tooltip)
		add_text("button")
		if selected_node.toggle_mode:
			if selected_node.pressed:
				if !(selected_node is CheckBox || selected_node is CheckButton):
					add_text("Toggled")
				else:
					add_text("Checked")
			else:
				if !(selected_node is CheckBox || selected_node is CheckButton):
					add_text("Released")
				else:
					add_text("Unchecked")
		if selected_node.disabled:
			add_text("disabled")
	else:
		if selected_node.toggle_mode:
			if selected_node.pressed:
				if !(selected_node is CheckBox || selected_node is CheckButton):
					add_text("Toggled")
				else:
					add_text("Checked")
			else:
				if !(selected_node is CheckBox || selected_node is CheckButton):
					add_text("Released")
				else:
					add_text("Unchecked")
	read_text()
	
func label_focused():
	# skip labels if not in tts mode
	if !Global.TTS_enabled:
		if Input.is_action_just_pressed("ui_down"):
			selected_node = get_node(selected_node.focus_neighbour_bottom)
			selected_node.grab_focus()
			return
		elif Input.is_action_just_pressed("ui_up"):
			selected_node = get_node(selected_node.focus_neighbour_top)
			selected_node.grab_focus()
			return
		elif Input.is_action_just_pressed("ui_focus_next"):
			selected_node = get_node(selected_node.focus_next)
			selected_node.grab_focus()
			return
		elif Input.is_action_just_pressed("ui_focus_prev"):
			selected_node = get_node(selected_node.focus_previous)
			selected_node.grab_focus()
			return
	add_text(selected_node.text)
	if Global.TTS_verbose:
		add_text("Label")
	read_text()
	
func lineEdit_focused():
	if Global.TTS_enabled:
		Global.playSoundFx("access/access_textedit")
	add_text(selected_node.text)
	if Global.TTS_verbose:
		add_text("Line Edit")
	read_text()
	
func progressBar_focused():
	add_text(str(selected_node.value))
	
	var label = grabLabel(selected_node)
	if label != null:
		add_text(label.text)
		
	if Global.TTS_verbose:
		add_text("Out of %s" % selected_node.max_value)
		add_text("Progress Bar")
	read_text()
	
func range_focused():
	if !(selected_node is ScrollBar):
		add_text(str(selected_node.value))
		
		var label = grabLabel(selected_node)
		if label != null:
			add_text(label.text)	
				
		if Global.TTS_verbose:
			if selected_node is HSlider:
				add_text("horizontal slider")
			elif selected_node is VSlider:
				add_text("vertical slider")
			elif selected_node is SpinBox:
				add_text("spin box")
			else:
				add_text("range")
			add_text("minimum %s" % selected_node.min_value)
			add_text("maximum %s" % selected_node.max_value)
			if OS.has_touchscreen_ui_hint():
				add_text("Swipe up and down to change.")
		read_text()
		
func tabContainer_focused():
	if selected_node is TabContainer:
		var text = selected_node.get_tab_title(selected_node.current_tab)
		if Global.TTS_verbose:
			text += ": tab: " + str(selected_node.current_tab + 1) + " of " + str(selected_node.get_tab_count())
		add_text(text, false)
		read_text()
		
func tabContainer_changed(i):
	if !help_open:
		reload()
		tabContainer_focused()

func itemList_focused():
	var count = selected_node.get_item_count()
	var selected = selected_node.get_selected_items()

	if len(selected) == 0:
		if selected_node.get_item_count() == 0:
			return add_text("list, 0 items", false)
		selected = 0
		selected_node.select(selected)
		selected_node.emit_signal("item_selected", selected)
	else:
		selected = selected[0]
	position_in_children = selected
	#read_text()
	itemList_item_focused(selected)
	
#### Pressed Text Functions ####

func doPress():
	if !help_open:
		if selected_node is Button:
			button_pressed()

func button_pressed():
	if !selected_node.disabled:
		if selected_node.toggle_mode:
			if !selected_node.pressed:
				add_text("Toggled")
			else:
				add_text("Released")
		else:
			add_text("Pressed")
	button_focused()
	
#### Support Functions ####
	
func line_edit_text_changed(text):
	if text == null or old_text == null:
		return
	if len(text) > len(old_text):
		for i in range(len(text)):
			if text.substr(i, 1) != old_text.substr(i, 1):
				old_text = text
				add_text(text.substr(i, 1))
				read_text()
				return
	else:
		for i in range(len(old_text)):
			if old_text.substr(i, 1) != text.substr(i, 1):
				old_text = text
				add_text(old_text.substr(i, 1))
				read_text()
				return

func range_value_changed(value):
	if !(selected_node is ScrollBar || selected_node is ProgressBar):
		add_text("%s" % value, true)
		read_text()
		
func tabContainer_input(event):
	if !help_open:
		# push the focus down to the content of the tab
		if event.is_action_pressed("ui_down"):
			selected_node.accept_event()
			get_node(selected_node.focus_next).grab_focus()
			return
		var tab_changed = false
		var new_tab = selected_node.current_tab
		if event.is_action_pressed("ui_right"):
			selected_node.accept_event()
			new_tab += 1
			tab_changed = true
		elif event.is_action_pressed("ui_left"):
			selected_node.accept_event()
			new_tab -= 1
			tab_changed = true
		if new_tab < 0:
			new_tab = selected_node.get_tab_count() - 1
		elif new_tab >= selected_node.get_tab_count():
			new_tab = 0
		if selected_node.current_tab != new_tab:
			selected_node.current_tab = new_tab
		#if tab_changed:
			#reload()
			#tabContainer_focused()
		
func itemList_item_focused(idx):
	var text = selected_node.get_item_text(idx)
	if text:
		add_text(text)
	text = selected_node.get_item_tooltip(idx)
	if text:
		add_text(text)
	if Global.TTS_verbose:
		add_text("%s of %s" % [idx + 1, selected_node.get_item_count()])
	read_text()

func itemList_item_selected(index):
	itemList_item_focused(index)


func itemList_multi_selected(index, selected):
	add_text("Multiselect", false)
	#read_text()


func itemList_nothing_selected():
	add_text("Nothing selected", false)
	#read_text()
		
func itemList_input(event):
	if event.is_action_pressed("ui_right") or event.is_action_pressed("ui_left"):
		return selected_node.accept_event()
	var old_count = selected_node.get_item_count()
	var old_pos = position_in_children
	if event.is_action_pressed("ui_up"):
		selected_node.accept_event()
		if position_in_children == 0:
			get_node(selected_node.focus_neighbour_top).grab_focus()
		else:
			position_in_children -= 1
	elif event.is_action_pressed("ui_down"):
		selected_node.accept_event()
		if position_in_children >= selected_node.get_item_count() - 1:
			get_node(selected_node.focus_neighbour_bottom).grab_focus()
		else:
			position_in_children += 1
	elif event.is_action_pressed("ui_right"):
		selected_node.accept_event()
		if selected_node.focus_neighbour_right != "":
			get_node(selected_node.focus_neighbour_right).grab_focus()
	elif event.is_action_pressed("ui_left"):
		selected_node.accept_event()
		if selected_node.focus_neighbour_left != "":
			get_node(selected_node.focus_neighbour_left).grab_focus()
	elif event.is_action_pressed("ui_home"):
		selected_node.accept_event()
		position_in_children = 0
	elif event.is_action_pressed("ui_end"):
		selected_node.accept_event()
		position_in_children = selected_node.get_item_count() - 1
	if old_pos != position_in_children:
		if position_in_children >= selected_node.get_item_count():
			position_in_children = 0
		selected_node.unselect_all()
		selected_node.select(position_in_children)
		selected_node.emit_signal("itemList_item_selected", position_in_children)
		itemList_item_focused(position_in_children)
#### Signal Based Functions ####

# when the control regains control

# what happens on mouse input
func gui_input(event):

	if !help_open:
		# Unique movement routines
		if selected_node is TabContainer:
			return tabContainer_input(event)
		if selected_node is ItemList:
			return itemList_input(event)
	else:
		selected_node.accept_event()
		
# performs an accessible click action
func click(item := selected_node, button_index = BUTTON_LEFT):
	if Global.TTS_enabled:
		if !help_open:
			print_debug("Click")
			var click = InputEventMouseButton.new()
			click.button_index = button_index
			click.pressed = true
			if item is Node:
				click.position = item.rect_global_position
			else:
				click.position = selected_node.get_tree().root.get_mouse_position()
			selected_node.get_tree().input_event(click)
			click.pressed = false
			selected_node.get_tree().input_event(click)	

# what happens when focused
func focused():
	if !disabled && !help_open:
		var focus = get_focus_owner()
		selected_node = focus
		selected_index = 0
		for c in items:
			if c == selected_node:
				break;
			selected_index+=1
		
		doFocus()
		
func _ready():
	init()
	
func init():
	if name != "Control":
		add_text(name)
	get_nodes(self)
	setNeighbors()
	if items.size() > selected_index:
		items[selected_index].grab_focus()
	onReady()
	
	
func reload():
	clearNeighbors()
	items = []
	init()

func _exit_tree():
	if base_parent != null:
		if base_parent.has_method("enable"):
			base_parent.enable()
			base_parent.reload()
		elif base_parent.has_method("restarter"):
			base_parent.restarter()
		Global.tts_stop()
		
func create_submenu(menu):
	var instance = menu.instance()
	instance.base_parent = self
	disable()
	add_child(instance)
	return instance

#### OVERRIDES ####
# Override these functions instead of overriding the base functions!

func onReady():
	pass
