extends Node2D

onready var background = 0
onready var back_hair = 0
onready var body = 0
onready var neck = 0
onready var shirt = 0
onready var necklace = 0
onready var face = 0
onready var nose = 0
onready var mouth = 0
onready var eyes_back = 0
onready var eyes = 0
onready var eyebrows = 0
onready var snout = 0
onready var snout2 = 0
onready var glasses = 0
onready var ear = 0
onready var earring = 0
onready var front_hair = 0
onready var hat = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	loadYourCharacter()
	
func loadYourCharacter():
	loadFromDictionary(SaveFile.character_appearance)
	loadFromColorDictionary(SaveFile.character_colors)
	
func loadFromColorDictionary(dictionary):
	setDictionaryColorProperty("background",dictionary)
	setDictionaryColorProperty("back_hair",dictionary)
	setDictionaryColorProperty("body",dictionary)
	setDictionaryColorProperty("neck",dictionary)
	setDictionaryColorProperty("shirt",dictionary)
	setDictionaryColorProperty("necklace",dictionary)
	setDictionaryColorProperty("face",dictionary)
	setDictionaryColorProperty("nose",dictionary)
	setDictionaryColorProperty("mouth",dictionary)
	setDictionaryColorProperty("eyes_back",dictionary)
	setDictionaryColorProperty("eyes",dictionary)
	setDictionaryColorProperty("eyebrows",dictionary)
	setDictionaryColorProperty("snout",dictionary)
	setDictionaryColorProperty("snout2",dictionary)
	setDictionaryColorProperty("glasses",dictionary)
	setDictionaryColorProperty("ear",dictionary)
	setDictionaryColorProperty("earring",dictionary)
	setDictionaryColorProperty("front_hair",dictionary)
	setDictionaryColorProperty("hat",dictionary)
	
func loadFromDictionary(dictionary):
	setDictionaryProperty("background",dictionary)
	setDictionaryProperty("back_hair",dictionary)
	setDictionaryProperty("body",dictionary)
	setDictionaryProperty("neck",dictionary)
	setDictionaryProperty("shirt",dictionary)
	setDictionaryProperty("necklace",dictionary)
	setDictionaryProperty("face",dictionary)
	setDictionaryProperty("nose",dictionary)
	setDictionaryProperty("mouth",dictionary)
	setDictionaryProperty("eyes_back",dictionary)
	setDictionaryProperty("eyes",dictionary)
	setDictionaryProperty("eyebrows",dictionary)
	setDictionaryProperty("snout",dictionary)
	setDictionaryProperty("snout2",dictionary)
	setDictionaryProperty("glasses",dictionary)
	setDictionaryProperty("ear",dictionary)
	setDictionaryProperty("earring",dictionary)
	setDictionaryProperty("front_hair",dictionary)
	setDictionaryProperty("hat",dictionary)

func setProperty(property,value):
	# If the value isn't populated yet, set it to 1
	if value == null:
		value = 1
	var number = "%04d" % value
	SaveFile.character_appearance[property] = value
	get_node(property).texture = load("res://texture/faces/"+property+ number +".png")

func setPropertyTexture(property,value):
	get_node(property).texture = value
	var getvalue = value.resource_path.get_file().get_basename().split("0",false,1)
	SaveFile.character_appearance[property] = int(getvalue[1])

func setColorProperty(property,value):
	if value == null:
		value = Color(1,1,1)
	get_node(property).modulate = value
	# special exceptions
	SaveFile.character_colors[property] = value
	if property == "back_hair":
		get_node("front_hair").modulate = value
		SaveFile.character_colors["front_hair"] = value
	if property == "body":
		get_node("neck").modulate = value
		get_node("face").modulate = value
		get_node("nose").modulate = value
		get_node("snout").modulate = value
		get_node("ear").modulate = value
		SaveFile.character_colors["neck"] = value
		SaveFile.character_colors["face"] = value
		SaveFile.character_colors["nose"] = value
		SaveFile.character_colors["snout"] = value
		SaveFile.character_colors["ear"] = value
	
func setDictionaryProperty(property,dictionary):
	var value = 1
	if dictionary.has(property):
		value = dictionary[property]
	# If the value isn't populated yet, set it to 1
	if value == null:
		value = 1
	var number = "%04d" % value
	get_node(property).texture = load("res://texture/faces/"+property+ number +".png")

func setDictionaryColorProperty(property,dictionary):
	var value = 1
	if dictionary.has(property):
		value = dictionary[property]
	# If the value isn't populated yet, set it to 1
	if value == null:
		value = Color(1,1,1)
	get_node(property).modulate = value
	# special exceptions
	if property == "body":
		get_node("neck").modulate = value
		get_node("face").modulate = value
		get_node("nose").modulate = value
		get_node("snout").modulate = value
		get_node("ear").modulate = value

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
