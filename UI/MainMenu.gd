extends Control

onready var optionsMenu = preload("res://Scene/UI/menu/newMenu/newMenu.tscn")
onready var credits = preload("res://map/Credits.tscn")


var instance = null
# Called when the node enters the scene tree for the first time.
func _ready():
	$GridContainer/NewGame.grab_focus()
	Global.tts_stop()
	Global.tts_say("Philosophy Fantasy")
	pass # Replace with function body.

func _on_Options_pressed():
	if !is_instance_valid(instance):
		instance = create_submenu(optionsMenu)
	
func noSaveFile():
	$GridContainer/Continue.queue_free()


func _on_Credits_pressed():
	if get_parent().canClickCredits:
		get_parent().fadeOut()
		var instance = credits.instance()
		get_parent().add_child(instance)


func _on_NewGame_pressed():
	var file = File.new()
	if file.file_exists("user://savegame.save"):
		var instance = preload("res://map/ConfirmSaveDialog.tscn").instance()
		get_parent().add_child(instance)
	else:
		startNewGame()

func startNewGame():
	if !get_parent().startNewGame:
		get_parent().fadeKill()

func _on_Continue_pressed():
	SaveFile.loadFile()
	get_parent().continuez = true;
	get_parent().fadeKill()

func create_submenu(menu):
	var instance = menu.instance()
	instance.base_parent = self
	add_child(instance)
	return instance
	
func restarter():
	$GridContainer/Options.grab_focus()

func _on_NewGame_focus_entered():
	Global.tts_say($GridContainer/NewGame.text)


func _on_Continue_focus_entered():
	Global.tts_say($GridContainer/Continue.text)


func _on_Options_focus_entered():
	Global.tts_say($GridContainer/Options.text)


func _on_Credits_focus_entered():
	Global.tts_say($GridContainer/Credits.text)

func _input(event):
	if event is InputEventKey:
		if Input.is_action_just_pressed("ui_up"):
			Global.playSoundFx("ui/menu_up")
		if Input.is_action_just_pressed("ui_down"):
			Global.playSoundFx("ui/menu_down")
		if Input.is_action_just_pressed("ui_select"):
			get_focus_owner().emit_signal("pressed")
	if Input.is_action_just_pressed("ui_accept") || Input.is_action_just_pressed("ui_select"):
		Global.playSoundFx("ui/menu_open")
