extends "res://data/card/effect/shared/greek.gd"

func doSummon():
	other_side.addStatus("block",2)
	duel.addAction("msgbox","The next effect used by your opponent will be blocked.")
	
func doSummonTest():
	var counter = 0
	for c in side.get_node("Bench").cards:
		if c.isArchetype(c.card_ref.archetype.GREEK):
			return side == duel.players[duel.current_player]
	return false
