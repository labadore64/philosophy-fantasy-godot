extends "res://data/card/effect/shared/greek.gd"

func doSummon():
	duel.addAction("msgbox","Select a card from the Deck to add to the hand.")
	duel.addAction("select",[[side.get_node("Deck")],{"archetype":["Greek"]}])
	duel.addAction("card",[-1,side.get_node("Hand")])
	
func doSummonTest():
	var counter = 0
	for c in side.get_node("Bench").cards:
		if c.isArchetype(c.card_ref.archetype.GREEK):
			return side == duel.players[duel.current_player]
	return false
