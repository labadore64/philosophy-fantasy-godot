extends "res://map/gameplay/duel/DuelCard.gd"

func doActiveBeforeBattle():
	var counter = 0
	for c in side.get_node("Bench").cards:
		if c.isArchetype(c.card_ref.archetype.GREEK):
			counter+=1			
	
	card_ref.attack_boost += counter
	card_ref.updateDisplay()
	
func doActiveBeforeBattleTest():
	var counter = 0
	for c in side.get_node("Bench").cards:
		if c.isArchetype(c.card_ref.archetype.GREEK):
			return side == duel.players[duel.current_player]
	return false
