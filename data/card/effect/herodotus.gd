extends "res://data/card/effect/shared/greek.gd"

func doSummon():
	other_side.get_node("Active").cards[0].moveCard("Limbo")

func doSummonTest():
	var counter = 0
	for c in side.get_node("Bench").cards:
		if c.isArchetype(c.card_ref.archetype.GREEK):
			return side == duel.players[duel.current_player]
	return false
